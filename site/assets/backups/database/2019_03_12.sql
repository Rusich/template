# --- WireDatabaseBackup {"time":"2019-03-12 22:08:27","user":"admin","dbName":"vip990_test","description":"","tables":[],"excludeTables":[],"excludeCreateTables":[],"excludeExportTables":[]}

DROP TABLE IF EXISTS `caches`;
CREATE TABLE `caches` (
  `name` varchar(250) NOT NULL,
  `data` mediumtext NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`name`),
  KEY `expires` (`expires`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.wire/modules/', 'AdminTheme/AdminThemeDefault/AdminThemeDefault.module\nAdminTheme/AdminThemeReno/AdminThemeReno.module\nAdminTheme/AdminThemeUikit/AdminThemeUikit.module\nFieldtype/FieldtypeCache.module\nFieldtype/FieldtypeCheckbox.module\nFieldtype/FieldtypeComments/CommentFilterAkismet.module\nFieldtype/FieldtypeComments/FieldtypeComments.module\nFieldtype/FieldtypeComments/InputfieldCommentsAdmin.module\nFieldtype/FieldtypeDatetime.module\nFieldtype/FieldtypeEmail.module\nFieldtype/FieldtypeFieldsetClose.module\nFieldtype/FieldtypeFieldsetOpen.module\nFieldtype/FieldtypeFieldsetTabOpen.module\nFieldtype/FieldtypeFile.module\nFieldtype/FieldtypeFloat.module\nFieldtype/FieldtypeImage.module\nFieldtype/FieldtypeInteger.module\nFieldtype/FieldtypeModule.module\nFieldtype/FieldtypeOptions/FieldtypeOptions.module\nFieldtype/FieldtypePage.module\nFieldtype/FieldtypePageTable.module\nFieldtype/FieldtypePageTitle.module\nFieldtype/FieldtypePassword.module\nFieldtype/FieldtypeRepeater/FieldtypeFieldsetPage.module\nFieldtype/FieldtypeRepeater/FieldtypeRepeater.module\nFieldtype/FieldtypeRepeater/InputfieldRepeater.module\nFieldtype/FieldtypeSelector.module\nFieldtype/FieldtypeText.module\nFieldtype/FieldtypeTextarea.module\nFieldtype/FieldtypeURL.module\nFileCompilerTags.module\nImage/ImageSizerEngineAnimatedGif/ImageSizerEngineAnimatedGif.module\nImage/ImageSizerEngineIMagick/ImageSizerEngineIMagick.module\nInputfield/InputfieldAsmSelect/InputfieldAsmSelect.module\nInputfield/InputfieldButton.module\nInputfield/InputfieldCheckbox.module\nInputfield/InputfieldCheckboxes/InputfieldCheckboxes.module\nInputfield/InputfieldCKEditor/InputfieldCKEditor.module\nInputfield/InputfieldDatetime/InputfieldDatetime.module\nInputfield/InputfieldEmail.module\nInputfield/InputfieldFieldset.module\nInputfield/InputfieldFile/InputfieldFile.module\nInputfield/InputfieldFloat.module\nInputfield/InputfieldForm.module\nInputfield/InputfieldHidden.module\nInputfield/InputfieldIcon/InputfieldIcon.module\nInputfield/InputfieldImage/InputfieldImage.module\nInputfield/InputfieldInteger.module\nInputfield/InputfieldMarkup.module\nInputfield/InputfieldName.module\nInputfield/InputfieldPage/InputfieldPage.module\nInputfield/InputfieldPageAutocomplete/InputfieldPageAutocomplete.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelect.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelectMultiple.module\nInputfield/InputfieldPageName/InputfieldPageName.module\nInputfield/InputfieldPageTable/InputfieldPageTable.module\nInputfield/InputfieldPageTitle/InputfieldPageTitle.module\nInputfield/InputfieldPassword/InputfieldPassword.module\nInputfield/InputfieldRadios/InputfieldRadios.module\nInputfield/InputfieldSelect.module\nInputfield/InputfieldSelectMultiple.module\nInputfield/InputfieldSelector/InputfieldSelector.module\nInputfield/InputfieldSubmit/InputfieldSubmit.module\nInputfield/InputfieldText.module\nInputfield/InputfieldTextarea.module\nInputfield/InputfieldURL.module\nJquery/JqueryCore/JqueryCore.module\nJquery/JqueryMagnific/JqueryMagnific.module\nJquery/JqueryTableSorter/JqueryTableSorter.module\nJquery/JqueryUI/JqueryUI.module\nJquery/JqueryWireTabs/JqueryWireTabs.module\nLanguageSupport/FieldtypePageTitleLanguage.module\nLanguageSupport/FieldtypeTextareaLanguage.module\nLanguageSupport/FieldtypeTextLanguage.module\nLanguageSupport/LanguageSupport.module\nLanguageSupport/LanguageSupportFields.module\nLanguageSupport/LanguageSupportPageNames.module\nLanguageSupport/LanguageTabs.module\nLanguageSupport/ProcessLanguage.module\nLanguageSupport/ProcessLanguageTranslator.module\nLazyCron.module\nMarkup/MarkupAdminDataTable/MarkupAdminDataTable.module\nMarkup/MarkupCache.module\nMarkup/MarkupHTMLPurifier/MarkupHTMLPurifier.module\nMarkup/MarkupPageArray.module\nMarkup/MarkupPageFields.module\nMarkup/MarkupPagerNav/MarkupPagerNav.module\nMarkup/MarkupRSS.module\nPage/PageFrontEdit/PageFrontEdit.module\nPagePathHistory.module\nPagePaths.module\nPagePermissions.module\nPageRender.module\nProcess/ProcessCommentsManager/ProcessCommentsManager.module\nProcess/ProcessField/ProcessField.module\nProcess/ProcessForgotPassword.module\nProcess/ProcessHome.module\nProcess/ProcessList.module\nProcess/ProcessLogger/ProcessLogger.module\nProcess/ProcessLogin/ProcessLogin.module\nProcess/ProcessModule/ProcessModule.module\nProcess/ProcessPageAdd/ProcessPageAdd.module\nProcess/ProcessPageClone.module\nProcess/ProcessPageEdit/ProcessPageEdit.module\nProcess/ProcessPageEditImageSelect/ProcessPageEditImageSelect.module\nProcess/ProcessPageEditLink/ProcessPageEditLink.module\nProcess/ProcessPageList/ProcessPageList.module\nProcess/ProcessPageLister/ProcessPageLister.module\nProcess/ProcessPageSearch/ProcessPageSearch.module\nProcess/ProcessPagesExportImport/ProcessPagesExportImport.module\nProcess/ProcessPageSort.module\nProcess/ProcessPageTrash.module\nProcess/ProcessPageType/ProcessPageType.module\nProcess/ProcessPageView.module\nProcess/ProcessPermission/ProcessPermission.module\nProcess/ProcessProfile/ProcessProfile.module\nProcess/ProcessRecentPages/ProcessRecentPages.module\nProcess/ProcessRole/ProcessRole.module\nProcess/ProcessTemplate/ProcessTemplate.module\nProcess/ProcessUser/ProcessUser.module\nSession/SessionHandlerDB/ProcessSessionDB.module\nSession/SessionHandlerDB/SessionHandlerDB.module\nSession/SessionLoginThrottle/SessionLoginThrottle.module\nSystem/SystemNotifications/FieldtypeNotifications.module\nSystem/SystemNotifications/SystemNotifications.module\nSystem/SystemUpdater/SystemUpdater.module\nTextformatter/TextformatterEntities.module\nTextformatter/TextformatterMarkdownExtra/TextformatterMarkdownExtra.module\nTextformatter/TextformatterNewlineBR.module\nTextformatter/TextformatterNewlineUL.module\nTextformatter/TextformatterPstripper.module\nTextformatter/TextformatterSmartypants/TextformatterSmartypants.module\nTextformatter/TextformatterStripTags.module', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e7d87da4a5b2cef998e8b6fc4bfa4fc2', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpConfig.php\",\"hash\":\"13c09ca2bd5625656ce786d9cc4113de\",\"size\":14370,\"time\":1542962722,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpConfig.php\",\"hash\":\"9f3b16b62cd6d489209fa6a1a541b96d\",\"size\":14565,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.info', '{\"148\":{\"name\":\"AdminThemeDefault\",\"title\":\"Default\",\"version\":14,\"autoload\":\"template=admin\",\"created\":1542309801,\"configurable\":19,\"namespace\":\"ProcessWire\\\\\"},\"163\":{\"name\":\"AdminThemeReno\",\"title\":\"Reno\",\"version\":17,\"requiresVersions\":{\"AdminThemeDefault\":[\">=\",0]},\"autoload\":\"template=admin\",\"created\":1542309916,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"160\":{\"name\":\"AdminThemeUikit\",\"title\":\"Uikit\",\"version\":24,\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.94\"]},\"autoload\":\"template=admin\",\"created\":1542309861,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\"},\"97\":{\"name\":\"FieldtypeCheckbox\",\"title\":\"Checkbox\",\"version\":101,\"singular\":true,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"28\":{\"name\":\"FieldtypeDatetime\",\"title\":\"Datetime\",\"version\":104,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\"},\"29\":{\"name\":\"FieldtypeEmail\",\"title\":\"E-Mail\",\"version\":100,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\"},\"106\":{\"name\":\"FieldtypeFieldsetClose\",\"title\":\"Fieldset (Close)\",\"version\":100,\"singular\":true,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"105\":{\"name\":\"FieldtypeFieldsetOpen\",\"title\":\"Fieldset (Open)\",\"version\":101,\"singular\":true,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"107\":{\"name\":\"FieldtypeFieldsetTabOpen\",\"title\":\"Fieldset in Tab (Open)\",\"version\":100,\"singular\":true,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"6\":{\"name\":\"FieldtypeFile\",\"title\":\"Files\",\"version\":105,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"89\":{\"name\":\"FieldtypeFloat\",\"title\":\"Float\",\"version\":105,\"singular\":1,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"57\":{\"name\":\"FieldtypeImage\",\"title\":\"Images\",\"version\":101,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"84\":{\"name\":\"FieldtypeInteger\",\"title\":\"Integer\",\"version\":101,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"27\":{\"name\":\"FieldtypeModule\",\"title\":\"Module Reference\",\"version\":101,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"4\":{\"name\":\"FieldtypePage\",\"title\":\"Page Reference\",\"version\":104,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"111\":{\"name\":\"FieldtypePageTitle\",\"title\":\"Page Title\",\"version\":100,\"singular\":true,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"133\":{\"name\":\"FieldtypePassword\",\"title\":\"Password\",\"version\":101,\"singular\":true,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"164\":{\"name\":\"FieldtypeRepeater\",\"title\":\"Repeater\",\"version\":106,\"installs\":[\"InputfieldRepeater\"],\"autoload\":true,\"singular\":true,\"created\":1542309970,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"165\":{\"name\":\"InputfieldRepeater\",\"title\":\"Repeater\",\"version\":106,\"requiresVersions\":{\"FieldtypeRepeater\":[\">=\",0]},\"created\":1542309970,\"namespace\":\"ProcessWire\\\\\"},\"3\":{\"name\":\"FieldtypeText\",\"title\":\"Text\",\"version\":100,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"1\":{\"name\":\"FieldtypeTextarea\",\"title\":\"Textarea\",\"version\":106,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"135\":{\"name\":\"FieldtypeURL\",\"title\":\"URL\",\"version\":101,\"singular\":true,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"25\":{\"name\":\"InputfieldAsmSelect\",\"title\":\"asmSelect\",\"version\":121,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"131\":{\"name\":\"InputfieldButton\",\"title\":\"Button\",\"version\":100,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"37\":{\"name\":\"InputfieldCheckbox\",\"title\":\"Checkbox\",\"version\":105,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"38\":{\"name\":\"InputfieldCheckboxes\",\"title\":\"Checkboxes\",\"version\":107,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"155\":{\"name\":\"InputfieldCKEditor\",\"title\":\"CKEditor\",\"version\":160,\"installs\":[\"MarkupHTMLPurifier\"],\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\"},\"94\":{\"name\":\"InputfieldDatetime\",\"title\":\"Datetime\",\"version\":106,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"80\":{\"name\":\"InputfieldEmail\",\"title\":\"Email\",\"version\":101,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\"},\"78\":{\"name\":\"InputfieldFieldset\",\"title\":\"Fieldset\",\"version\":101,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"55\":{\"name\":\"InputfieldFile\",\"title\":\"Files\",\"version\":125,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"90\":{\"name\":\"InputfieldFloat\",\"title\":\"Float\",\"version\":103,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"30\":{\"name\":\"InputfieldForm\",\"title\":\"Form\",\"version\":107,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"40\":{\"name\":\"InputfieldHidden\",\"title\":\"Hidden\",\"version\":101,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"162\":{\"name\":\"InputfieldIcon\",\"title\":\"Icon\",\"version\":2,\"created\":1542309873,\"namespace\":\"ProcessWire\\\\\"},\"56\":{\"name\":\"InputfieldImage\",\"title\":\"Images\",\"version\":122,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"85\":{\"name\":\"InputfieldInteger\",\"title\":\"Integer\",\"version\":104,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"79\":{\"name\":\"InputfieldMarkup\",\"title\":\"Markup\",\"version\":102,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"41\":{\"name\":\"InputfieldName\",\"title\":\"Name\",\"version\":100,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"60\":{\"name\":\"InputfieldPage\",\"title\":\"Page\",\"version\":107,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"181\":{\"name\":\"InputfieldPageAutocomplete\",\"title\":\"Page Auto Complete\",\"version\":112,\"created\":1544458428,\"namespace\":\"ProcessWire\\\\\"},\"15\":{\"name\":\"InputfieldPageListSelect\",\"title\":\"Page List Select\",\"version\":101,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"137\":{\"name\":\"InputfieldPageListSelectMultiple\",\"title\":\"Page List Select Multiple\",\"version\":102,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"86\":{\"name\":\"InputfieldPageName\",\"title\":\"Page Name\",\"version\":106,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"112\":{\"name\":\"InputfieldPageTitle\",\"title\":\"Page Title\",\"version\":102,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"122\":{\"name\":\"InputfieldPassword\",\"title\":\"Password\",\"version\":102,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"39\":{\"name\":\"InputfieldRadios\",\"title\":\"Radio Buttons\",\"version\":105,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"36\":{\"name\":\"InputfieldSelect\",\"title\":\"Select\",\"version\":102,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"43\":{\"name\":\"InputfieldSelectMultiple\",\"title\":\"Select Multiple\",\"version\":101,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"149\":{\"name\":\"InputfieldSelector\",\"title\":\"Selector\",\"version\":27,\"autoload\":\"template=admin\",\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"addFlag\":32},\"32\":{\"name\":\"InputfieldSubmit\",\"title\":\"Submit\",\"version\":102,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"34\":{\"name\":\"InputfieldText\",\"title\":\"Text\",\"version\":106,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"35\":{\"name\":\"InputfieldTextarea\",\"title\":\"Textarea\",\"version\":103,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"108\":{\"name\":\"InputfieldURL\",\"title\":\"URL\",\"version\":102,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\"},\"116\":{\"name\":\"JqueryCore\",\"title\":\"jQuery Core\",\"version\":183,\"singular\":true,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"151\":{\"name\":\"JqueryMagnific\",\"title\":\"jQuery Magnific Popup\",\"version\":1,\"singular\":1,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\"},\"103\":{\"name\":\"JqueryTableSorter\",\"title\":\"jQuery Table Sorter Plugin\",\"version\":221,\"singular\":1,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\"},\"117\":{\"name\":\"JqueryUI\",\"title\":\"jQuery UI\",\"version\":196,\"singular\":true,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"45\":{\"name\":\"JqueryWireTabs\",\"title\":\"jQuery Wire Tabs Plugin\",\"version\":108,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"171\":{\"name\":\"FieldtypePageTitleLanguage\",\"title\":\"Page Title (Multi-Language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0],\"FieldtypeTextLanguage\":[\">=\",0]},\"singular\":1,\"created\":1542310081,\"namespace\":\"ProcessWire\\\\\"},\"172\":{\"name\":\"FieldtypeTextareaLanguage\",\"title\":\"Textarea (Multi-language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"singular\":1,\"created\":1542310081,\"namespace\":\"ProcessWire\\\\\"},\"170\":{\"name\":\"FieldtypeTextLanguage\",\"title\":\"Text (Multi-language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"singular\":1,\"created\":1542310080,\"namespace\":\"ProcessWire\\\\\"},\"166\":{\"name\":\"LanguageSupport\",\"title\":\"Languages Support\",\"version\":103,\"installs\":[\"ProcessLanguage\",\"ProcessLanguageTranslator\"],\"autoload\":true,\"singular\":true,\"created\":1542310004,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"addFlag\":32},\"169\":{\"name\":\"LanguageSupportFields\",\"title\":\"Languages Support - Fields\",\"version\":100,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"installs\":[\"FieldtypePageTitleLanguage\",\"FieldtypeTextareaLanguage\",\"FieldtypeTextLanguage\"],\"autoload\":true,\"singular\":true,\"created\":1542310080,\"namespace\":\"ProcessWire\\\\\"},\"167\":{\"name\":\"ProcessLanguage\",\"title\":\"Languages\",\"version\":103,\"icon\":\"language\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"singular\":1,\"created\":1542310004,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true},\"168\":{\"name\":\"ProcessLanguageTranslator\",\"title\":\"Language Translator\",\"version\":101,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"singular\":1,\"created\":1542310005,\"namespace\":\"ProcessWire\\\\\"},\"67\":{\"name\":\"MarkupAdminDataTable\",\"title\":\"Admin Data Table\",\"version\":107,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"177\":{\"name\":\"MarkupCache\",\"title\":\"Markup Cache\",\"version\":101,\"autoload\":true,\"singular\":true,\"created\":1542310764,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"156\":{\"name\":\"MarkupHTMLPurifier\",\"title\":\"HTML Purifier\",\"version\":492,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\"},\"113\":{\"name\":\"MarkupPageArray\",\"title\":\"PageArray Markup\",\"version\":100,\"autoload\":true,\"singular\":true,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\"},\"98\":{\"name\":\"MarkupPagerNav\",\"title\":\"Pager (Pagination) Navigation\",\"version\":105,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\"},\"114\":{\"name\":\"PagePermissions\",\"title\":\"Page Permissions\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"115\":{\"name\":\"PageRender\",\"title\":\"Page Render\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"48\":{\"name\":\"ProcessField\",\"title\":\"Fields\",\"version\":112,\"icon\":\"cube\",\"permission\":\"field-admin\",\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"87\":{\"name\":\"ProcessHome\",\"title\":\"Admin Home\",\"version\":101,\"permission\":\"page-view\",\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"76\":{\"name\":\"ProcessList\",\"title\":\"List\",\"version\":101,\"permission\":\"page-view\",\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"161\":{\"name\":\"ProcessLogger\",\"title\":\"Logs\",\"version\":1,\"icon\":\"tree\",\"permission\":\"logs-view\",\"singular\":1,\"created\":1542309873,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true},\"10\":{\"name\":\"ProcessLogin\",\"title\":\"Login\",\"version\":104,\"permission\":\"page-view\",\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"50\":{\"name\":\"ProcessModule\",\"title\":\"Modules\",\"version\":118,\"permission\":\"module-admin\",\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"nav\":[{\"url\":\"?site#tab_site_modules\",\"label\":\"Site\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?site=1\"},{\"url\":\"?core#tab_core_modules\",\"label\":\"Core\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?core=1\"},{\"url\":\"?configurable#tab_configurable_modules\",\"label\":\"Configure\",\"icon\":\"gear\",\"navJSON\":\"navJSON\\/?configurable=1\"},{\"url\":\"?install#tab_install_modules\",\"label\":\"Install\",\"icon\":\"sign-in\",\"navJSON\":\"navJSON\\/?install=1\"},{\"url\":\"?reset=1\",\"label\":\"Refresh\",\"icon\":\"refresh\"}]},\"17\":{\"name\":\"ProcessPageAdd\",\"title\":\"Page Add\",\"version\":108,\"icon\":\"plus-circle\",\"permission\":\"page-edit\",\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"7\":{\"name\":\"ProcessPageEdit\",\"title\":\"Page Edit\",\"version\":108,\"icon\":\"edit\",\"permission\":\"page-edit\",\"singular\":1,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"129\":{\"name\":\"ProcessPageEditImageSelect\",\"title\":\"Page Edit Image\",\"version\":120,\"permission\":\"page-edit\",\"singular\":1,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"121\":{\"name\":\"ProcessPageEditLink\",\"title\":\"Page Edit Link\",\"version\":108,\"icon\":\"link\",\"permission\":\"page-edit\",\"singular\":1,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"12\":{\"name\":\"ProcessPageList\",\"title\":\"Page List\",\"version\":119,\"icon\":\"sitemap\",\"permission\":\"page-edit\",\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"150\":{\"name\":\"ProcessPageLister\",\"title\":\"Lister\",\"version\":24,\"icon\":\"search\",\"permission\":\"page-lister\",\"created\":1542309801,\"configurable\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"104\":{\"name\":\"ProcessPageSearch\",\"title\":\"Page Search\",\"version\":106,\"permission\":\"page-edit\",\"singular\":1,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"14\":{\"name\":\"ProcessPageSort\",\"title\":\"Page Sort and Move\",\"version\":100,\"permission\":\"page-edit\",\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"109\":{\"name\":\"ProcessPageTrash\",\"title\":\"Page Trash\",\"version\":102,\"singular\":1,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"134\":{\"name\":\"ProcessPageType\",\"title\":\"Page Type\",\"version\":101,\"singular\":1,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"83\":{\"name\":\"ProcessPageView\",\"title\":\"Page View\",\"version\":104,\"permission\":\"page-view\",\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"136\":{\"name\":\"ProcessPermission\",\"title\":\"Permissions\",\"version\":101,\"icon\":\"gear\",\"permission\":\"permission-admin\",\"singular\":1,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"138\":{\"name\":\"ProcessProfile\",\"title\":\"User Profile\",\"version\":103,\"permission\":\"profile-edit\",\"singular\":1,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"159\":{\"name\":\"ProcessRecentPages\",\"title\":\"Recent Pages\",\"version\":2,\"icon\":\"clock-o\",\"permission\":\"page-edit-recent\",\"singular\":1,\"created\":1542309861,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true,\"nav\":[{\"url\":\"?edited=1\",\"label\":\"Edited\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?edited=1\"},{\"url\":\"?added=1\",\"label\":\"Created\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?added=1&me=1\"},{\"url\":\"?edited=1&me=1\",\"label\":\"Edited by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?edited=1&me=1\"},{\"url\":\"?added=1&me=1\",\"label\":\"Created by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?added=1&me=1\"},{\"url\":\"another\\/\",\"label\":\"Add another\",\"icon\":\"plus-circle\",\"navJSON\":\"anotherNavJSON\\/\"}]},\"68\":{\"name\":\"ProcessRole\",\"title\":\"Roles\",\"version\":104,\"icon\":\"gears\",\"permission\":\"role-admin\",\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"47\":{\"name\":\"ProcessTemplate\",\"title\":\"Templates\",\"version\":114,\"icon\":\"cubes\",\"permission\":\"template-admin\",\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"66\":{\"name\":\"ProcessUser\",\"title\":\"Users\",\"version\":107,\"icon\":\"group\",\"permission\":\"user-admin\",\"created\":1542309801,\"configurable\":\"ProcessUserConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"125\":{\"name\":\"SessionLoginThrottle\",\"title\":\"Session Login Throttle\",\"version\":102,\"autoload\":\"function\",\"singular\":true,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"139\":{\"name\":\"SystemUpdater\",\"title\":\"System Updater\",\"version\":16,\"singular\":true,\"created\":1542309801,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"61\":{\"name\":\"TextformatterEntities\",\"title\":\"HTML Entity Encoder (htmlspecialchars)\",\"version\":100,\"created\":1542309801,\"namespace\":\"ProcessWire\\\\\"},\"179\":{\"name\":\"Duplicator\",\"title\":\"Duplicator\",\"version\":\"1.2.9\",\"icon\":\"clone\",\"autoload\":\"function\",\"singular\":true,\"created\":1542313727,\"configurable\":true,\"namespace\":\"\\\\\"},\"180\":{\"name\":\"ProcessDuplicator\",\"title\":\"Duplicator - Packages Manager\",\"version\":\"1.2.9\",\"icon\":\"clone\",\"permission\":\"duplicator\",\"autoload\":\"function\",\"singular\":1,\"created\":1542313846,\"namespace\":\"\\\\\",\"require\":\"Duplicator\"},\"178\":{\"name\":\"FieldtypeYaml\",\"title\":\"Fieldtype YAML\",\"version\":\"0.3.0\",\"icon\":\"code\",\"requiresVersions\":{\"PHP\":[\">=\",\"5.4\"],\"ProcessWire\":[\">=\",\"2.5.5\"]},\"singular\":true,\"created\":1542312099,\"namespace\":\"\\\\\"},\"174\":{\"name\":\"HelperFieldLinks\",\"title\":\"Developer helper - field & template shortcuts\",\"version\":108,\"autoload\":true,\"singular\":true,\"created\":1542310242,\"namespace\":\"\\\\\"},\"176\":{\"name\":\"MarkupSitemapXML\",\"title\":\"Markup Sitemap XML\",\"version\":110,\"autoload\":true,\"singular\":true,\"created\":1542310764,\"namespace\":\"\\\\\"},\"173\":{\"name\":\"ProcessDatabaseBackups\",\"title\":\"Database Backups\",\"version\":4,\"icon\":\"database\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.62\"]},\"permission\":\"db-backup\",\"singular\":1,\"created\":1542310192,\"nav\":[{\"url\":\".\\/\",\"label\":\"View\",\"icon\":\"list\"},{\"url\":\"backup\\/\",\"label\":\"Backup\",\"icon\":\"plus-circle\"},{\"url\":\"upload\\/\",\"label\":\"Upload\",\"icon\":\"cloud-upload\"}]},\"182\":{\"name\":\"ProcessWireUpgrade\",\"title\":\"Upgrades\",\"version\":7,\"icon\":\"coffee\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"2.5.20\"]},\"installs\":[\"ProcessWireUpgradeCheck\"],\"singular\":true,\"created\":1544459122,\"namespace\":\"\\\\\"},\"183\":{\"name\":\"ProcessWireUpgradeCheck\",\"title\":\"Upgrades Checker\",\"version\":7,\"icon\":\"coffee\",\"autoload\":\"template=admin\",\"singular\":true,\"created\":1544459123,\"configurable\":\"ProcessWireUpgradeCheck.config.php\",\"namespace\":\"\\\\\"},\"175\":{\"name\":\"WireMailSmtp\",\"title\":\"Wire Mail SMTP\",\"version\":\"0.3.0\",\"created\":1542310295,\"configurable\":true,\"namespace\":\"\\\\\"}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ModulesVerbose.info', '{\"148\":{\"summary\":\"Minimal admin theme that supports all ProcessWire features.\",\"core\":true,\"versionStr\":\"0.1.4\"},\"163\":{\"summary\":\"Admin theme for ProcessWire 2.5+ by Tom Reno (Renobird)\",\"author\":\"Tom Reno (Renobird)\",\"core\":true,\"versionStr\":\"0.1.7\"},\"160\":{\"summary\":\"Uikit v3 admin theme\",\"core\":true,\"versionStr\":\"0.2.4\"},\"97\":{\"summary\":\"This Fieldtype stores an ON\\/OFF toggle via a single checkbox. The ON value is 1 and OFF value is 0.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"28\":{\"summary\":\"Field that stores a date and optionally time\",\"core\":true,\"versionStr\":\"1.0.4\"},\"29\":{\"summary\":\"Field that stores an e-mail address\",\"core\":true,\"versionStr\":\"1.0.0\"},\"106\":{\"summary\":\"Close a fieldset opened by FieldsetOpen. \",\"core\":true,\"versionStr\":\"1.0.0\"},\"105\":{\"summary\":\"Open a fieldset to group fields. Should be followed by a Fieldset (Close) after one or more fields.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"107\":{\"summary\":\"Open a fieldset to group fields. Same as Fieldset (Open) except that it displays in a tab instead.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"6\":{\"summary\":\"Field that stores one or more files\",\"core\":true,\"versionStr\":\"1.0.5\"},\"89\":{\"summary\":\"Field that stores a floating point (decimal) number\",\"core\":true,\"versionStr\":\"1.0.5\"},\"57\":{\"summary\":\"Field that stores one or more GIF, JPG, or PNG images\",\"core\":true,\"versionStr\":\"1.0.1\"},\"84\":{\"summary\":\"Field that stores an integer\",\"core\":true,\"versionStr\":\"1.0.1\"},\"27\":{\"summary\":\"Field that stores a reference to another module\",\"core\":true,\"versionStr\":\"1.0.1\"},\"4\":{\"summary\":\"Field that stores one or more references to ProcessWire pages\",\"core\":true,\"versionStr\":\"1.0.4\"},\"111\":{\"summary\":\"Field that stores a page title\",\"core\":true,\"versionStr\":\"1.0.0\"},\"133\":{\"summary\":\"Field that stores a hashed and salted password\",\"core\":true,\"versionStr\":\"1.0.1\"},\"164\":{\"summary\":\"Maintains a collection of fields that are repeated for any number of times.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"165\":{\"summary\":\"Repeats fields from another template. Provides the input for FieldtypeRepeater.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"3\":{\"summary\":\"Field that stores a single line of text\",\"core\":true,\"versionStr\":\"1.0.0\"},\"1\":{\"summary\":\"Field that stores multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.6\"},\"135\":{\"summary\":\"Field that stores a URL\",\"core\":true,\"versionStr\":\"1.0.1\"},\"25\":{\"summary\":\"Multiple selection, progressive enhancement to select multiple\",\"core\":true,\"versionStr\":\"1.2.1\"},\"131\":{\"summary\":\"Form button element that you can optionally pass an href attribute to.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"37\":{\"summary\":\"Single checkbox toggle\",\"core\":true,\"versionStr\":\"1.0.5\"},\"38\":{\"summary\":\"Multiple checkbox toggles\",\"core\":true,\"versionStr\":\"1.0.7\"},\"155\":{\"summary\":\"CKEditor textarea rich text editor.\",\"core\":true,\"versionStr\":\"1.6.0\"},\"94\":{\"summary\":\"Inputfield that accepts date and optionally time\",\"core\":true,\"versionStr\":\"1.0.6\"},\"80\":{\"summary\":\"E-Mail address in valid format\",\"core\":true,\"versionStr\":\"1.0.1\"},\"78\":{\"summary\":\"Groups one or more fields together in a container\",\"core\":true,\"versionStr\":\"1.0.1\"},\"55\":{\"summary\":\"One or more file uploads (sortable)\",\"core\":true,\"versionStr\":\"1.2.5\"},\"90\":{\"summary\":\"Floating point number with precision\",\"core\":true,\"versionStr\":\"1.0.3\"},\"30\":{\"summary\":\"Contains one or more fields in a form\",\"core\":true,\"versionStr\":\"1.0.7\"},\"40\":{\"summary\":\"Hidden value in a form\",\"core\":true,\"versionStr\":\"1.0.1\"},\"162\":{\"summary\":\"Select an icon\",\"core\":true,\"versionStr\":\"0.0.2\"},\"56\":{\"summary\":\"One or more image uploads (sortable)\",\"core\":true,\"versionStr\":\"1.2.2\"},\"85\":{\"summary\":\"Integer (positive or negative)\",\"core\":true,\"versionStr\":\"1.0.4\"},\"79\":{\"summary\":\"Contains any other markup and optionally child Inputfields\",\"core\":true,\"versionStr\":\"1.0.2\"},\"41\":{\"summary\":\"Text input validated as a ProcessWire name field\",\"core\":true,\"versionStr\":\"1.0.0\"},\"60\":{\"summary\":\"Select one or more pages\",\"core\":true,\"versionStr\":\"1.0.7\"},\"181\":{\"summary\":\"Multiple Page selection using auto completion and sorting capability. Intended for use as an input field for Page reference fields.\",\"core\":true,\"versionStr\":\"1.1.2\"},\"15\":{\"summary\":\"Selection of a single page from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"137\":{\"summary\":\"Selection of multiple pages from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.2\"},\"86\":{\"summary\":\"Text input validated as a ProcessWire Page name field\",\"core\":true,\"versionStr\":\"1.0.6\"},\"112\":{\"summary\":\"Handles input of Page Title and auto-generation of Page Name (when name is blank)\",\"core\":true,\"versionStr\":\"1.0.2\"},\"122\":{\"summary\":\"Password input with confirmation field that doesn&#039;t ever echo the input back.\",\"core\":true,\"versionStr\":\"1.0.2\"},\"39\":{\"summary\":\"Radio buttons for selection of a single item\",\"core\":true,\"versionStr\":\"1.0.5\"},\"36\":{\"summary\":\"Selection of a single value from a select pulldown\",\"core\":true,\"versionStr\":\"1.0.2\"},\"43\":{\"summary\":\"Select multiple items from a list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"149\":{\"summary\":\"Build a page finding selector visually.\",\"author\":\"Avoine + ProcessWire\",\"core\":true,\"versionStr\":\"0.2.7\"},\"32\":{\"summary\":\"Form submit button\",\"core\":true,\"versionStr\":\"1.0.2\"},\"34\":{\"summary\":\"Single line of text\",\"core\":true,\"versionStr\":\"1.0.6\"},\"35\":{\"summary\":\"Multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.3\"},\"108\":{\"summary\":\"URL in valid format\",\"core\":true,\"versionStr\":\"1.0.2\"},\"116\":{\"summary\":\"jQuery Core as required by ProcessWire Admin and plugins\",\"href\":\"http:\\/\\/jquery.com\",\"core\":true,\"versionStr\":\"1.8.3\"},\"151\":{\"summary\":\"Provides lightbox capability for image galleries. Replacement for FancyBox. Uses Magnific Popup by @dimsemenov.\",\"href\":\"http:\\/\\/dimsemenov.com\\/plugins\\/magnific-popup\\/\",\"core\":true,\"versionStr\":\"0.0.1\"},\"103\":{\"summary\":\"Provides a jQuery plugin for sorting tables.\",\"href\":\"http:\\/\\/mottie.github.io\\/tablesorter\\/\",\"core\":true,\"versionStr\":\"2.2.1\"},\"117\":{\"summary\":\"jQuery UI as required by ProcessWire and plugins\",\"href\":\"http:\\/\\/ui.jquery.com\",\"core\":true,\"versionStr\":\"1.9.6\"},\"45\":{\"summary\":\"Provides a jQuery plugin for generating tabs in ProcessWire.\",\"core\":true,\"versionStr\":\"1.0.8\"},\"171\":{\"summary\":\"Field that stores a page title in multiple languages. Use this only if you want title inputs created for ALL languages on ALL pages. Otherwise create separate languaged-named title fields, i.e. title_fr, title_es, title_fi, etc. \",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.0\"},\"172\":{\"summary\":\"Field that stores a multiple lines of text in multiple languages\",\"core\":true,\"versionStr\":\"1.0.0\"},\"170\":{\"summary\":\"Field that stores a single line of text in multiple languages\",\"core\":true,\"versionStr\":\"1.0.0\"},\"166\":{\"summary\":\"ProcessWire multi-language support.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.3\"},\"169\":{\"summary\":\"Required to use multi-language fields.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.0\"},\"167\":{\"summary\":\"Manage system languages\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.3\",\"permissions\":{\"lang-edit\":\"Administer languages and static translation files\"}},\"168\":{\"summary\":\"Provides language translation capabilities for ProcessWire core and modules.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.1\"},\"67\":{\"summary\":\"Generates markup for data tables used by ProcessWire admin\",\"core\":true,\"versionStr\":\"1.0.7\"},\"177\":{\"summary\":\"A simple way to cache segments of markup in your templates. \",\"href\":\"https:\\/\\/processwire.com\\/api\\/modules\\/markupcache\\/\",\"core\":true,\"versionStr\":\"1.0.1\"},\"156\":{\"summary\":\"Front-end to the HTML Purifier library.\",\"core\":true,\"versionStr\":\"4.9.2\"},\"113\":{\"summary\":\"Adds a render() method to all PageArray instances for basic unordered list generation of PageArrays.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"98\":{\"summary\":\"Generates markup for pagination navigation\",\"core\":true,\"versionStr\":\"1.0.5\"},\"114\":{\"summary\":\"Adds various permission methods to Page objects that are used by Process modules.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"115\":{\"summary\":\"Adds a render method to Page and caches page output.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"48\":{\"summary\":\"Edit individual fields that hold page data\",\"core\":true,\"versionStr\":\"1.1.2\"},\"87\":{\"summary\":\"Acts as a placeholder Process for the admin root. Ensures proper flow control after login.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"76\":{\"summary\":\"Lists the Process assigned to each child page of the current\",\"core\":true,\"versionStr\":\"1.0.1\"},\"161\":{\"summary\":\"View and manage system logs.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.0.1\",\"permissions\":{\"logs-view\":\"Can view system logs\",\"logs-edit\":\"Can manage system logs\"},\"page\":{\"name\":\"logs\",\"parent\":\"setup\",\"title\":\"Logs\"}},\"10\":{\"summary\":\"Login to ProcessWire\",\"core\":true,\"versionStr\":\"1.0.4\"},\"50\":{\"summary\":\"List, edit or install\\/uninstall modules\",\"core\":true,\"versionStr\":\"1.1.8\"},\"17\":{\"summary\":\"Add a new page\",\"core\":true,\"versionStr\":\"1.0.8\"},\"7\":{\"summary\":\"Edit a Page\",\"core\":true,\"versionStr\":\"1.0.8\"},\"129\":{\"summary\":\"Provides image manipulation functions for image fields and rich text editors.\",\"core\":true,\"versionStr\":\"1.2.0\"},\"121\":{\"summary\":\"Provides a link capability as used by some Fieldtype modules (like rich text editors).\",\"core\":true,\"versionStr\":\"1.0.8\"},\"12\":{\"summary\":\"List pages in a hierarchal tree structure\",\"core\":true,\"versionStr\":\"1.1.9\"},\"150\":{\"summary\":\"Admin tool for finding and listing pages by any property.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.2.4\",\"permissions\":{\"page-lister\":\"Use Page Lister\"}},\"104\":{\"summary\":\"Provides a page search engine for admin use.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"14\":{\"summary\":\"Handles page sorting and moving for PageList\",\"core\":true,\"versionStr\":\"1.0.0\"},\"109\":{\"summary\":\"Handles emptying of Page trash\",\"core\":true,\"versionStr\":\"1.0.2\"},\"134\":{\"summary\":\"List, Edit and Add pages of a specific type\",\"core\":true,\"versionStr\":\"1.0.1\"},\"83\":{\"summary\":\"All page views are routed through this Process\",\"core\":true,\"versionStr\":\"1.0.4\"},\"136\":{\"summary\":\"Manage system permissions\",\"core\":true,\"versionStr\":\"1.0.1\"},\"138\":{\"summary\":\"Enables user to change their password, email address and other settings that you define.\",\"core\":true,\"versionStr\":\"1.0.3\"},\"159\":{\"summary\":\"Shows a list of recently edited pages in your admin.\",\"author\":\"Ryan Cramer\",\"href\":\"http:\\/\\/modules.processwire.com\\/\",\"core\":true,\"versionStr\":\"0.0.2\",\"permissions\":{\"page-edit-recent\":\"Can see recently edited pages\"},\"page\":{\"name\":\"recent-pages\",\"parent\":\"page\",\"title\":\"Recent\"}},\"68\":{\"summary\":\"Manage user roles and what permissions are attached\",\"core\":true,\"versionStr\":\"1.0.4\"},\"47\":{\"summary\":\"List and edit the templates that control page output\",\"core\":true,\"versionStr\":\"1.1.4\"},\"66\":{\"summary\":\"Manage system users\",\"core\":true,\"versionStr\":\"1.0.7\"},\"125\":{\"summary\":\"Throttles the frequency of logins for a given account, helps to reduce dictionary attacks by introducing an exponential delay between logins.\",\"core\":true,\"versionStr\":\"1.0.2\"},\"139\":{\"summary\":\"Manages system versions and upgrades.\",\"core\":true,\"versionStr\":\"0.1.6\"},\"61\":{\"summary\":\"Entity encode ampersands, quotes (single and double) and greater-than\\/less-than signs using htmlspecialchars(str, ENT_QUOTES). It is recommended that you use this on all text\\/textarea fields except those using a rich text editor or a markup language like Markdown.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"179\":{\"summary\":\"Duplicate, backup and transfer an entire site from one location to another.\",\"author\":\"flydev\",\"href\":\"https:\\/\\/processwire.com\\/talk\\/topic\\/15345-duplicator-backup-and-move-sites\\/\",\"versionStr\":\"1.2.9\"},\"180\":{\"summary\":\"Manage your backups\\/packages built with Duplicator inside ProcessWire.\",\"author\":\"flydev\",\"href\":\"https:\\/\\/processwire.com\\/talk\\/topic\\/15345-duplicator-backup-and-move-sites\\/\",\"versionStr\":\"1.2.9\",\"permissions\":{\"duplicator\":\"Run the Duplicator module\"},\"page\":{\"name\":\"duplicator\",\"parent\":\"setup\",\"title\":\"Duplicator\"}},\"178\":{\"summary\":\"Field that stores YAML data and formats it as an object, when requested.\",\"author\":\"owzim\",\"versionStr\":\"0.3.0\"},\"174\":{\"summary\":\"Adds shortcut edit link to all input fields on page edit. Adds shortcut edit link for the selected template. Only visible for superusers.\",\"href\":\"http:\\/\\/processwire.com\\/talk\\/topic\\/421-helperfieldlinks-field-and-template-edit-shortcuts\\/\",\"versionStr\":\"1.0.8\"},\"176\":{\"summary\":\"Generates an XML sitemap at yoursite.com\\/sitemap.xml for use with Google Webmaster Tools etc.\",\"href\":\"http:\\/\\/processwire.com\\/talk\\/index.php\\/topic,867.0.html\",\"versionStr\":\"1.1.0\"},\"173\":{\"summary\":\"Create and\\/or restore database backups from ProcessWire admin.\",\"author\":\"Ryan Cramer\",\"versionStr\":\"0.0.4\",\"permissions\":{\"db-backup\":\"Manage database backups (recommended for superuser only)\"},\"page\":{\"name\":\"db-backups\",\"parent\":\"setup\",\"title\":\"DB Backups\"}},\"182\":{\"summary\":\"Tool that helps you identify and install core and module upgrades.\",\"author\":\"Ryan Cramer\",\"versionStr\":\"0.0.7\"},\"183\":{\"summary\":\"Automatically checks for core and installed module upgrades at routine intervals.\",\"author\":\"Ryan Cramer\",\"versionStr\":\"0.0.7\"},\"175\":{\"summary\":\"Extends WireMail, uses SMTP protocol (plain | SSL | TLS), provides: to, cc, bcc, attachments, priority, disposition notification, bulksending, ...\",\"author\":\"horst\",\"href\":\"http:\\/\\/processwire.com\\/talk\\/topic\\/5704-module-wiremailsmtp\\/\",\"versionStr\":\"0.3.0\"}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__90e0fa97782426f9eaefea37ef8ea944', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"9a456aa6c977b2bc54f64156e5338003\",\"size\":1956,\"time\":1542312096,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"bdb3e7cbe178af9774bd024d0e17f2e1\",\"size\":2289,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.site/modules/', 'Duplicator/Duplicator.module\nDuplicator/ProcessDuplicator.module\nFieldtypeYaml/FieldtypeYaml.module\nHelloworld/Helloworld.module\nHelperFieldLinks/HelperFieldLinks.module\nMarkupSitemapXML/MarkupSitemapXML.module\nProcessDatabaseBackups/ProcessDatabaseBackups.module\nProcessWireUpgrade/ProcessWireUpgrade.module\nProcessWireUpgrade/ProcessWireUpgradeCheck.module\nWireMailSmtp/WireMailSmtp.module', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2891a70d10e7a6ca2527a36d6de69bc9', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1542312096,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__6fca0171abedbb9c5e31387e35b70164', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1523039149,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1523039149}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__45f330469c8da3ba4bb96ca9365ef775', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpAdaptor.php\",\"hash\":\"33b0828d5e139807897b0312953f2e79\",\"size\":15708,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpAdaptor.php\",\"hash\":\"0e468aa6b23937ad312d097348102a05\",\"size\":16614,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__38993b44ce8710ec41e375f526822126', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"447210ddea9dc0bc815c4f871e949319\",\"size\":6477,\"time\":1542310239,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"58bca7ab4ab7bd6142f4e81a3a9aad4e\",\"size\":6516,\"time\":1542310239}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__84edb210e2546ab8f0ea1a8a2465d986', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/templates\\/home.php\",\"hash\":\"3beefb00777a6bd04265b7d33c23efa9\",\"size\":7,\"time\":1542399199,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"3beefb00777a6bd04265b7d33c23efa9\",\"size\":7,\"time\":1542399199}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a0e6cedcc49ce28a5a39b08c041b50b9', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtp.module\",\"hash\":\"e382a2d0423183c7521e8643255d2c6e\",\"size\":34306,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtp.module\",\"hash\":\"3f1cf1ff2d981683a7e5c401b3156e3a\",\"size\":35028,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__7bb342d5bcf35b8b325c19b4e1643a11', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpConfig.php\",\"hash\":\"13c09ca2bd5625656ce786d9cc4113de\",\"size\":14370,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpConfig.php\",\"hash\":\"9f3b16b62cd6d489209fa6a1a541b96d\",\"size\":14565,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__252a7e73e74cfe0c851e97a38e9846e8', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/email_message.php\",\"hash\":\"5509de5f6ee8dc71b066dba00d7cd58f\",\"size\":121559,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/email_message.php\",\"hash\":\"9bbc832ccaa29bf04a2fe91478110bc9\",\"size\":121763,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__09c4122b9b673fc6be435d6b3ad718e0', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp_message.php\",\"hash\":\"43375d48bfee6397cf317b13230e78f0\",\"size\":24885,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp_message.php\",\"hash\":\"1f1d2e162c26d680e1403ad38823159b\",\"size\":25491,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c1a84bd8d5f0a856582b42264cf9436a', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp.php\",\"hash\":\"e3e1d60858ea13473fa4dd8235f17c9f\",\"size\":51914,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp.php\",\"hash\":\"e3e1d60858ea13473fa4dd8235f17c9f\",\"size\":51914,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__066509a6756a4604852092829f07e1e5', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/sasl.php\",\"hash\":\"3fce02fe61a0d90291086b55c1c2dfa2\",\"size\":11898,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/sasl.php\",\"hash\":\"843e748905597a053c57e17cb65c6318\",\"size\":11995,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b3542a6c3880882feca98dae9033d918', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"a306809bf7d98c551d13c92f6ffd528e\",\"size\":5138,\"time\":1542310762,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"47fa5e008b812951e7106f1b399982a6\",\"size\":5346,\"time\":1542310762}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ModulesUninstalled.info', '{\"FieldtypeCache\":{\"name\":\"FieldtypeCache\",\"title\":\"Cache\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Caches the values of other fields for fewer runtime queries. Can also be used to combine multiple text fields and have them all be searchable under the cached field name.\",\"created\":1542962837,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"CommentFilterAkismet\":{\"name\":\"CommentFilterAkismet\",\"title\":\"Comment Filter: Akismet\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Uses the Akismet service to identify comment spam. Module plugin for the Comments Fieldtype.\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1542962838,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeComments\":{\"name\":\"FieldtypeComments\",\"title\":\"Comments\",\"version\":107,\"versionStr\":\"1.0.7\",\"summary\":\"Field that stores user posted comments for a single Page\",\"installs\":[\"InputfieldCommentsAdmin\"],\"created\":1542962838,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldCommentsAdmin\":{\"name\":\"InputfieldCommentsAdmin\",\"title\":\"Comments Admin\",\"version\":104,\"versionStr\":\"1.0.4\",\"summary\":\"Provides an administrative interface for working with comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1542962838,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeOptions\":{\"name\":\"FieldtypeOptions\",\"title\":\"Select Options\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Field that stores single and multi select options.\",\"created\":1542962838,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypePageTable\":{\"name\":\"FieldtypePageTable\",\"title\":\"ProFields: Page Table\",\"version\":8,\"versionStr\":\"0.0.8\",\"summary\":\"A fieldtype containing a group of editable pages.\",\"installs\":[\"InputfieldPageTable\"],\"autoload\":true,\"created\":1542962838,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeFieldsetPage\":{\"name\":\"FieldtypeFieldsetPage\",\"title\":\"Fieldset (Page)\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Fieldset with fields isolated to separate namespace (page), enabling re-use of fields.\",\"requiresVersions\":{\"FieldtypeRepeater\":[\">=\",0]},\"autoload\":true,\"created\":1542962838,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeSelector\":{\"name\":\"FieldtypeSelector\",\"title\":\"Selector\",\"version\":13,\"versionStr\":\"0.1.3\",\"author\":\"Avoine + ProcessWire\",\"summary\":\"Build a page finding selector visually.\",\"created\":1542962838,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FileCompilerTags\":{\"name\":\"FileCompilerTags\",\"title\":\"Tags File Compiler\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Enables {var} or {var.property} variables in markup sections of a file. Can be used with any API variable.\",\"created\":1542962832,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ImageSizerEngineAnimatedGif\":{\"name\":\"ImageSizerEngineAnimatedGif\",\"title\":\"Animated GIF Image Sizer\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Horst Nogajski\",\"summary\":\"Upgrades image manipulations for animated GIFs.\",\"created\":1542962839,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ImageSizerEngineIMagick\":{\"name\":\"ImageSizerEngineIMagick\",\"title\":\"IMagick Image Sizer\",\"version\":2,\"versionStr\":\"0.0.2\",\"author\":\"Horst Nogajski\",\"summary\":\"Upgrades image manipulations to use PHP\'s ImageMagick library when possible.\",\"created\":1542962839,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldPageTable\":{\"name\":\"InputfieldPageTable\",\"title\":\"ProFields: Page Table\",\"version\":13,\"versionStr\":\"0.1.3\",\"summary\":\"Inputfield to accompany FieldtypePageTable\",\"requiresVersions\":{\"FieldtypePageTable\":[\">=\",0]},\"created\":1542962858,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LanguageSupportPageNames\":{\"name\":\"LanguageSupportPageNames\",\"title\":\"Languages Support - Page Names\",\"version\":9,\"versionStr\":\"0.0.9\",\"author\":\"Ryan Cramer\",\"summary\":\"Required to use multi-language page names.\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0],\"LanguageSupportFields\":[\">=\",0]},\"autoload\":true,\"singular\":true,\"created\":1542962862,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LanguageTabs\":{\"name\":\"LanguageTabs\",\"title\":\"Languages Support - Tabs\",\"version\":114,\"versionStr\":\"1.1.4\",\"author\":\"adamspruijt, ryan\",\"summary\":\"Organizes multi-language fields into tabs for a cleaner easier to use interface.\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"autoload\":\"template=admin\",\"singular\":true,\"created\":1542962862,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LazyCron\":{\"name\":\"LazyCron\",\"title\":\"Lazy Cron\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Provides hooks that are automatically executed at various intervals. It is called \'lazy\' because it\'s triggered by a pageview, so the interval is guaranteed to be at least the time requested, rather than exactly the time requested. This is fine for most cases, but you can make it not lazy by connecting this to a real CRON job. See the module file for details. \",\"href\":\"https:\\/\\/processwire.com\\/api\\/modules\\/lazy-cron\\/\",\"autoload\":true,\"singular\":true,\"created\":1542962832,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"MarkupPageFields\":{\"name\":\"MarkupPageFields\",\"title\":\"Markup Page Fields\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Adds $page->renderFields() and $page->images->render() methods that return basic markup for output during development and debugging.\",\"autoload\":true,\"singular\":true,\"created\":1542962863,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"permanent\":true},\"MarkupRSS\":{\"name\":\"MarkupRSS\",\"title\":\"Markup RSS Feed\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Renders an RSS feed. Given a PageArray, renders an RSS feed of them.\",\"created\":1542962863,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"PageFrontEdit\":{\"name\":\"PageFrontEdit\",\"title\":\"Front-End Page Editor\",\"version\":2,\"versionStr\":\"0.0.2\",\"author\":\"Ryan Cramer\",\"summary\":\"Enables front-end editing of page fields.\",\"icon\":\"cube\",\"permissions\":{\"page-edit-front\":\"Use the front-end page editor\"},\"autoload\":true,\"created\":1542962867,\"installed\":false,\"configurable\":\"PageFrontEditConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"license\":\"MPL 2.0\"},\"PagePathHistory\":{\"name\":\"PagePathHistory\",\"title\":\"Page Path History\",\"version\":2,\"versionStr\":\"0.0.2\",\"summary\":\"Keeps track of past URLs where pages have lived and automatically redirects (301 permament) to the new location whenever the past URL is accessed.\",\"autoload\":true,\"singular\":true,\"created\":1542962832,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"PagePaths\":{\"name\":\"PagePaths\",\"title\":\"Page Paths\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Enables page paths\\/urls to be queryable by selectors. Also offers potential for improved load performance. Builds an index at install (may take time on a large site). Currently supports only single languages sites.\",\"autoload\":true,\"singular\":true,\"created\":1542962832,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessCommentsManager\":{\"name\":\"ProcessCommentsManager\",\"title\":\"Comments\",\"version\":8,\"versionStr\":\"0.0.8\",\"author\":\"Ryan Cramer\",\"summary\":\"Manage comments in your site outside of the page editor.\",\"icon\":\"comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"permission\":\"comments-manager\",\"permissions\":{\"comments-manager\":\"Use the comments manager\"},\"created\":1542962867,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"comments\",\"parent\":\"setup\",\"title\":\"Comments\"},\"nav\":[{\"url\":\"?go=approved\",\"label\":\"Approved\"},{\"url\":\"?go=pending\",\"label\":\"Pending\"},{\"url\":\"?go=spam\",\"label\":\"Spam\"},{\"url\":\"?go=all\",\"label\":\"All\"}]},\"ProcessForgotPassword\":{\"name\":\"ProcessForgotPassword\",\"title\":\"Forgot Password\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Provides password reset\\/email capability for the Login process.\",\"permission\":\"page-view\",\"created\":1542962867,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessPageClone\":{\"name\":\"ProcessPageClone\",\"title\":\"Page Clone\",\"version\":103,\"versionStr\":\"1.0.3\",\"summary\":\"Provides ability to clone\\/copy\\/duplicate pages in the admin. Adds a &quot;copy&quot; option to all applicable pages in the PageList.\",\"permission\":\"page-clone\",\"permissions\":{\"page-clone\":\"Clone a page\",\"page-clone-tree\":\"Clone a tree of pages\"},\"autoload\":\"template=admin\",\"created\":1542962867,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"clone\",\"title\":\"Clone\",\"parent\":\"page\",\"status\":1024}},\"ProcessPagesExportImport\":{\"name\":\"ProcessPagesExportImport\",\"title\":\"Pages Export\\/Import\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Ryan Cramer\",\"summary\":\"Enables exporting and importing of pages. Development version, not yet recommended for production use.\",\"icon\":\"paper-plane-o\",\"permission\":\"page-edit-export\",\"created\":1542962869,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"export-import\",\"parent\":\"page\",\"title\":\"Export\\/Import\"}},\"ProcessSessionDB\":{\"name\":\"ProcessSessionDB\",\"title\":\"Sessions\",\"version\":3,\"versionStr\":\"0.0.3\",\"summary\":\"Enables you to browse active database sessions.\",\"icon\":\"dashboard\",\"requiresVersions\":{\"SessionHandlerDB\":[\">=\",0]},\"created\":1542962870,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"SessionHandlerDB\":{\"name\":\"SessionHandlerDB\",\"title\":\"Session Handler Database\",\"version\":5,\"versionStr\":\"0.0.5\",\"summary\":\"Installing this module makes ProcessWire store sessions in the database rather than the file system. Note that this module will log you out after install or uninstall.\",\"installs\":[\"ProcessSessionDB\"],\"created\":1542962870,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeNotifications\":{\"name\":\"FieldtypeNotifications\",\"title\":\"Notifications\",\"version\":4,\"versionStr\":\"0.0.4\",\"summary\":\"Field that stores user notifications.\",\"requiresVersions\":{\"SystemNotifications\":[\">=\",0]},\"created\":1542962870,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"SystemNotifications\":{\"name\":\"SystemNotifications\",\"title\":\"System Notifications\",\"version\":12,\"versionStr\":\"0.1.2\",\"summary\":\"Adds support for notifications in ProcessWire (currently in development)\",\"icon\":\"bell\",\"installs\":[\"FieldtypeNotifications\"],\"autoload\":true,\"created\":1542962870,\"installed\":false,\"configurable\":\"SystemNotificationsConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterMarkdownExtra\":{\"name\":\"TextformatterMarkdownExtra\",\"title\":\"Markdown\\/Parsedown Extra\",\"version\":130,\"versionStr\":\"1.3.0\",\"summary\":\"Markdown\\/Parsedown extra lightweight markup language by Emanuil Rusev. Based on Markdown by John Gruber.\",\"created\":1542962870,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterNewlineBR\":{\"name\":\"TextformatterNewlineBR\",\"title\":\"Newlines to XHTML Line Breaks\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to XHTML line break <br \\/> tags. \",\"created\":1542962870,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterNewlineUL\":{\"name\":\"TextformatterNewlineUL\",\"title\":\"Newlines to Unordered List\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to <li> list items and surrounds in an <ul> unordered list. \",\"created\":1542962870,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterPstripper\":{\"name\":\"TextformatterPstripper\",\"title\":\"Paragraph Stripper\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips paragraph <p> tags that may have been applied by other text formatters before it. \",\"created\":1542962870,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterSmartypants\":{\"name\":\"TextformatterSmartypants\",\"title\":\"SmartyPants Typographer\",\"version\":171,\"versionStr\":\"1.7.1\",\"summary\":\"Smart typography for web sites, by Michel Fortin based on SmartyPants by John Gruber. If combined with Markdown, it should be applied AFTER Markdown.\",\"created\":1542962871,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"url\":\"https:\\/\\/github.com\\/michelf\\/php-smartypants\"},\"TextformatterStripTags\":{\"name\":\"TextformatterStripTags\",\"title\":\"Strip Markup Tags\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips HTML\\/XHTML Markup Tags\",\"created\":1542962870,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"Helloworld\":{\"name\":\"Helloworld\",\"title\":\"Hello World\",\"version\":3,\"versionStr\":\"0.0.3\",\"summary\":\"An example module used for demonstration purposes.\",\"href\":\"https:\\/\\/processwire.com\",\"icon\":\"smile-o\",\"autoload\":true,\"singular\":true,\"created\":1542962722,\"installed\":false}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__640196ace9545186a9cba816602b3113', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"f858e3b40cd800dde05598d5f58926c1\",\"size\":3744,\"time\":1542312096,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"19e4ca2e539f2e20991aa25759aad4ab\",\"size\":4039,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ce29432100686e55b2eb80cba0701e85', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"07ff2191e35f5eff83db624f071cf004\",\"size\":21316,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"ea8bf95fae9e3f58769108a861639125\",\"size\":22421,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__98a5e66dd606f5d277b18ba2eeee2aa5', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"4b3a272433f78b0df205b5c993c90634\",\"size\":54385,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"6ae67b63e147b9da5e1a327060b9e76a\",\"size\":57936,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__97ca79731a988f4807cad97d797a0e97', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"d6a2e4d9f1815df7679a46b2498c4c87\",\"size\":8645,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"5aafcfabb3b48b69bc3e8e132be325f7\",\"size\":8684,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__94c78f6c77ce05907c6f10b89e94f55c', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"bf05a996f3b205ac1e535e118899d01b\",\"size\":1413,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"4cdce8a4606dabcfd7700b9272883aa5\",\"size\":1491,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e52e09c93dacc88c2f7b63dc86ce140b', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"f8ef48a73ab0a102eb1f2c71f06fe2e6\",\"size\":2118,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"64fbc5f8fab4346d7263db714cf05697\",\"size\":2157,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__60622dec7ebead07d1f8bc17af6278cf', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/templates\\/basic-page.php\",\"hash\":\"ce407ff5715c837d02b1aba7975bf512\",\"size\":6,\"time\":1542399219,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"ce407ff5715c837d02b1aba7975bf512\",\"size\":6,\"time\":1542399219}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Permissions.names', '{\"db-backup\":1021,\"duplicator\":1024,\"lang-edit\":1016,\"logs-edit\":1014,\"logs-view\":1013,\"page-delete\":34,\"page-edit\":32,\"page-edit-recent\":1011,\"page-lister\":1006,\"page-lock\":54,\"page-move\":35,\"page-sort\":50,\"page-template\":51,\"page-view\":36,\"profile-edit\":53,\"user-admin\":52}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d503d79fdd5a16951a40831b4979b1d3', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/templates\\/_init.php\",\"hash\":\"50603393c8117b612cb74ed899760001\",\"size\":955,\"time\":1542399440,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"19ad9e302bd026c3f1696b5b021e0162\",\"size\":1128,\"time\":1542399440}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__7eff4d3d26a5a9cb7616e823ed42dcbb', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/templates\\/_main.php\",\"hash\":\"228a3a6a03f4bc6546ab9f574b5452b1\",\"size\":1157,\"time\":1542747954,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"7e57d87b48f0989c01bcb5dedfe3704a\",\"size\":1596,\"time\":1542747954}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__51ff59ed91f7a606917b7cd788f4fef8', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1542399364,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1542399364}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__fb95bcd652d4190e4975d6c2e695bf02', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"099fb995346f31c749f6e40db0f395e3\",\"size\":6,\"time\":1542400439,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"099fb995346f31c749f6e40db0f395e3\",\"size\":6,\"time\":1542400439}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__711a20ca606e6671d50f352214b3eaf9', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"251d164643533a527361dbe1a7b9235d\",\"size\":6,\"time\":1542400413,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/fcs\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"251d164643533a527361dbe1a7b9235d\",\"size\":6,\"time\":1542400413}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e44d2db8318b9ed28cfb367f91580254', '{\"source\":{\"file\":\"views\\/home\\/home.view.php\",\"hash\":\"949b9ad31434073edb7777f2095e4fe0\",\"size\":1117,\"time\":1551212729,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/views\\/home\\/home.view.php\",\"hash\":\"949b9ad31434073edb7777f2095e4fe0\",\"size\":1117,\"time\":1551212729}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f77b12a794e34fead68eee757762d9ba', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"4b3a272433f78b0df205b5c993c90634\",\"size\":54385,\"time\":1542962722,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"6ae67b63e147b9da5e1a327060b9e76a\",\"size\":57936,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__00f09aff3a05dfdb0db5fbf1e87fe083', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"d6a2e4d9f1815df7679a46b2498c4c87\",\"size\":8645,\"time\":1542962722,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"5aafcfabb3b48b69bc3e8e132be325f7\",\"size\":8684,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__6cbf084ee2b0bd9ca1d6cb81598272c5', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"bf05a996f3b205ac1e535e118899d01b\",\"size\":1413,\"time\":1542962722,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"4cdce8a4606dabcfd7700b9272883aa5\",\"size\":1491,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e685df52439a3c81f1786d4e37456822', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"f8ef48a73ab0a102eb1f2c71f06fe2e6\",\"size\":2118,\"time\":1542962722,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"64fbc5f8fab4346d7263db714cf05697\",\"size\":2157,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__30c48180b40b78147c022cd2f55ac267', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"07ff2191e35f5eff83db624f071cf004\",\"size\":21316,\"time\":1542962722,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"ea8bf95fae9e3f58769108a861639125\",\"size\":22421,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d5e31810bb5d8c9056bb4d681742449b', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"447210ddea9dc0bc815c4f871e949319\",\"size\":6477,\"time\":1542962722,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"58bca7ab4ab7bd6142f4e81a3a9aad4e\",\"size\":6516,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__97220095c77f0a532b5937690ba50c71', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"a306809bf7d98c551d13c92f6ffd528e\",\"size\":5138,\"time\":1542962722,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"47fa5e008b812951e7106f1b399982a6\",\"size\":5346,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a4e30da2045e11d9cf251346ea01094f', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"f858e3b40cd800dde05598d5f58926c1\",\"size\":3744,\"time\":1542962722,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"19e4ca2e539f2e20991aa25759aad4ab\",\"size\":4039,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__14be93168b8c8b1c8646374203e28138', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"9a456aa6c977b2bc54f64156e5338003\",\"size\":1956,\"time\":1542962722,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"bdb3e7cbe178af9774bd024d0e17f2e1\",\"size\":2289,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__06b78479051302ced635ff87bdc2048d', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/_init.php\",\"hash\":\"c2cc083e2f5ff6abf095ff5f758b7dae\",\"size\":1052,\"time\":1543482754,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"a5a5104bc48332bbd101c7576d956b4c\",\"size\":1227,\"time\":1543482754}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e81dc9c542d0d7a6b2813b127e7b3ad5', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/_main.php\",\"hash\":\"c9feb2edc5ee0758c28d391fca412d17\",\"size\":2232,\"time\":1545283294,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"6df9871d34335dfb82b8efb5104f74c9\",\"size\":3213,\"time\":1545283294}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__3f04ea7794cb10c98218f8cfa90189d3', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1543482754,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1543482754}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c3b55c4fe58fc0570ee6044df7f88499', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1542962722,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__711ad8dd0c8e75ada69e2f1f87a947bd', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1542962722,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__756dc31d7215f00b3383a203a86f87e8', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"8d49407da8f8ef621cf6618e138a635b\",\"size\":3147,\"time\":1543618190,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"8d49407da8f8ef621cf6618e138a635b\",\"size\":3147,\"time\":1543618190}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__6e098702dee2e132823baadd941a24c0', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"02a297456781aac9a05c4153c1895e86\",\"size\":649,\"time\":1543618190,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"02a297456781aac9a05c4153c1895e86\",\"size\":649,\"time\":1543618190}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__6fc61dd7db9c05ef7c075904d758de75', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1543482754,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1543482754}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bac66b979afa1fc3c1bc3b74da9873b8', '{\"source\":{\"file\":\"views\\/basic-page\\/basic-page.view.php\",\"hash\":\"e634aba121366b73cb53eefdd4a71d86\",\"size\":2668,\"time\":1552396316,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/views\\/basic-page\\/basic-page.view.php\",\"hash\":\"08c2cc67f12f35d488e9c2e9928cd3e3\",\"size\":3073,\"time\":1552396316}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__70ecdf03abf4b69f3b4a4e5010d14c00', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1542962722,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__024f5b324e90adde444df659a6f2d8b6', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtp.module\",\"hash\":\"e382a2d0423183c7521e8643255d2c6e\",\"size\":34306,\"time\":1542962722,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtp.module\",\"hash\":\"3f1cf1ff2d981683a7e5c401b3156e3a\",\"size\":35028,\"time\":1542962722}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__746b59175f9dd36c3ee5c4163421e295', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"4bf1281b852ab97c251f43067e79943e\",\"size\":162,\"time\":1543864333,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"63baa22625d09632cb303f234b30efad\",\"size\":341,\"time\":1543864333}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ab2a7954164587e8d77332adf57cef4d', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/carousel\\/carousel.php\",\"hash\":\"c5c8d404faa97d17fedc7ab3a7450425\",\"size\":3743,\"time\":1544126546,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/carousel\\/carousel.php\",\"hash\":\"c5c8d404faa97d17fedc7ab3a7450425\",\"size\":3743,\"time\":1544126546}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1c6e1c382fa07d26bf9e998fef2606dc', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/news-cards\\/news-cards.php\",\"hash\":\"c7d82dc90ce2f69ca0a2a42bb25f7978\",\"size\":96,\"time\":1543864333,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/news-cards\\/news-cards.php\",\"hash\":\"c7d82dc90ce2f69ca0a2a42bb25f7978\",\"size\":96,\"time\":1543864333}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1e3ba1b5f9ebd43b50fa3f0ad875d464', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/advantages\\/advantages.php\",\"hash\":\"38a9b94a2a1ef6c2c00baf0e90552e63\",\"size\":19,\"time\":1543868810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/advantages\\/advantages.php\",\"hash\":\"38a9b94a2a1ef6c2c00baf0e90552e63\",\"size\":19,\"time\":1543868810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__6c68e90c613316d1404f6a9aaf511df0', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"e90cb04b6e85b3bef9fe93ef264717a6\",\"size\":2389,\"time\":1544301046,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"e90cb04b6e85b3bef9fe93ef264717a6\",\"size\":2389,\"time\":1544301046}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d87918393a3f47ce94bcbc5e5511972e', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/pub-cards\\/pub-cards.php\",\"hash\":\"6b7de5af92be539e8b347f80f193d9ea\",\"size\":2596,\"time\":1544301046,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/pub-cards\\/pub-cards.php\",\"hash\":\"6b7de5af92be539e8b347f80f193d9ea\",\"size\":2596,\"time\":1544301046}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1578e1c8ba04c18354386bfa9da6bb35', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"5f0e2018faad229607c4db2c0501c962\",\"size\":1337,\"time\":1544128353,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"5f0e2018faad229607c4db2c0501c962\",\"size\":1337,\"time\":1544128353}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__156eb4b4808192e23d531f21663e1f52', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/static-cards\\/static-cards.php\",\"hash\":\"8221c67563f609a507bfa42743681b35\",\"size\":2567,\"time\":1544301239,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/static-cards\\/static-cards.php\",\"hash\":\"8221c67563f609a507bfa42743681b35\",\"size\":2567,\"time\":1544301239}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__cc781cb7cd40ebd495c6b4f04dc0f8b6', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/blog\\/blog.php\",\"hash\":\"d9fc8ec4e65511cc81500f39599122f7\",\"size\":2668,\"time\":1544301356,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/blog\\/blog.php\",\"hash\":\"d9fc8ec4e65511cc81500f39599122f7\",\"size\":2668,\"time\":1544301356}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__11e2fad77ffdb8af4c4d015830a767be', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/content-tabs\\/content-tabs.php\",\"hash\":\"2427ea14f50de5e025bd86755c51c2ca\",\"size\":96,\"time\":1543864333,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/content-tabs\\/content-tabs.php\",\"hash\":\"2427ea14f50de5e025bd86755c51c2ca\",\"size\":96,\"time\":1543864333}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__53ce20bfc4a3fd5da1e412892e0f9dc0', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/tabs\\/tabs.php\",\"hash\":\"5f98e44bdbcef30f158834912bc4d429\",\"size\":2217,\"time\":1544301476,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/tabs\\/tabs.php\",\"hash\":\"5f98e44bdbcef30f158834912bc4d429\",\"size\":2217,\"time\":1544301476}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__19162792f6a0a15073682d36d03377f8', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/gallery\\/gallery.php\",\"hash\":\"b357bd0ea5941a75510eaccaf462bc36\",\"size\":1607,\"time\":1544301576,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/gallery\\/gallery.php\",\"hash\":\"b357bd0ea5941a75510eaccaf462bc36\",\"size\":1607,\"time\":1544301576}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ef464aae7aa216f78ccaeb3edfc50444', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgrade.module\",\"hash\":\"66cc6ed58e83f659bc4a51665b4d2d83\",\"size\":26751,\"time\":1544459120,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgrade.module\",\"hash\":\"e755d22d24df133c8b6ce26f22e281c1\",\"size\":27050,\"time\":1544459120}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5bd182525b4d7576c3e3d61b18c952da', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"53718147e5a1797508754e5af4b8e65a\",\"size\":11548,\"time\":1544469770,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"7a6e8d63f0cc239af9ddd07fc46c6175\",\"size\":11639,\"time\":1544469770}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__78c511bf6fe637923abd5c430f4f4030', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"395d9f4cc277ebe83c0f7d11a3eabf44\",\"size\":643,\"time\":1544469770,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"a17712b4a5db5fac542d046390afc8ab\",\"size\":656,\"time\":1544469770}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a940756ec749195b83a70365af22ae13', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"e2525a6ef3cb321a71a36b6ec8f37135\",\"size\":6631,\"time\":1545334824,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"e2525a6ef3cb321a71a36b6ec8f37135\",\"size\":6631,\"time\":1545334824}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__35d7d2c797093849aecb6ae10688dc51', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/map\\/map.php\",\"hash\":\"d907fd172ccb2ac75cfc071653acd551\",\"size\":670,\"time\":1545247349,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/map\\/map.php\",\"hash\":\"d907fd172ccb2ac75cfc071653acd551\",\"size\":670,\"time\":1545247349}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d30b907382c31e2022d36a6f13c764ab', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"81ec7c0e727415fbb37fe56beb47e1a6\",\"size\":1959,\"time\":1545332970,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"81ec7c0e727415fbb37fe56beb47e1a6\",\"size\":1959,\"time\":1545332970}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__490559e1d9526c7b981b39de0bacedc9', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/templates\\/mail.php\",\"hash\":\"76a5032974d79f2b2284f542fef33cbb\",\"size\":73,\"time\":1545339429,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/template\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/mail.php\",\"hash\":\"76a5032974d79f2b2284f542fef33cbb\",\"size\":73,\"time\":1545339429}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5f06492c6adf08a298b4c29a93f932ed', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"4b3a272433f78b0df205b5c993c90634\",\"size\":54385,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"6ae67b63e147b9da5e1a327060b9e76a\",\"size\":57936,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__6d1f7629b8042bb5559759a5d214c76b', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"d6a2e4d9f1815df7679a46b2498c4c87\",\"size\":8645,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"5aafcfabb3b48b69bc3e8e132be325f7\",\"size\":8684,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__0ecf6d0b2ca6bffe7a0a2e256f792a7f', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"bf05a996f3b205ac1e535e118899d01b\",\"size\":1413,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"4cdce8a4606dabcfd7700b9272883aa5\",\"size\":1491,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__487dba332a9b47efa8cd09b6e9b2f8d7', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"f8ef48a73ab0a102eb1f2c71f06fe2e6\",\"size\":2118,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"64fbc5f8fab4346d7263db714cf05697\",\"size\":2157,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__575607afb7fbb4cec9784fa3c6891553', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"07ff2191e35f5eff83db624f071cf004\",\"size\":21316,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"ea8bf95fae9e3f58769108a861639125\",\"size\":22421,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__294e92cc54c94b51797eac9b8413753b', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"447210ddea9dc0bc815c4f871e949319\",\"size\":6477,\"time\":1542310239,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"58bca7ab4ab7bd6142f4e81a3a9aad4e\",\"size\":6516,\"time\":1542310239}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e9aeed48ea1315a7a81ddb1234b11068', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"a306809bf7d98c551d13c92f6ffd528e\",\"size\":5138,\"time\":1542310762,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"47fa5e008b812951e7106f1b399982a6\",\"size\":5346,\"time\":1542310762}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2005269c24e25208f01557009d02f441', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"f858e3b40cd800dde05598d5f58926c1\",\"size\":3744,\"time\":1542312096,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"19e4ca2e539f2e20991aa25759aad4ab\",\"size\":4039,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__768d480432769d280a96239f612fe2f5', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"9a456aa6c977b2bc54f64156e5338003\",\"size\":1956,\"time\":1542312096,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"bdb3e7cbe178af9774bd024d0e17f2e1\",\"size\":2289,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__4b9325cef26286f84e092d36c14feca4', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/_init.php\",\"hash\":\"c2cc083e2f5ff6abf095ff5f758b7dae\",\"size\":1052,\"time\":1543517811,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"a5a5104bc48332bbd101c7576d956b4c\",\"size\":1227,\"time\":1543517811}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__8116d7dd0e1fa07f6952c0629e607f7f', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/_main.php\",\"hash\":\"30906d217880cf1cd1aa7debc75393d2\",\"size\":2237,\"time\":1548361697,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"5759db9d97ef94d9ff2592bea8855566\",\"size\":3218,\"time\":1548361697}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2b33a30a6f5e7ed86cc8b1e56ae81730', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1543517811,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1543517811}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a02b39f3615f5e03b946eac80c914c4a', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1542312096,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__3e92dda55a2d15801d4dd34502d1a97e', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1542399364,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1542399364}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b938e915d0289f33f66ca33cace96455', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"81ec7c0e727415fbb37fe56beb47e1a6\",\"size\":1959,\"time\":1545332970,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"81ec7c0e727415fbb37fe56beb47e1a6\",\"size\":1959,\"time\":1545332970}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2c2563d1e2db8043bffbcb422c28e14c', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"28d03cc7adea3058fa2cea05826dffe8\",\"size\":3247,\"time\":1549696525,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"28d03cc7adea3058fa2cea05826dffe8\",\"size\":3247,\"time\":1549696525}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__035f0a0763a23c5631b82f0f15031321', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"4bf1281b852ab97c251f43067e79943e\",\"size\":162,\"time\":1543864333,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"63baa22625d09632cb303f234b30efad\",\"size\":341,\"time\":1543864333}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b40f3130917ba2f0bb37ae0f88735469', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/carousel\\/carousel.php\",\"hash\":\"2374eeb74a4c3efeffb2c04c203ffad0\",\"size\":3978,\"time\":1548360735,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/carousel\\/carousel.php\",\"hash\":\"2374eeb74a4c3efeffb2c04c203ffad0\",\"size\":3978,\"time\":1548360735}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d622622550f42397ab600fa96cfe5b26', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"e90cb04b6e85b3bef9fe93ef264717a6\",\"size\":2389,\"time\":1544301046,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"e90cb04b6e85b3bef9fe93ef264717a6\",\"size\":2389,\"time\":1544301046}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ef636ea979d415baed779d490457a19f', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/pub-cards\\/pub-cards.php\",\"hash\":\"6b7de5af92be539e8b347f80f193d9ea\",\"size\":2596,\"time\":1544301046,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/pub-cards\\/pub-cards.php\",\"hash\":\"6b7de5af92be539e8b347f80f193d9ea\",\"size\":2596,\"time\":1544301046}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a16069a95b3b377c72dd22a63b2bc40c', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"5f0e2018faad229607c4db2c0501c962\",\"size\":1337,\"time\":1544128353,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"5f0e2018faad229607c4db2c0501c962\",\"size\":1337,\"time\":1544128353}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1270aabd9705e6175765553c08f3f146', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/static-cards\\/static-cards.php\",\"hash\":\"8221c67563f609a507bfa42743681b35\",\"size\":2567,\"time\":1544301239,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/static-cards\\/static-cards.php\",\"hash\":\"8221c67563f609a507bfa42743681b35\",\"size\":2567,\"time\":1544301239}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__7ae50772668cf3ef85f3232cef6e5a70', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/blog\\/blog.php\",\"hash\":\"d9fc8ec4e65511cc81500f39599122f7\",\"size\":2668,\"time\":1544301356,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/blog\\/blog.php\",\"hash\":\"d9fc8ec4e65511cc81500f39599122f7\",\"size\":2668,\"time\":1544301356}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__87c3ff57ccf9d713411db5c66ecfff53', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/tabs\\/tabs.php\",\"hash\":\"5f98e44bdbcef30f158834912bc4d429\",\"size\":2217,\"time\":1544301476,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/tabs\\/tabs.php\",\"hash\":\"5f98e44bdbcef30f158834912bc4d429\",\"size\":2217,\"time\":1544301476}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__cbc3dc4018d67c00e35c519f864037da', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/gallery\\/gallery.php\",\"hash\":\"b357bd0ea5941a75510eaccaf462bc36\",\"size\":1607,\"time\":1544301576,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/gallery\\/gallery.php\",\"hash\":\"b357bd0ea5941a75510eaccaf462bc36\",\"size\":1607,\"time\":1544301576}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1c4b8370c5c8e170dea341d17ff9156d', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"e2525a6ef3cb321a71a36b6ec8f37135\",\"size\":6631,\"time\":1545334824,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"e2525a6ef3cb321a71a36b6ec8f37135\",\"size\":6631,\"time\":1545334824}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__65e298afb807458613121d7637a8be67', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/map\\/map.php\",\"hash\":\"d907fd172ccb2ac75cfc071653acd551\",\"size\":670,\"time\":1545247349,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/map\\/map.php\",\"hash\":\"d907fd172ccb2ac75cfc071653acd551\",\"size\":670,\"time\":1545247349}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__55d04a3d6524a74f77de416345b20e85', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"02a297456781aac9a05c4153c1895e86\",\"size\":649,\"time\":1543618190,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"02a297456781aac9a05c4153c1895e86\",\"size\":649,\"time\":1543618190}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b982be7afadf4c91e1fdc6cf38cfb056', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"53718147e5a1797508754e5af4b8e65a\",\"size\":11548,\"time\":1544469770,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"7a6e8d63f0cc239af9ddd07fc46c6175\",\"size\":11639,\"time\":1544469770}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__abc2cddd2414b6380aaad8001268e400', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"395d9f4cc277ebe83c0f7d11a3eabf44\",\"size\":643,\"time\":1544469770,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"a17712b4a5db5fac542d046390afc8ab\",\"size\":656,\"time\":1544469770}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f98daa39822570817ad54272c0d2fa44', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1523039149,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1523039149}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1852c5d69e9ca0ceae055be9507c11aa', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1543517811,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1543517811}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__97c98bbf2e37b5818ca91945246d7487', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/mail.php\",\"hash\":\"215a0e92fbcb002559dabc6008286ffd\",\"size\":56,\"time\":1548971551,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/mail.php\",\"hash\":\"215a0e92fbcb002559dabc6008286ffd\",\"size\":56,\"time\":1548971551}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__16d4ed100c7744f6bcc06e8c4a4698a1', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/parts\\/categories\\/blog-category.php\",\"hash\":\"3a4ab8b9c4058ff757c60497029db5be\",\"size\":1722,\"time\":1549698323,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/categories\\/blog-category.php\",\"hash\":\"3a4ab8b9c4058ff757c60497029db5be\",\"size\":1722,\"time\":1549698323}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__7aeba4ba4a0a71464231fb40577201b4', '{\"source\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/parts\\/components\\/pagination.php\",\"hash\":\"4267954a1c20dffac5b08051b00fd62e\",\"size\":838,\"time\":1549700932,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/components\\/pagination.php\",\"hash\":\"4267954a1c20dffac5b08051b00fd62e\",\"size\":838,\"time\":1549700932}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__3cbc5301df415983adef5567570d7d2f', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"4b3a272433f78b0df205b5c993c90634\",\"size\":54385,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"6ae67b63e147b9da5e1a327060b9e76a\",\"size\":57936,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__56ea539afef68788b8849f318510eb3a', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"d6a2e4d9f1815df7679a46b2498c4c87\",\"size\":8645,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"5aafcfabb3b48b69bc3e8e132be325f7\",\"size\":8684,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__23bdef9d596138fffe467ab0c6153c91', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"bf05a996f3b205ac1e535e118899d01b\",\"size\":1413,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"4cdce8a4606dabcfd7700b9272883aa5\",\"size\":1491,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__180439274e3e12a27dfa5f0143cb66bc', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"f8ef48a73ab0a102eb1f2c71f06fe2e6\",\"size\":2118,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"64fbc5f8fab4346d7263db714cf05697\",\"size\":2157,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b4b140f467b0db6fec5e63d33e236f32', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"07ff2191e35f5eff83db624f071cf004\",\"size\":21316,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"ea8bf95fae9e3f58769108a861639125\",\"size\":22421,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1a51e698e502158b1a629646e53614c5', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"447210ddea9dc0bc815c4f871e949319\",\"size\":6477,\"time\":1542310239,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"58bca7ab4ab7bd6142f4e81a3a9aad4e\",\"size\":6516,\"time\":1542310239}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__cf191c25f9eb4e449c2ea849a2c2b73e', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"a306809bf7d98c551d13c92f6ffd528e\",\"size\":5138,\"time\":1542310762,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"47fa5e008b812951e7106f1b399982a6\",\"size\":5346,\"time\":1542310762}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__cdcb4c35ebbd999a9a062e87cf48ce79', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"f858e3b40cd800dde05598d5f58926c1\",\"size\":3744,\"time\":1542312096,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"19e4ca2e539f2e20991aa25759aad4ab\",\"size\":4039,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__10440494e9fdac18f03218544d92fbad', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"9a456aa6c977b2bc54f64156e5338003\",\"size\":1956,\"time\":1542312096,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"bdb3e7cbe178af9774bd024d0e17f2e1\",\"size\":2289,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2cc8d9f0e735c57916208d775829f4e5', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/_init.php\",\"hash\":\"bbf69a97668920f5bd69fa5fb82ced93\",\"size\":1045,\"time\":1551211863,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"a8572b69c1249be46efa3979c8d27eb5\",\"size\":1220,\"time\":1551211863}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__af123ffc50f964e2707beb44e458ee70', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/_main.php\",\"hash\":\"d08c3f409251ed6807bdf664c120e401\",\"size\":2951,\"time\":1551213653,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"9cf0e17dea5159d025698047836f8b3e\",\"size\":3932,\"time\":1551213653}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5051e9c0934fcc83cc7f2925e88e4ec2', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1543517811,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1543517811}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__4daa6defbed33c47aab76bb4cf50b5a9', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1542312096,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__da9cf9eb3c949af00f70053ada22d923', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1542399364,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1542399364}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__8295249ef6909b034fe1661662ab3bf5', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"d0ebac99bcb48c394d308b26192ea3d5\",\"size\":2066,\"time\":1549743070,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"d0ebac99bcb48c394d308b26192ea3d5\",\"size\":2066,\"time\":1549743070}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__3faa58a3e8ad918633806da3c3cc1360', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"55b042c35212425e11b168b72c09e9bb\",\"size\":3815,\"time\":1551213653,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"55b042c35212425e11b168b72c09e9bb\",\"size\":3815,\"time\":1551213653}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2176e6136469c2388152accb332fbbd3', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"4bf1281b852ab97c251f43067e79943e\",\"size\":162,\"time\":1543864333,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"63baa22625d09632cb303f234b30efad\",\"size\":341,\"time\":1543864333}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__195fd86eaae7578650f6dfe77922cd45', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/carousel\\/carousel.php\",\"hash\":\"2374eeb74a4c3efeffb2c04c203ffad0\",\"size\":3978,\"time\":1548360735,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/carousel\\/carousel.php\",\"hash\":\"2374eeb74a4c3efeffb2c04c203ffad0\",\"size\":3978,\"time\":1548360735}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b926bcca63b3713c09770a7636cadef4', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"44ea3e2fd629a1eb9f42f4b44d99441f\",\"size\":2411,\"time\":1550004409,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"44ea3e2fd629a1eb9f42f4b44d99441f\",\"size\":2411,\"time\":1550004409}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c00eb1d427ee95122c516d7790e071ca', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"8d73281e8c69bef03a7e54c953d7e955\",\"size\":1688,\"time\":1550431356,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"8d73281e8c69bef03a7e54c953d7e955\",\"size\":1688,\"time\":1550431356}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__7b6de0e3cf3cc016113f602b87cb16e9', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/blog\\/blog.php\",\"hash\":\"9bc3c0fce24328c2db3fb491c6537325\",\"size\":2677,\"time\":1549721712,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/blog\\/blog.php\",\"hash\":\"9bc3c0fce24328c2db3fb491c6537325\",\"size\":2677,\"time\":1549721712}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__98629216028e0d8dccc2d6c80edac9b8', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/tabs\\/tabs.php\",\"hash\":\"5f98e44bdbcef30f158834912bc4d429\",\"size\":2217,\"time\":1544301476,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/tabs\\/tabs.php\",\"hash\":\"5f98e44bdbcef30f158834912bc4d429\",\"size\":2217,\"time\":1544301476}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b774fbb048d5ed2b96e6da4a99f8d526', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"e6401c4a85728580346eb0ec98f99b31\",\"size\":7351,\"time\":1549743070,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"e6401c4a85728580346eb0ec98f99b31\",\"size\":7351,\"time\":1549743070}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__8bef33b0d161ac785ec73d21f0788b17', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"8629ce50be5feb72805549a880cda974\",\"size\":689,\"time\":1551213159,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"8629ce50be5feb72805549a880cda974\",\"size\":689,\"time\":1551213159}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__be247ffa22955d79a3ac973db65960d1', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1543517811,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1543517811}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c190f4429ec3b2230d41958cd9b9d6ba', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/parts\\/categories\\/blog-category.php\",\"hash\":\"e475eeafe2539148ac6d7dbf0e2f7740\",\"size\":1987,\"time\":1551212943,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/categories\\/blog-category.php\",\"hash\":\"e475eeafe2539148ac6d7dbf0e2f7740\",\"size\":1987,\"time\":1551212943}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c609024c9b57091823de425d5229ec52', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/parts\\/components\\/pagination.php\",\"hash\":\"864ca1bdecbe9aa16ca057c5879446c3\",\"size\":897,\"time\":1549721566,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/components\\/pagination.php\",\"hash\":\"864ca1bdecbe9aa16ca057c5879446c3\",\"size\":897,\"time\":1549721566}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1273d8e20a75323dc3cd64802b3c53b6', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"53718147e5a1797508754e5af4b8e65a\",\"size\":11548,\"time\":1544469770,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"7a6e8d63f0cc239af9ddd07fc46c6175\",\"size\":11639,\"time\":1544469770}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__77d93eadb4948786e2031aa2850868c4', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"395d9f4cc277ebe83c0f7d11a3eabf44\",\"size\":643,\"time\":1544469770,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"a17712b4a5db5fac542d046390afc8ab\",\"size\":656,\"time\":1544469770}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__0248b84e8f47c24db7823eb43a548247', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1523039149,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1523039149}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__99e44c313ba22e6bc02a06f755463425', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/templates\\/mail.php\",\"hash\":\"95671c3b28b8aa10945c9740bf1ed7a9\",\"size\":1918,\"time\":1549746181,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/mail.php\",\"hash\":\"22e2fb0af0929e6d763b75ece054df2e\",\"size\":1931,\"time\":1549746181}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__839b4af84d07db4254068d6057e0a5fb', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtp.module\",\"hash\":\"e382a2d0423183c7521e8643255d2c6e\",\"size\":34306,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtp.module\",\"hash\":\"3f1cf1ff2d981683a7e5c401b3156e3a\",\"size\":35028,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__4cac9653a7bd5ee810ad315de4eb0ffd', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpConfig.php\",\"hash\":\"13c09ca2bd5625656ce786d9cc4113de\",\"size\":14370,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpConfig.php\",\"hash\":\"9f3b16b62cd6d489209fa6a1a541b96d\",\"size\":14565,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e36876b0f96e38071b9d17182bc18446', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpAdaptor.php\",\"hash\":\"33b0828d5e139807897b0312953f2e79\",\"size\":15708,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpAdaptor.php\",\"hash\":\"0e468aa6b23937ad312d097348102a05\",\"size\":16614,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a036c00482791c09fb0016261d8d5604', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/email_message.php\",\"hash\":\"5509de5f6ee8dc71b066dba00d7cd58f\",\"size\":121559,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/email_message.php\",\"hash\":\"9bbc832ccaa29bf04a2fe91478110bc9\",\"size\":121763,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__498e231463b433bf00af99112337280c', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp_message.php\",\"hash\":\"43375d48bfee6397cf317b13230e78f0\",\"size\":24885,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp_message.php\",\"hash\":\"1f1d2e162c26d680e1403ad38823159b\",\"size\":25491,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1fedd5341bb572f8bdc5f6dfd84b73c8', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp.php\",\"hash\":\"e3e1d60858ea13473fa4dd8235f17c9f\",\"size\":51914,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp.php\",\"hash\":\"e3e1d60858ea13473fa4dd8235f17c9f\",\"size\":51914,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f8afb3ae6d60ce7eb739944ae30db4d5', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/sasl.php\",\"hash\":\"3fce02fe61a0d90291086b55c1c2dfa2\",\"size\":11898,\"time\":1542310290,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/gruz\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/sasl.php\",\"hash\":\"843e748905597a053c57e17cb65c6318\",\"size\":11995,\"time\":1542310290}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c7f3a8334322f0feca43d00b33ada0a3', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"4b3a272433f78b0df205b5c993c90634\",\"size\":54385,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"6ae67b63e147b9da5e1a327060b9e76a\",\"size\":57936,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2644d0ecc312723a46b577b36f277e64', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"d6a2e4d9f1815df7679a46b2498c4c87\",\"size\":8645,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"5aafcfabb3b48b69bc3e8e132be325f7\",\"size\":8684,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__4a2ba4220c41eee0adf534e891bc565d', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"bf05a996f3b205ac1e535e118899d01b\",\"size\":1413,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"4cdce8a4606dabcfd7700b9272883aa5\",\"size\":1491,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__9e5d3481f06c29675c3e63e34d9f1952', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"f8ef48a73ab0a102eb1f2c71f06fe2e6\",\"size\":2118,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"64fbc5f8fab4346d7263db714cf05697\",\"size\":2157,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f57c7c56fdb593d754ab4ec50acec823', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"07ff2191e35f5eff83db624f071cf004\",\"size\":21316,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"ea8bf95fae9e3f58769108a861639125\",\"size\":22421,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__914cac8aeeb2194c5bc2ccbab6852879', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"447210ddea9dc0bc815c4f871e949319\",\"size\":6477,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"58bca7ab4ab7bd6142f4e81a3a9aad4e\",\"size\":6516,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__88b43a6abff103c2ece853f263b4ab67', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"a306809bf7d98c551d13c92f6ffd528e\",\"size\":5138,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"47fa5e008b812951e7106f1b399982a6\",\"size\":5346,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d0260d40440aadd41b61b283d57267de', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"f858e3b40cd800dde05598d5f58926c1\",\"size\":3744,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"19e4ca2e539f2e20991aa25759aad4ab\",\"size\":4039,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__89ea5345aaafab245f9cb5db719ba265', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"9a456aa6c977b2bc54f64156e5338003\",\"size\":1956,\"time\":1549827827,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"bdb3e7cbe178af9774bd024d0e17f2e1\",\"size\":2289,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__32d97d4bf851760096e62e1258738073', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"53718147e5a1797508754e5af4b8e65a\",\"size\":11548,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"7a6e8d63f0cc239af9ddd07fc46c6175\",\"size\":11639,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__43873fb8717ab8c0c19607845cb9e8e2', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"395d9f4cc277ebe83c0f7d11a3eabf44\",\"size\":643,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"a17712b4a5db5fac542d046390afc8ab\",\"size\":656,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__4677386cb490c7f4c41caf86b3eef8a5', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1549827827,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b053b52793ab3bebc32341f2bc6037a7', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/_init.php\",\"hash\":\"c2cc083e2f5ff6abf095ff5f758b7dae\",\"size\":1052,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"a5a5104bc48332bbd101c7576d956b4c\",\"size\":1227,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__8c3b17acef5ef05cc6f134a0296d8fef', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/_main.php\",\"hash\":\"30906d217880cf1cd1aa7debc75393d2\",\"size\":2237,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"5759db9d97ef94d9ff2592bea8855566\",\"size\":3218,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__0b5d02594f96eb7a4481fa75459b74d4', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__59959436724e596db851744bd7e5b3c8', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1549827827,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f41bfe8144207909ac75cdc02decf5f0', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__fb652dbc415ce3cff7f175bcaa9b4319', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"d0ebac99bcb48c394d308b26192ea3d5\",\"size\":2066,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"d0ebac99bcb48c394d308b26192ea3d5\",\"size\":2066,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f45c2b49740334dfd592643d81260bc5', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"9df809c423c07512b26e3bde455b9fc2\",\"size\":3691,\"time\":1550213502,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"9df809c423c07512b26e3bde455b9fc2\",\"size\":3691,\"time\":1550213502}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d11e26530918ebe9a432aedb62fe6cfe', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"4bf1281b852ab97c251f43067e79943e\",\"size\":162,\"time\":1543864333,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"63baa22625d09632cb303f234b30efad\",\"size\":341,\"time\":1543864333}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d08635c8c6452e3cfbd2744626584d8d', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/modules\\/carousel\\/carousel.php\",\"hash\":\"2374eeb74a4c3efeffb2c04c203ffad0\",\"size\":3978,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/carousel\\/carousel.php\",\"hash\":\"2374eeb74a4c3efeffb2c04c203ffad0\",\"size\":3978,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a475cac779ad976fc09d58e876e08c79', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"44ea3e2fd629a1eb9f42f4b44d99441f\",\"size\":2411,\"time\":1550004409,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"44ea3e2fd629a1eb9f42f4b44d99441f\",\"size\":2411,\"time\":1550004409}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c3d62876f3a06dbe6cfb0729507f6d13', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"8d73281e8c69bef03a7e54c953d7e955\",\"size\":1688,\"time\":1550431356,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"8d73281e8c69bef03a7e54c953d7e955\",\"size\":1688,\"time\":1550431356}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__566a66ff945500574bb24a124e6e4ff8', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/modules\\/blog\\/blog.php\",\"hash\":\"9bc3c0fce24328c2db3fb491c6537325\",\"size\":2677,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/blog\\/blog.php\",\"hash\":\"9bc3c0fce24328c2db3fb491c6537325\",\"size\":2677,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__4853f1f68ce5d82d798b4751a6042e83', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/modules\\/tabs\\/tabs.php\",\"hash\":\"5f98e44bdbcef30f158834912bc4d429\",\"size\":2217,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/tabs\\/tabs.php\",\"hash\":\"5f98e44bdbcef30f158834912bc4d429\",\"size\":2217,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__758482700bda83ebee5a157c7318defb', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"e6401c4a85728580346eb0ec98f99b31\",\"size\":7351,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"e6401c4a85728580346eb0ec98f99b31\",\"size\":7351,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__be69b25f66b29afcb872f1f7b7c172ef', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"02a297456781aac9a05c4153c1895e86\",\"size\":649,\"time\":1543618190,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"02a297456781aac9a05c4153c1895e86\",\"size\":649,\"time\":1543618190}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e0747c04ff55d41ea343dd438f135ef1', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__397a519e9777d0458ccabc680f8acf85', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/parts\\/categories\\/blog-category.php\",\"hash\":\"dd39033dc670c486ff8183ff7a2148b3\",\"size\":1972,\"time\":1550004074,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/categories\\/blog-category.php\",\"hash\":\"dd39033dc670c486ff8183ff7a2148b3\",\"size\":1972,\"time\":1550004074}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__cbd6923542606266cbeaeebf282554b3', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/parts\\/components\\/pagination.php\",\"hash\":\"864ca1bdecbe9aa16ca057c5879446c3\",\"size\":897,\"time\":1549721566,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/components\\/pagination.php\",\"hash\":\"864ca1bdecbe9aa16ca057c5879446c3\",\"size\":897,\"time\":1549721566}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__108963ce521f6df40176074ca9c6a1e2', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgrade.module\",\"hash\":\"d502a6a5d8775254dfe1d011f9b5e555\",\"size\":27544,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgrade.module\",\"hash\":\"1bf2599c8b4392e5e0b2a4c21bc03cbd\",\"size\":27843,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__fad1dd107b407e87c276b8eeeac22898', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtp.module\",\"hash\":\"e382a2d0423183c7521e8643255d2c6e\",\"size\":34306,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtp.module\",\"hash\":\"3f1cf1ff2d981683a7e5c401b3156e3a\",\"size\":35028,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5b85aa299d5f4bbda28c21c402ff3916', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/templates\\/mail.php\",\"hash\":\"95671c3b28b8aa10945c9740bf1ed7a9\",\"size\":1918,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/mail.php\",\"hash\":\"22e2fb0af0929e6d763b75ece054df2e\",\"size\":1931,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__baa0530ded8c3c14e3d5f3bb73da7178', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpAdaptor.php\",\"hash\":\"33b0828d5e139807897b0312953f2e79\",\"size\":15708,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtpAdaptor.php\",\"hash\":\"0e468aa6b23937ad312d097348102a05\",\"size\":16614,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5e16e119ed1e054a15c9c639de8cfcfe', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/email_message.php\",\"hash\":\"5509de5f6ee8dc71b066dba00d7cd58f\",\"size\":121559,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/email_message.php\",\"hash\":\"9bbc832ccaa29bf04a2fe91478110bc9\",\"size\":121763,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__7687c016c3a09f5432221a75082e341c', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp_message.php\",\"hash\":\"43375d48bfee6397cf317b13230e78f0\",\"size\":24885,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp_message.php\",\"hash\":\"1f1d2e162c26d680e1403ad38823159b\",\"size\":25491,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__866480083cb396d2b085f5da87a5b904', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp.php\",\"hash\":\"e3e1d60858ea13473fa4dd8235f17c9f\",\"size\":51914,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/smtp.php\",\"hash\":\"e3e1d60858ea13473fa4dd8235f17c9f\",\"size\":51914,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__518b33a4325a353f2703a21f95250808', '{\"source\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/sasl.php\",\"hash\":\"3fce02fe61a0d90291086b55c1c2dfa2\",\"size\":11898,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/b\\/bamblebee\\/gruz\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/smtp_classes\\/sasl.php\",\"hash\":\"843e748905597a053c57e17cb65c6318\",\"size\":11995,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__3285688d3eff6f14b959a4b5a8a7661e', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"4b3a272433f78b0df205b5c993c90634\",\"size\":54385,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"6ae67b63e147b9da5e1a327060b9e76a\",\"size\":57936,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__043f9c7539c66c82d98e2abfaf746fd9', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"d6a2e4d9f1815df7679a46b2498c4c87\",\"size\":8645,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"5aafcfabb3b48b69bc3e8e132be325f7\",\"size\":8684,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__cbd2facbda2cdf72b623b4cfdf366d1f', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"bf05a996f3b205ac1e535e118899d01b\",\"size\":1413,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"4cdce8a4606dabcfd7700b9272883aa5\",\"size\":1491,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__63c23e8e33477ba4a6f19c0b7f61133f', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"f8ef48a73ab0a102eb1f2c71f06fe2e6\",\"size\":2118,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"64fbc5f8fab4346d7263db714cf05697\",\"size\":2157,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__9c16bdd3c9eb5ec7abff4207ac87ba76', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"07ff2191e35f5eff83db624f071cf004\",\"size\":21316,\"time\":1542313720,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"ea8bf95fae9e3f58769108a861639125\",\"size\":22421,\"time\":1542313720}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__4300eafb03548c66b8820975bc9b4c7f', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"447210ddea9dc0bc815c4f871e949319\",\"size\":6477,\"time\":1542310239,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"58bca7ab4ab7bd6142f4e81a3a9aad4e\",\"size\":6516,\"time\":1542310239}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c8ca970f3686cf52a74ed3f165a60912', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"a306809bf7d98c551d13c92f6ffd528e\",\"size\":5138,\"time\":1542310762,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"47fa5e008b812951e7106f1b399982a6\",\"size\":5346,\"time\":1542310762}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__0fdd0b803e4ba10a41b4e627616414c6', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"f858e3b40cd800dde05598d5f58926c1\",\"size\":3744,\"time\":1542312096,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"19e4ca2e539f2e20991aa25759aad4ab\",\"size\":4039,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d173a35b8934124a3c7c95711fb3d8db', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"9a456aa6c977b2bc54f64156e5338003\",\"size\":1956,\"time\":1542312096,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"bdb3e7cbe178af9774bd024d0e17f2e1\",\"size\":2289,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__424d87e0772e010029ca510c91349872', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/_init.php\",\"hash\":\"bbf69a97668920f5bd69fa5fb82ced93\",\"size\":1045,\"time\":1551211863,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"a8572b69c1249be46efa3979c8d27eb5\",\"size\":1220,\"time\":1551211863}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e069df1768a0e31d861de19a195a600c', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/_main.php\",\"hash\":\"482614d04fab67545b2926a0a9bd4691\",\"size\":3233,\"time\":1551728352,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"6b3416871ec8d95b73096e8ecae8b071\",\"size\":4214,\"time\":1551728352}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__53ce9a38072b3b60dec357bd20841228', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__af776d56b91e58095e24f3a31dced39a', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1542312096,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1542312096}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d97cde68b14edbfa2ddbb77aa35cf81b', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__996825278c47763f5fd4a0950433e720', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"d0ebac99bcb48c394d308b26192ea3d5\",\"size\":2066,\"time\":1549743070,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"d0ebac99bcb48c394d308b26192ea3d5\",\"size\":2066,\"time\":1549743070}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d262ff4d886a30fda5ec9a4a53ba4f77', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"f0fb870cfc8b7b371a55b599cb93248c\",\"size\":3819,\"time\":1551301696,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"f0fb870cfc8b7b371a55b599cb93248c\",\"size\":3819,\"time\":1551301696}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__55cf99d08a7ce6a7ba7b291979469a22', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"4bf1281b852ab97c251f43067e79943e\",\"size\":162,\"time\":1543864333,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"63baa22625d09632cb303f234b30efad\",\"size\":341,\"time\":1543864333}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a140d159afbeb2783e9c830119cbba1b', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/modules\\/carousel\\/carousel.php\",\"hash\":\"2374eeb74a4c3efeffb2c04c203ffad0\",\"size\":3978,\"time\":1548360735,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/carousel\\/carousel.php\",\"hash\":\"2374eeb74a4c3efeffb2c04c203ffad0\",\"size\":3978,\"time\":1548360735}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bbd9d4abffe49f1a2a042dc061fa9850', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"33178e99c41410ec4491f0502f597346\",\"size\":2479,\"time\":1551388087,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"33178e99c41410ec4491f0502f597346\",\"size\":2479,\"time\":1551388087}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__418ada8334d43f2d322f56a6c39dafcb', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"952395e3348e8bb2cfcd01e64bfa7818\",\"size\":1694,\"time\":1551297269,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"952395e3348e8bb2cfcd01e64bfa7818\",\"size\":1694,\"time\":1551297269}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__7ff47c53f676e0f7490d1638855659bd', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/modules\\/tabs\\/tabs.php\",\"hash\":\"5f98e44bdbcef30f158834912bc4d429\",\"size\":2217,\"time\":1544301476,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/tabs\\/tabs.php\",\"hash\":\"5f98e44bdbcef30f158834912bc4d429\",\"size\":2217,\"time\":1544301476}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__50e107e8248959bc44825c9331d2145a', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"263a505cb57a479f0c005befcc5cfcf0\",\"size\":7490,\"time\":1551703938,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"263a505cb57a479f0c005befcc5cfcf0\",\"size\":7490,\"time\":1551703938}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5c1e6753b5deb3588301bc26e48dd1b2', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"5d1070b42b9db0fc31044efd3bc45a4e\",\"size\":693,\"time\":1551296404,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"5d1070b42b9db0fc31044efd3bc45a4e\",\"size\":693,\"time\":1551296404}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2d5f0e0f00036d2348b7eac332cfdc40', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"53718147e5a1797508754e5af4b8e65a\",\"size\":11548,\"time\":1544469770,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"7a6e8d63f0cc239af9ddd07fc46c6175\",\"size\":11639,\"time\":1544469770}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c9d38e4994a5aac5742ca480fd5da092', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"395d9f4cc277ebe83c0f7d11a3eabf44\",\"size\":643,\"time\":1544469770,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"a17712b4a5db5fac542d046390afc8ab\",\"size\":656,\"time\":1544469770}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__144f584a30d9573777d178b7b016672b', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1549827827,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bac49a02fb68ac8cf6406715fa534e27', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__7c06bfa5d459ad9b6b078e39ee14783b', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/parts\\/categories\\/blog-category.php\",\"hash\":\"1ba49d8710c4205a89e028409d4431f0\",\"size\":1965,\"time\":1551297595,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/categories\\/blog-category.php\",\"hash\":\"1ba49d8710c4205a89e028409d4431f0\",\"size\":1965,\"time\":1551297595}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__23d39780966eb771ded2810be223662f', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/parts\\/components\\/pagination.php\",\"hash\":\"864ca1bdecbe9aa16ca057c5879446c3\",\"size\":897,\"time\":1549721566,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/components\\/pagination.php\",\"hash\":\"864ca1bdecbe9aa16ca057c5879446c3\",\"size\":897,\"time\":1549721566}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b97cd4e86d15400040d96e13b04cc619', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/modules\\/gallery\\/gallery.php\",\"hash\":\"172d85e4e5de15a5e3f4413017acd2d1\",\"size\":2918,\"time\":1551386129,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/gallery\\/gallery.php\",\"hash\":\"172d85e4e5de15a5e3f4413017acd2d1\",\"size\":2918,\"time\":1551386129}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__9a085f1541f12436d541fed55b28a251', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/modules\\/map\\/map.php\",\"hash\":\"ea788591ff1c15e3ffbfc4e5eb96f3c5\",\"size\":733,\"time\":1551940149,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/map\\/map.php\",\"hash\":\"ea788591ff1c15e3ffbfc4e5eb96f3c5\",\"size\":733,\"time\":1551940149}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ff5a933a1bf8ab6ca3605787ccb4a883', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/modules\\/video\\/video.php\",\"hash\":\"d828f586346dd14ce83d676ebdd42af7\",\"size\":612,\"time\":1551731628,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/video\\/video.php\",\"hash\":\"d828f586346dd14ce83d676ebdd42af7\",\"size\":612,\"time\":1551731628}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__9fca7a6d8519941b16fac28c8b9e77ce', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"4b3a272433f78b0df205b5c993c90634\",\"size\":54385,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Duplicator.module\",\"hash\":\"6ae67b63e147b9da5e1a327060b9e76a\",\"size\":57936,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__efdee55268db518db82ddff5a16e6d61', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"d6a2e4d9f1815df7679a46b2498c4c87\",\"size\":8645,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupUtil.php\",\"hash\":\"5aafcfabb3b48b69bc3e8e132be325f7\",\"size\":8684,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__8a9cf5a7fd21505b571cf4779c415427', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"bf05a996f3b205ac1e535e118899d01b\",\"size\":1413,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/DupLogs.php\",\"hash\":\"4cdce8a4606dabcfd7700b9272883aa5\",\"size\":1491,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ab50602afc2634fa095711aef120ae3f', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"f8ef48a73ab0a102eb1f2c71f06fe2e6\",\"size\":2118,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/Classes\\/BackupDatabase.php\",\"hash\":\"64fbc5f8fab4346d7263db714cf05697\",\"size\":2157,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__13a04b0902d127ea0099e9ff35dbc0e4', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"07ff2191e35f5eff83db624f071cf004\",\"size\":21316,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/Duplicator\\/ProcessDuplicator.module\",\"hash\":\"ea8bf95fae9e3f58769108a861639125\",\"size\":22421,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b8a64e58d2c3845c4c90df34c75e16e8', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"447210ddea9dc0bc815c4f871e949319\",\"size\":6477,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/HelperFieldLinks\\/HelperFieldLinks.module\",\"hash\":\"58bca7ab4ab7bd6142f4e81a3a9aad4e\",\"size\":6516,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f1e18c35c7827d64a46c1e0633de1546', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"a306809bf7d98c551d13c92f6ffd528e\",\"size\":5138,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"47fa5e008b812951e7106f1b399982a6\",\"size\":5346,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__829fe3e07979b796de327f74c6e5b41b', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"f858e3b40cd800dde05598d5f58926c1\",\"size\":3744,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/FieldtypeYaml.module\",\"hash\":\"19e4ca2e539f2e20991aa25759aad4ab\",\"size\":4039,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1acd1bae2e4156a1805aa2a840123aaf', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"9a456aa6c977b2bc54f64156e5338003\",\"size\":1956,\"time\":1551388810,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/Autoloader.php\",\"hash\":\"bdb3e7cbe178af9774bd024d0e17f2e1\",\"size\":2289,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__4ee6d533c9600adeb558687625ae0d31', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"53718147e5a1797508754e5af4b8e65a\",\"size\":11548,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"7a6e8d63f0cc239af9ddd07fc46c6175\",\"size\":11639,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__8d843153074a74424cc2e664abe7c46d', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"395d9f4cc277ebe83c0f7d11a3eabf44\",\"size\":643,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"a17712b4a5db5fac542d046390afc8ab\",\"size\":656,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__66842ef7f3c749124b181e87d51170e1', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1549827827,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bc4d4d64a8766c47fa191d40a86a07b6', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/_init.php\",\"hash\":\"bbf69a97668920f5bd69fa5fb82ced93\",\"size\":1045,\"time\":1551211863,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"a8572b69c1249be46efa3979c8d27eb5\",\"size\":1220,\"time\":1551211863}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1ec63bdf596f0e96174d22dd080684d0', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/_main.php\",\"hash\":\"482614d04fab67545b2926a0a9bd4691\",\"size\":3233,\"time\":1551728352,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"6b3416871ec8d95b73096e8ecae8b071\",\"size\":4214,\"time\":1551728352}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__241e2e539bd629970342f7f6cf0071a2', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"0626dd0a792cb94785311e71be41aba9\",\"size\":74,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__374c39ae7b1cf7ffa0cadd5744cf46d1', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1551388810,\"ns\":\"owzim\\\\FieldtypeYaml\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeYaml\\/owzim\\/FieldtypeYaml\\/FTY.php\",\"hash\":\"9c8cd47288d52909d7e9769b9e90c756\",\"size\":4376,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__11c79aac40c1c3a34101dd81a923e115', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/func\\/func.php\",\"hash\":\"f84214fc43f665c79b92dc7a092b364f\",\"size\":7,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__eeec970202778dee5faa1a8be42ccc01', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"d0ebac99bcb48c394d308b26192ea3d5\",\"size\":2066,\"time\":1549743070,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/js-generator.php\",\"hash\":\"d0ebac99bcb48c394d308b26192ea3d5\",\"size\":2066,\"time\":1549743070}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ea931c4637230d33d03c55e329b728b2', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"9ce029863c1bb9362b5c5ff5b9895e84\",\"size\":3852,\"time\":1552396735,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/header\\/header.part.php\",\"hash\":\"9ce029863c1bb9362b5c5ff5b9895e84\",\"size\":3852,\"time\":1552396735}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__39826e8f1b1ce6ac1710f0d111bc0b97', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"4bf1281b852ab97c251f43067e79943e\",\"size\":162,\"time\":1543864333,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/modules-controller.php\",\"hash\":\"63baa22625d09632cb303f234b30efad\",\"size\":341,\"time\":1543864333}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__452c12bf6c5683b2b368704eacb8bf28', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/modules\\/video\\/video.php\",\"hash\":\"d828f586346dd14ce83d676ebdd42af7\",\"size\":612,\"time\":1551731628,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/video\\/video.php\",\"hash\":\"d828f586346dd14ce83d676ebdd42af7\",\"size\":612,\"time\":1551731628}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__696c7c713c4c81a83c8844e9ec775db2', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"33178e99c41410ec4491f0502f597346\",\"size\":2479,\"time\":1551388087,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/flip-cards\\/flip-cards.php\",\"hash\":\"33178e99c41410ec4491f0502f597346\",\"size\":2479,\"time\":1551388087}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__778fcccd46a3aaa71c99629abd3d57c6', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"952395e3348e8bb2cfcd01e64bfa7818\",\"size\":1694,\"time\":1551297269,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/page-preview\\/page-preview.php\",\"hash\":\"952395e3348e8bb2cfcd01e64bfa7818\",\"size\":1694,\"time\":1551297269}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__af7195b88fca08649ccb1b550a0b7db0', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/modules\\/gallery\\/gallery.php\",\"hash\":\"172d85e4e5de15a5e3f4413017acd2d1\",\"size\":2918,\"time\":1551386129,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/gallery\\/gallery.php\",\"hash\":\"172d85e4e5de15a5e3f4413017acd2d1\",\"size\":2918,\"time\":1551386129}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__04d81e59304d0c00268fc9122d49fff5', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"263a505cb57a479f0c005befcc5cfcf0\",\"size\":7490,\"time\":1551703938,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/form\\/form.php\",\"hash\":\"263a505cb57a479f0c005befcc5cfcf0\",\"size\":7490,\"time\":1551703938}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__917686410936e5a3bba91cd9dc2e83e6', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/modules\\/map\\/map.php\",\"hash\":\"ea788591ff1c15e3ffbfc4e5eb96f3c5\",\"size\":733,\"time\":1551940149,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/map\\/map.php\",\"hash\":\"ea788591ff1c15e3ffbfc4e5eb96f3c5\",\"size\":733,\"time\":1551940149}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__53ea69f80b07a1380ff460e034e1c6ea', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"5d1070b42b9db0fc31044efd3bc45a4e\",\"size\":693,\"time\":1551296404,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/footer\\/footer.part.php\",\"hash\":\"5d1070b42b9db0fc31044efd3bc45a4e\",\"size\":693,\"time\":1551296404}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__4e7b022408269464875028dbad5204ad', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1549827827,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"0faaa2b1e9bfa767e2c9b6f16d80347e\",\"size\":115,\"time\":1549827827}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b42f5a47f19d566b1b3a9880c8e33a53', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/parts\\/categories\\/blog-category.php\",\"hash\":\"4dc19ea453cedb96644b6813c60e6d98\",\"size\":1992,\"time\":1552396382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/categories\\/blog-category.php\",\"hash\":\"4dc19ea453cedb96644b6813c60e6d98\",\"size\":1992,\"time\":1552396382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e9c7569759b56270732a3e1cf0794f82', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/parts\\/components\\/pagination.php\",\"hash\":\"864ca1bdecbe9aa16ca057c5879446c3\",\"size\":897,\"time\":1549721566,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/components\\/pagination.php\",\"hash\":\"864ca1bdecbe9aa16ca057c5879446c3\",\"size\":897,\"time\":1549721566}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__49269ad5eebe815a5de9802d393ee929', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/contacts.php\",\"hash\":\"97847b1e24771b404922705d90264acf\",\"size\":65,\"time\":1551817883,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/contacts.php\",\"hash\":\"97847b1e24771b404922705d90264acf\",\"size\":65,\"time\":1551817883}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bbbc2a87c489d10d36ff29d1b5b5b4b3', '{\"source\":{\"file\":\"views\\/contacts\\/contacts.view.php\",\"hash\":\"5bd71c04581527a9bd0e3f94fdcb9bdc\",\"size\":4308,\"time\":1552079954,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/views\\/contacts\\/contacts.view.php\",\"hash\":\"5bd71c04581527a9bd0e3f94fdcb9bdc\",\"size\":4308,\"time\":1552079954}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__6f47689be0ed88b9d4b69156a6e28d05', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgrade.module\",\"hash\":\"d502a6a5d8775254dfe1d011f9b5e555\",\"size\":27544,\"time\":1544469770,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgrade.module\",\"hash\":\"1bf2599c8b4392e5e0b2a4c21bc03cbd\",\"size\":27843,\"time\":1544469770}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__525101f70c997fbbdb3591f90ae1aa56', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/modules\\/gallery2\\/gallery2.php\",\"hash\":\"5abde6e233112fa130b06e97535d7303\",\"size\":2946,\"time\":1552075302,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/gallery2\\/gallery2.php\",\"hash\":\"5abde6e233112fa130b06e97535d7303\",\"size\":2946,\"time\":1552075302}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__8bd351c593de7b02c5484b806eb9f7c4', '{\"source\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/templates\\/parts\\/breadcrumbs\\/breadcrumbs.php\",\"hash\":\"12dc064eaa9ad171c0d6ee47a8297f8a\",\"size\":402,\"time\":1552079457,\"ns\":\"\\\\\"},\"target\":{\"file\":\"E:\\/OpenServer\\/OSPanel\\/domains\\/much\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/breadcrumbs\\/breadcrumbs.php\",\"hash\":\"12dc064eaa9ad171c0d6ee47a8297f8a\",\"size\":402,\"time\":1552079457}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__700319de0000b7868a700518cc9fdfe0', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/modules\\/gallery2\\/gallery2.php\",\"hash\":\"737f646622ba16387f607f3878e70637\",\"size\":3176,\"time\":1552395757,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/modules\\/gallery2\\/gallery2.php\",\"hash\":\"737f646622ba16387f607f3878e70637\",\"size\":3176,\"time\":1552395757}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__0e2178f73a69062fa8e15512a9f5bc7d', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/parts\\/breadcrumbs\\/breadcrumbs.php\",\"hash\":\"12dc064eaa9ad171c0d6ee47a8297f8a\",\"size\":402,\"time\":1552079457,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/parts\\/breadcrumbs\\/breadcrumbs.php\",\"hash\":\"12dc064eaa9ad171c0d6ee47a8297f8a\",\"size\":402,\"time\":1552079457}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__01050518dda190f2e4cf97ec192d3692', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/templates\\/contacts.php\",\"hash\":\"97847b1e24771b404922705d90264acf\",\"size\":65,\"time\":1551817883,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/contacts.php\",\"hash\":\"97847b1e24771b404922705d90264acf\",\"size\":65,\"time\":1551817883}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__46912ef1c15ce7c1fda49681eadbf62c', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgrade.module\",\"hash\":\"d502a6a5d8775254dfe1d011f9b5e555\",\"size\":27544,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgrade.module\",\"hash\":\"1bf2599c8b4392e5e0b2a4c21bc03cbd\",\"size\":27843,\"time\":1551388810}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bffadaac034951e768c03bad197f87ed', '{\"source\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtp.module\",\"hash\":\"e382a2d0423183c7521e8643255d2c6e\",\"size\":34306,\"time\":1551388810,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/v\\/vip990\\/test\\/public_html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/WireMailSmtp\\/WireMailSmtp.module\",\"hash\":\"3f1cf1ff2d981683a7e5c401b3156e3a\",\"size\":35028,\"time\":1551388810}}', '2010-04-08 03:10:10');

DROP TABLE IF EXISTS `field_admin_theme`;
CREATE TABLE `field_admin_theme` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_admin_theme` (`pages_id`, `data`) VALUES('41', '163');

DROP TABLE IF EXISTS `field_author`;
CREATE TABLE `field_author` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_author` (`pages_id`, `data`) VALUES('1140', 'streetAddress');
INSERT INTO `field_author` (`pages_id`, `data`) VALUES('1143', 'telephone');
INSERT INTO `field_author` (`pages_id`, `data`) VALUES('1144', 'telephone');
INSERT INTO `field_author` (`pages_id`, `data`) VALUES('1145', 'email');
INSERT INTO `field_author` (`pages_id`, `data`) VALUES('1134', 'Ксения');
INSERT INTO `field_author` (`pages_id`, `data`) VALUES('1135', 'Иванов Иван Иванович (директор)');

DROP TABLE IF EXISTS `field_blog`;
CREATE TABLE `field_blog` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_blog_end`;
CREATE TABLE `field_blog_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_blog_max`;
CREATE TABLE `field_blog_max` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_blog_max` (`pages_id`, `data`) VALUES('1', '3');
INSERT INTO `field_blog_max` (`pages_id`, `data`) VALUES('1117', '15');
INSERT INTO `field_blog_max` (`pages_id`, `data`) VALUES('1029', '10');

DROP TABLE IF EXISTS `field_blog_parent`;
CREATE TABLE `field_blog_parent` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_blog_section_classes`;
CREATE TABLE `field_blog_section_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_blog_section_classes` (`pages_id`, `data`) VALUES('1', 'view light-section');

DROP TABLE IF EXISTS `field_blog_section_image`;
CREATE TABLE `field_blog_section_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_blog_section_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1', '4.jpg', '0', '[null]', '2019-01-17 00:07:12', '2019-01-17 00:07:12', '');

DROP TABLE IF EXISTS `field_blog_section_parallax`;
CREATE TABLE `field_blog_section_parallax` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_blog_section_parallax` (`pages_id`, `data`) VALUES('1', '1');

DROP TABLE IF EXISTS `field_blog_title`;
CREATE TABLE `field_blog_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_blog_title` (`pages_id`, `data`) VALUES('1', 'Акции');

DROP TABLE IF EXISTS `field_blog_title_classes`;
CREATE TABLE `field_blog_title_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_blog_title_classes` (`pages_id`, `data`) VALUES('1', 'text-black-50 text-center pb-3 text-uppercase');

DROP TABLE IF EXISTS `field_body`;
CREATE TABLE `field_body` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1041', '<p>© 2019 Сайт сделан студией <a href=\"http://rusich69.ru/\" target=\"_blank\" rel=\"noreferrer noopener\">Русич</a></p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1065', '<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1067', '<p>При выборе обоев для необходимо в первую очередь учитывать, какое помещение планируется оформлять. Условия освещенности, размер и форма комнаты играют определяющую роль, ведь цвет, насыщенность тона а также рисунок настенного покрытия могут значительно изменить восприятие интерьера</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1031', '<p>Рекламно-производственная компания \"Мухомор\" осуществляет полный комплекс услуг в области изготовления наружной и внутренней рекламы. Мы группа креативно мыслящих людей, авантюристов в душе и свободных в мыслях. Нешаблонный подход к поставленным задачам и нестандартные решения. Нашими клиентами становятся компании сдущие в ногу со временем и осознающие реальность работы над собственным брендом. У нас большой опыт работы, доступные цены, собственное производство.</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1125', '<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно, то есть:</p>\n\n<ul>\n	<li>быть презентабельным и интересным (нести четкую и конкретную идею);</li>\n	<li>предоставлять всю необходимую информацию;</li>\n	<li>обладать индивидуальным визуальным оформлением;</li>\n	<li>выглядеть надежным и качественным (иначе как клиент поймет, что вам можно доверять);</li>\n	<li>являться настоящим рекламным провокатором, чтобы клиент решил: «Куплю всё, что эти рекламные стенды предлагают».</li>\n</ul>\n\n<p><strong>Как добиться такого рекламного эффекта?</strong></p>\n\n<p>Выбрать, каким будет ваш рекламный стенд. Если решение дается трудно, стоит обратиться к мастеру создания стендов, например к нашей компании. «Белая полоса» – настоящий специалист в области торгового и выставочного оборудования любой сложности.</p>\n\n<p><strong>Каким может быть ваш рекламный стенд?</strong></p>\n\n<p>Наш ответ: любым, каким вы пожелаете его видеть:<br />\n<strong>Мобильные стенды</strong> – простой, эффективный и, главное, экономичный способ выразительно представить свою компанию. Обычно производство рекламных стендов этого типа требует незначительного количества материалов, но большого внимания к точности. Для создания этих стендов используются металлический каркас и закрепленное на нем изображение. Данный тип конструкции вариативен в плане исполнения: Banner-up, Salebox, Roll-Up, Pop-Up, Fold-Up, EasyShow, Пикколо и т. д. Каждая из систем обладает своими преимуществами, подробнее о которых вы можете узнать у наших специалистов.</p>\n\n<p><strong>Стационарные стенды</strong> отличаются простотой исполнения и надежностью, могут быть выполнены в любом формате (от маленьких или высоких напольных стеллажей до простых стендов баннерного типа).</p>\n\n<p><strong>Эксклюзивные стенды</strong> могут быть все той же мобильной конструкцией, но созданной не по шаблону, а по индивидуально разработанному дизайну. Для реализации эксклюзивного стенда за основу берется тот же мобильный механизм, но его конструкция, вид и форма могут быть выполнены в совершенно неожиданной манере. Например, одни элементы стенда могут быть статичными, а другие – вращаться, это могут быть простые или перфорированные полотна с картинками или карманами для буклетов.</p>\n\n<p>Также мы с удовольствием предложим вам один или несколько вариантов яркого, оригинального дизайна.<br />\nМатериалы, которые мы используем для создания рекламных стендов: металл, дерево, пластик, ПВХ, оргстекло и др. Для нанесения изображения применяются обычные типографские методы, качественные и проверенные временем.</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1076', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem, optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos. Odit sed qui, dolorum!.</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1077', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem, optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos. Odit sed qui, dolorum!.</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1078', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem, optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos. Odit sed qui, dolorum!.</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1082', '<p>После выполнения работы</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1083', '<p>Переводом на расчетный счет с документами</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1084', '<p>Переводом на карту физ.лица</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1147', '<p><strong>Условия конфиденциальности для сайта</strong></p>\n\n<p>Настоящая Политика конфиденциальности персональных данных (далее – Политика конфиденциальности) действует в отношении всей информации, которую данный сайт, на котором размещен текст этой Политики конфиденциальности, может получить о Пользователе, а также любых программ и продуктов, размещенных на нем.</p>\n\n<h2>1. ОПРЕДЕЛЕНИЕ ТЕРМИНОВ</h2>\n\n<p>1.1 В настоящей Политике конфиденциальности используются следующие термины:</p>\n\n<p>1.1.1. «Администрация сайта» – уполномоченные сотрудники на управления сайтом, действующие от его имени, которые организуют и (или) осуществляет обработку персональных данных, а также определяет цели обработки персональных данных, состав персональных данных, подлежащих обработке, действия (операции), совершаемые с персональными данными.</p>\n\n<p>1.1.2. «Персональные данные» - любая информация, относящаяся к прямо или косвенно определенному или определяемому физическому лицу (субъекту персональных данных).</p>\n\n<p>1.1.3. «Обработка персональных данных» - любое действие (операция) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение персональных данных.</p>\n\n<p>1.1.4. «Конфиденциальность персональных данных» - обязательное для соблюдения Администрацией сайта требование не допускать их умышленного распространения без согласия субъекта персональных данных или наличия иного законного основания.</p>\n\n<p>1.1.5. «Пользователь сайта (далее Пользователь)» – лицо, имеющее доступ к сайту, посредством сети Интернет и использующее данный сайт для своих целей.</p>\n\n<p>1.1.6. «Cookies» — небольшой фрагмент данных, отправленный веб-сервером и хранимый на компьютере пользователя, который веб-клиент или веб-браузер каждый раз пересылает веб-серверу в HTTP-запросе при попытке открыть страницу соответствующего сайта.</p>\n\n<p>1.1.7. «IP-адрес» — уникальный сетевой адрес узла в компьютерной сети, построенной по протоколу IP.</p>\n\n<h2>2. ОБЩИЕ ПОЛОЖЕНИЯ</h2>\n\n<p>2.1. Использование Пользователем сайта означает согласие с настоящей Политикой конфиденциальности и условиями обработки персональных данных Пользователя.</p>\n\n<p>2.2. В случае несогласия с условиями Политики конфиденциальности Пользователь должен прекратить использование сайта.</p>\n\n<p>2.3.Настоящая Политика конфиденциальности применяется только к данному сайту. Администрация сайта не контролирует и не несет ответственность за сайты третьих лиц, на которые Пользователь может перейти по ссылкам, доступным на данном сайте.</p>\n\n<p>2.4. Администрация сайта не проверяет достоверность персональных данных, предоставляемых Пользователем сайта.</p>\n\n<h2>3. ПРЕДМЕТ ПОЛИТИКИ КОНФИДЕНЦИАЛЬНОСТИ</h2>\n\n<p>3.1. Настоящая Политика конфиденциальности устанавливает обязательства Администрации сайта по умышленному неразглашению персональных данных, которые Пользователь предоставляет по разнообразным запросам Администрации сайта (например, при регистрации на сайте, оформлении заказа, подписки на уведомления и т.п).</p>\n\n<p>3.2. Персональные данные, разрешённые к обработке в рамках настоящей Политики конфиденциальности, предоставляются Пользователем путём заполнения специальных форм на Сайте и обычно включают в себя следующую информацию:</p>\n\n<p>3.2.1. фамилию, имя, отчество Пользователя;</p>\n\n<p>3.2.2. контактный телефон Пользователя;</p>\n\n<p>3.2.3. адрес электронной почты (e-mail);</p>\n\n<p>3.2.4. место жительство Пользователя и другие данные.</p>\n\n<p>3.3. Администрация сайта также принимает усилия по защите Персональных данных, которые автоматически передаются в процессе посещения страниц сайта:</p>\n\n<p>IP адрес;<br />\nинформация из cookies;<br />\nинформация о браузере (или иной программе, которая осуществляет доступ к сайту);<br />\nвремя доступа;<br />\nпосещенные адреса страниц;<br />\nреферер (адрес предыдущей страницы) и т.п.</p>\n\n<p>3.3.1. Отключение cookies может повлечь невозможность доступа к сайту.</p>\n\n<p>3.3.2. Сайт осуществляет сбор статистики об IP-адресах своих посетителей. Данная информация используется с целью выявления и решения технических проблем, для контроля корректности проводимых операций.</p>\n\n<p>3.4. Любая иная персональная информация неоговоренная выше (история покупок, используемые браузеры и операционные системы и т.д.) не подлежит умышленному разглашению, за исключением случаев, предусмотренных в п.п. 5.2. и 5.3. настоящей Политики конфиденциальности.</p>\n\n<h2>4. ЦЕЛИ СБОРА ПЕРСОНАЛЬНОЙ ИНФОРМАЦИИ ПОЛЬЗОВАТЕЛЯ</h2>\n\n<p>4.1. Персональные данные Пользователя Администрация сайта может использовать в целях:</p>\n\n<p>4.1.1. Идентификации Пользователя, зарегистрированного на сайте, для оформления заказа и (или) заключения Договора.</p>\n\n<p>4.1.2. Предоставления Пользователю доступа к персонализированным ресурсам сайта.<br />\n4.1.3. Установления с Пользователем обратной связи, включая направление уведомлений, запросов, касающихся использования сайта, оказания услуг, обработка запросов и заявок от Пользователя.</p>\n\n<p>4.1.4. Определения места нахождения Пользователя для обеспечения безопасности, предотвращения мошенничества.</p>\n\n<p>4.1.5. Подтверждения достоверности и полноты персональных данных, предоставленных Пользователем.</p>\n\n<p>4.1.6. Создания учетной записи для совершения покупок, если Пользователь дал согласие на создание учетной записи.</p>\n\n<p>4.1.7. Уведомления Пользователя сайта о состоянии Заказа.</p>\n\n<p>4.1.8. Обработки и получения платежей, подтверждения налога или налоговых льгот, оспаривания платежа, определения права на получение кредитной линии Пользователем.</p>\n\n<p>4.1.9. Предоставления Пользователю эффективной клиентской и технической поддержки при возникновении проблем связанных с использованием сайта.</p>\n\n<p>4.1.10. Предоставления Пользователю с его согласия, обновлений продукции, специальных предложений, информации о ценах, новостной рассылки и иных сведений от имени сайта или от имени партнеров сайта.</p>\n\n<p>4.1.11. Осуществления рекламной деятельности с согласия Пользователя.</p>\n\n<p>4.1.12. Предоставления доступа Пользователю на сторонние сайты или сервисы партнеров данного сайта с целью получения их предложений, обновлений или услуг.</p>\n\n<h2>5. СПОСОБЫ И СРОКИ ОБРАБОТКИ ПЕРСОНАЛЬНОЙ ИНФОРМАЦИИ</h2>\n\n<p>5.1. Обработка персональных данных Пользователя осуществляется без ограничения срока, любым законным способом, в том числе в информационных системах персональных данных с использованием средств автоматизации или без использования таких средств.</p>\n\n<p>5.2. Пользователь соглашается с тем, что Администрация сайта вправе передавать персональные данные третьим лицам, в частности, курьерским службам, организациями почтовой связи, операторам электросвязи, исключительно в целях выполнения заявок Пользователя, оформленных на сайте, в рамках Договора публичной оферты.</p>\n\n<p>5.3. Персональные данные Пользователя могут быть переданы уполномоченным органам государственной власти только по основаниям и в порядке, установленным действующим законодательством.</p>\n\n<h2>6. ОБЯЗАТЕЛЬСТВА СТОРОН</h2>\n\n<p>6.1. Пользователь обязуется:</p>\n\n<p>6.1.1. Предоставить корректную и правдивую информацию о персональных данных, необходимую для пользования сайтом.</p>\n\n<p>6.1.2. Обновить или дополнить предоставленную информацию о персональных данных в случае изменения данной информации.</p>\n\n<p>6.1.3. Принимать меры для защиты доступа к своим конфиденциальным данным, хранящимся на сайте.</p>\n\n<p>6.2. Администрация сайта обязуется:</p>\n\n<p>6.2.1. Использовать полученную информацию исключительно для целей, указанных в п. 4 настоящей Политики конфиденциальности.</p>\n\n<p>6.2.2. Не разглашать персональных данных Пользователя, за исключением п.п. 5.2. и 5.3. настоящей Политики Конфиденциальности.</p>\n\n<p>6.2.3. Осуществить блокирование персональных данных, относящихся к соответствующему Пользователю, с момента обращения или запроса Пользователя или его законного представителя либо уполномоченного органа по защите прав субъектов персональных данных на период проверки, в случае выявления недостоверных персональных данных или неправомерных действий.</p>\n\n<h2>7. ОТВЕТСТВЕННОСТЬ СТОРОН</h2>\n\n<p>7.1. Администрация сайта несёт ответственность за умышленное разглашение Персональных данных Пользователя в соответствии с действующим законодательством, за исключением случаев, предусмотренных п.п. 5.2., 5.3. и 7.2. настоящей Политики Конфиденциальности.</p>\n\n<p>7.2. В случае утраты или разглашения Персональных данных Администрация сайта не несёт ответственность, если данная конфиденциальная информация:</p>\n\n<p>7.2.1. Стала публичным достоянием до её утраты или разглашения.</p>\n\n<p>7.2.2. Была получена от третьей стороны до момента её получения Администрацией сайта.</p>\n\n<p>7.2.3. Была получена третьими лицами путем несанкционированного доступа к файлам сайта.</p>\n\n<p>7.2.4. Была разглашена с согласия Пользователя.</p>\n\n<p>7.3. Пользователь несет ответственность за правомерность, корректность и правдивость предоставленной Персональных данных в соответствии с действующим законодательством.</p>\n\n<h2>8. РАЗРЕШЕНИЕ СПОРОВ</h2>\n\n<p>8.1. До обращения в суд с иском по спорам, возникающим из отношений между Пользователем сайта и Администрацией сайта, обязательным является предъявление претензии (письменного предложения о добровольном урегулировании спора).</p>\n\n<p>8.2 .Получатель претензии в течение 30 календарных дней со дня получения претензии, письменно уведомляет заявителя претензии о результатах рассмотрения претензии.</p>\n\n<p>8.3. При не достижении соглашения спор будет передан на рассмотрение в судебный орган в соответствии с действующим законодательством.</p>\n\n<p>8.4. К настоящей Политике конфиденциальности и отношениям между Пользователем и Администрацией сайта применяется действующее законодательство.</p>\n\n<h2>9. ДОПОЛНИТЕЛЬНЫЕ УСЛОВИЯ</h2>\n\n<p>9.1. Администрация сайта вправе вносить изменения в настоящую Политику конфиденциальности без согласия Пользователя.</p>\n\n<p>9.2. Новая Политика конфиденциальности вступает в силу с момента ее размещения на Сайте, если иное не предусмотрено новой редакцией Политики конфиденциальности.</p>\n\n<p>Настоящая Политика конфиденциальности персональных данных (далее – Политика конфиденциальности) действует в отношении всей информации, которую данный сайт, на котором размещен текст этой Политики конфиденциальности, может получить о Пользователе, а также любых программ и продуктов, размещенных на нем.</p>\n\n<h2>1. ОПРЕДЕЛЕНИЕ ТЕРМИНОВ</h2>\n\n<p>1.1 В настоящей Политике конфиденциальности используются следующие термины:</p>\n\n<p>1.1.1. «Администрация сайта» – уполномоченные сотрудники на управления сайтом, действующие от его имени, которые организуют и (или) осуществляет обработку персональных данных, а также определяет цели обработки персональных данных, состав персональных данных, подлежащих обработке, действия (операции), совершаемые с персональными данными.</p>\n\n<p>1.1.2. «Персональные данные» - любая информация, относящаяся к прямо или косвенно определенному или определяемому физическому лицу (субъекту персональных данных).</p>\n\n<p>1.1.3. «Обработка персональных данных» - любое действие (операция) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение персональных данных.</p>\n\n<p>1.1.4. «Конфиденциальность персональных данных» - обязательное для соблюдения Администрацией сайта требование не допускать их умышленного распространения без согласия субъекта персональных данных или наличия иного законного основания.</p>\n\n<p>1.1.5. «Пользователь сайта (далее Пользователь)» – лицо, имеющее доступ к сайту, посредством сети Интернет и использующее данный сайт для своих целей.</p>\n\n<p>1.1.6. «Cookies» — небольшой фрагмент данных, отправленный веб-сервером и хранимый на компьютере пользователя, который веб-клиент или веб-браузер каждый раз пересылает веб-серверу в HTTP-запросе при попытке открыть страницу соответствующего сайта.</p>\n\n<p>1.1.7. «IP-адрес» — уникальный сетевой адрес узла в компьютерной сети, построенной по протоколу IP.</p>\n\n<h2>2. ОБЩИЕ ПОЛОЖЕНИЯ</h2>\n\n<p>2.1. Использование Пользователем сайта означает согласие с настоящей Политикой конфиденциальности и условиями обработки персональных данных Пользователя.</p>\n\n<p>2.2. В случае несогласия с условиями Политики конфиденциальности Пользователь должен прекратить использование сайта.</p>\n\n<p>2.3.Настоящая Политика конфиденциальности применяется только к данному сайту. Администрация сайта не контролирует и не несет ответственность за сайты третьих лиц, на которые Пользователь может перейти по ссылкам, доступным на данном сайте.</p>\n\n<p>2.4. Администрация сайта не проверяет достоверность персональных данных, предоставляемых Пользователем сайта.</p>\n\n<h2>3. ПРЕДМЕТ ПОЛИТИКИ КОНФИДЕНЦИАЛЬНОСТИ</h2>\n\n<p>3.1. Настоящая Политика конфиденциальности устанавливает обязательства Администрации сайта по умышленному неразглашению персональных данных, которые Пользователь предоставляет по разнообразным запросам Администрации сайта (например, при регистрации на сайте, оформлении заказа, подписки на уведомления и т.п).</p>\n\n<p>3.2. Персональные данные, разрешённые к обработке в рамках настоящей Политики конфиденциальности, предоставляются Пользователем путём заполнения специальных форм на Сайте и обычно включают в себя следующую информацию:</p>\n\n<p>3.2.1. фамилию, имя, отчество Пользователя;</p>\n\n<p>3.2.2. контактный телефон Пользователя;</p>\n\n<p>3.2.3. адрес электронной почты (e-mail);</p>\n\n<p>3.2.4. место жительство Пользователя и другие данные.</p>\n\n<p>3.3. Администрация сайта также принимает усилия по защите Персональных данных, которые автоматически передаются в процессе посещения страниц сайта:</p>\n\n<p>IP адрес;<br />\nинформация из cookies;<br />\nинформация о браузере (или иной программе, которая осуществляет доступ к сайту);<br />\nвремя доступа;<br />\nпосещенные адреса страниц;<br />\nреферер (адрес предыдущей страницы) и т.п.</p>\n\n<p>3.3.1. Отключение cookies может повлечь невозможность доступа к сайту.</p>\n\n<p>3.3.2. Сайт осуществляет сбор статистики об IP-адресах своих посетителей. Данная информация используется с целью выявления и решения технических проблем, для контроля корректности проводимых операций.</p>\n\n<p>3.4. Любая иная персональная информация неоговоренная выше (история покупок, используемые браузеры и операционные системы и т.д.) не подлежит умышленному разглашению, за исключением случаев, предусмотренных в п.п. 5.2. и 5.3. настоящей Политики конфиденциальности.</p>\n\n<h2>4. ЦЕЛИ СБОРА ПЕРСОНАЛЬНОЙ ИНФОРМАЦИИ ПОЛЬЗОВАТЕЛЯ</h2>\n\n<p>4.1. Персональные данные Пользователя Администрация сайта может использовать в целях:</p>\n\n<p>4.1.1. Идентификации Пользователя, зарегистрированного на сайте, для оформления заказа и (или) заключения Договора.</p>\n\n<p>4.1.2. Предоставления Пользователю доступа к персонализированным ресурсам сайта.<br />\n4.1.3. Установления с Пользователем обратной связи, включая направление уведомлений, запросов, касающихся использования сайта, оказания услуг, обработка запросов и заявок от Пользователя.</p>\n\n<p>4.1.4. Определения места нахождения Пользователя для обеспечения безопасности, предотвращения мошенничества.</p>\n\n<p>4.1.5. Подтверждения достоверности и полноты персональных данных, предоставленных Пользователем.</p>\n\n<p>4.1.6. Создания учетной записи для совершения покупок, если Пользователь дал согласие на создание учетной записи.</p>\n\n<p>4.1.7. Уведомления Пользователя сайта о состоянии Заказа.</p>\n\n<p>4.1.8. Обработки и получения платежей, подтверждения налога или налоговых льгот, оспаривания платежа, определения права на получение кредитной линии Пользователем.</p>\n\n<p>4.1.9. Предоставления Пользователю эффективной клиентской и технической поддержки при возникновении проблем связанных с использованием сайта.</p>\n\n<p>4.1.10. Предоставления Пользователю с его согласия, обновлений продукции, специальных предложений, информации о ценах, новостной рассылки и иных сведений от имени сайта или от имени партнеров сайта.</p>\n\n<p>4.1.11. Осуществления рекламной деятельности с согласия Пользователя.</p>\n\n<p>4.1.12. Предоставления доступа Пользователю на сторонние сайты или сервисы партнеров данного сайта с целью получения их предложений, обновлений или услуг.</p>\n\n<h2>5. СПОСОБЫ И СРОКИ ОБРАБОТКИ ПЕРСОНАЛЬНОЙ ИНФОРМАЦИИ</h2>\n\n<p>5.1. Обработка персональных данных Пользователя осуществляется без ограничения срока, любым законным способом, в том числе в информационных системах персональных данных с использованием средств автоматизации или без использования таких средств.</p>\n\n<p>5.2. Пользователь соглашается с тем, что Администрация сайта вправе передавать персональные данные третьим лицам, в частности, курьерским службам, организациями почтовой связи, операторам электросвязи, исключительно в целях выполнения заявок Пользователя, оформленных на сайте, в рамках Договора публичной оферты.</p>\n\n<p>5.3. Персональные данные Пользователя могут быть переданы уполномоченным органам государственной власти только по основаниям и в порядке, установленным действующим законодательством.</p>\n\n<h2>6. ОБЯЗАТЕЛЬСТВА СТОРОН</h2>\n\n<p>6.1. Пользователь обязуется:</p>\n\n<p>6.1.1. Предоставить корректную и правдивую информацию о персональных данных, необходимую для пользования сайтом.</p>\n\n<p>6.1.2. Обновить или дополнить предоставленную информацию о персональных данных в случае изменения данной информации.</p>\n\n<p>6.1.3. Принимать меры для защиты доступа к своим конфиденциальным данным, хранящимся на сайте.</p>\n\n<p>6.2. Администрация сайта обязуется:</p>\n\n<p>6.2.1. Использовать полученную информацию исключительно для целей, указанных в п. 4 настоящей Политики конфиденциальности.</p>\n\n<p>6.2.2. Не разглашать персональных данных Пользователя, за исключением п.п. 5.2. и 5.3. настоящей Политики Конфиденциальности.</p>\n\n<p>6.2.3. Осуществить блокирование персональных данных, относящихся к соответствующему Пользователю, с момента обращения или запроса Пользователя или его законного представителя либо уполномоченного органа по защите прав субъектов персональных данных на период проверки, в случае выявления недостоверных персональных данных или неправомерных действий.</p>\n\n<h2>7. ОТВЕТСТВЕННОСТЬ СТОРОН</h2>\n\n<p>7.1. Администрация сайта несёт ответственность за умышленное разглашение Персональных данных Пользователя в соответствии с действующим законодательством, за исключением случаев, предусмотренных п.п. 5.2., 5.3. и 7.2. настоящей Политики Конфиденциальности.</p>\n\n<p>7.2. В случае утраты или разглашения Персональных данных Администрация сайта не несёт ответственность, если данная конфиденциальная информация:</p>\n\n<p>7.2.1. Стала публичным достоянием до её утраты или разглашения.</p>\n\n<p>7.2.2. Была получена от третьей стороны до момента её получения Администрацией сайта.</p>\n\n<p>7.2.3. Была получена третьими лицами путем несанкционированного доступа к файлам сайта.</p>\n\n<p>7.2.4. Была разглашена с согласия Пользователя.</p>\n\n<p>7.3. Пользователь несет ответственность за правомерность, корректность и правдивость предоставленной Персональных данных в соответствии с действующим законодательством.</p>\n\n<h2>8. РАЗРЕШЕНИЕ СПОРОВ</h2>\n\n<p>8.1. До обращения в суд с иском по спорам, возникающим из отношений между Пользователем сайта и Администрацией сайта, обязательным является предъявление претензии (письменного предложения о добровольном урегулировании спора).</p>\n\n<p>8.2 .Получатель претензии в течение 30 календарных дней со дня получения претензии, письменно уведомляет заявителя претензии о результатах рассмотрения претензии.</p>\n\n<p>8.3. При не достижении соглашения спор будет передан на рассмотрение в судебный орган в соответствии с действующим законодательством.</p>\n\n<p>8.4. К настоящей Политике конфиденциальности и отношениям между Пользователем и Администрацией сайта применяется действующее законодательство.</p>\n\n<h2>9. ДОПОЛНИТЕЛЬНЫЕ УСЛОВИЯ</h2>\n\n<p>9.1. Администрация сайта вправе вносить изменения в настоящую Политику конфиденциальности без согласия Пользователя.</p>\n\n<p>9.2. Новая Политика конфиденциальности вступает в силу с момента ее размещения на Сайте, если иное не предусмотрено новой редакцией Политики конфиденциальности.</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1112', '<p>Производство наружной рекламы — это изготовление, стилистика, дизайн, монтаж рекламных носителей на зданиях и на улицах города. К наружной рекламе относятся: вывески, наружные плакаты, рекламные щиты, брэндмауэры, световые установки, перетяжки, козырьки</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1113', '<p>Фартук для кухни может быть изготовлен любых размеров, поэтому он подходит для отделки и небольших и просторных помещений</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1114', '<p>Материалом для производства служат натуральный холст, натянутый на подрамник или пластик ПВХ, толщиной 3-6мм</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1118', '<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно, то есть:</p>\n\n<ul>\n	<li>быть презентабельным и интересным (нести четкую и конкретную идею);</li>\n	<li>предоставлять всю необходимую информацию;</li>\n	<li>обладать индивидуальным визуальным оформлением;</li>\n	<li>выглядеть надежным и качественным (иначе как клиент поймет, что вам можно доверять);</li>\n	<li>являться настоящим рекламным провокатором, чтобы клиент решил: «Куплю всё, что эти рекламные стенды предлагают».</li>\n</ul>\n\n<p><strong>Как добиться такого рекламного эффекта?</strong></p>\n\n<p>Выбрать, каким будет ваш рекламный стенд. Если решение дается трудно, стоит обратиться к мастеру создания стендов, например к нашей компании. «Белая полоса» – настоящий специалист в области торгового и выставочного оборудования любой сложности.</p>\n\n<p><strong>Каким может быть ваш рекламный стенд?</strong></p>\n\n<p>Наш ответ: любым, каким вы пожелаете его видеть:<br />\n<strong>Мобильные стенды</strong> – простой, эффективный и, главное, экономичный способ выразительно представить свою компанию. Обычно производство рекламных стендов этого типа требует незначительного количества материалов, но большого внимания к точности. Для создания этих стендов используются металлический каркас и закрепленное на нем изображение. Данный тип конструкции вариативен в плане исполнения: Banner-up, Salebox, Roll-Up, Pop-Up, Fold-Up, EasyShow, Пикколо и т. д. Каждая из систем обладает своими преимуществами, подробнее о которых вы можете узнать у наших специалистов.</p>\n\n<p><strong>Стационарные стенды</strong> отличаются простотой исполнения и надежностью, могут быть выполнены в любом формате (от маленьких или высоких напольных стеллажей до простых стендов баннерного типа).</p>\n\n<p><strong>Эксклюзивные стенды</strong> могут быть все той же мобильной конструкцией, но созданной не по шаблону, а по индивидуально разработанному дизайну. Для реализации эксклюзивного стенда за основу берется тот же мобильный механизм, но его конструкция, вид и форма могут быть выполнены в совершенно неожиданной манере. Например, одни элементы стенда могут быть статичными, а другие – вращаться, это могут быть простые или перфорированные полотна с картинками или карманами для буклетов.</p>\n\n<p>Также мы с удовольствием предложим вам один или несколько вариантов яркого, оригинального дизайна.<br />\nМатериалы, которые мы используем для создания рекламных стендов: металл, дерево, пластик, ПВХ, оргстекло и др. Для нанесения изображения применяются обычные типографские методы, качественные и проверенные временем.</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1119', '<p>Современный дизайн обоев для гостиной отличается разнообразием, для любого стиля интерьера можно найти интересные варианты, подчеркивающие вкус и индивидуальность хозяина квартиры.</p>\n\n<p>При выборе обоев для необходимо в первую очередь учитывать, какое помещение планируется оформлять. Условия освещенности, размер и форма комнаты играют определяющую роль, ведь цвет, насыщенность тона а также рисунок настенного покрытия могут значительно изменить восприятие интерьера. </p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1120', '<p>Наружная реклама — это носители рекламы, которые располагаются вне помещений, воздействуя на широкую аудиторию. Это эффективное средство, направленное в основном на продвижение потребительских товаров и услуг, брэндов и рассчитанное на восприятие широкими слоями населения.</p>\n\n<p>Производство наружной рекламы — это изготовление, стилистика, дизайн, монтаж рекламных носителей на зданиях и на улицах города. К наружной рекламе относятся: вывески, наружные плакаты, рекламные щиты, брэндмауэры, световые установки, перетяжки, козырьки.</p>\n\n<p>Стоит отметить, что наружная реклама не является начальным этапом рекламной кампании, она ее продолжает.</p>\n\n<p>Наружная реклама должна выполнять следующие функции: -привлекать внимание, находясь постоянно на видном месте.</p>\n\n<ul>\n	<li>Быстро «схватываться»;</li>\n	<li>Остается в памяти человека при минимальном контакте;</li>\n	<li>Обеспечить высокую частоту показов.</li>\n	<li>Рассмотрим несколько видов рекламных носителей.</li>\n</ul>\n\n<p><strong>Акрилайт</strong> — один из видов рекламных щитов. Прозрачное панно из толстого оргстекла акриловой группы с выгравированным текстом, рисунком или логотипом, с использованием с торцевой стороны люминесцентной или светодиодной подсветки. В результате чего возникает эффект свечения выгравированного изображения. Считается одним из самых распространенных видов наружной рекламы. Бывают напольные, настенные, подвесные.</p>\n\n<p><strong>Афиша </strong>— вид печатного рекламного носителя, оповещающего широкую аудиторию о каких — либо предстоящих мероприятиях, являющегося анонсом какого-нибудь предстоящего события, представляет собой печатаемое одностороннее издание на крупноформатной бумаге или картоне, предназначенное для расклейки.</p>\n\n<p><strong>Баннер</strong> — это полотняный рекламный щит, который может быть украшен полноцветными красочными изображениями. Изображение, напечатанное на баннерном полотне более долговечно, чем напечатанное на бумаге.</p>\n\n<p><strong>Брандмауэр</strong> — глухая, без окон открытая для обзора стена здания, используемая для размещения рекламы, в виде рекламного щита или натянутого панно, обычно больших размеров. Изображение, также может наноситься на штукатурку стены здания.</p>\n\n<p><strong>Витрина</strong> — одно из средств рекламы в местах продаж. Выкладка товаров отделена от потребителя прозрачной перегородкой. Главная задача витрины обратить на себя внимание на подсознательном уровне. Витрины бывают открытого, закрытого и комбинированного типа. Через открытые витрины можно видеть внутреннее устройство магазина, и покупатель с открытой витрины взять любой товар. Через закрытые витрины внутреннее убранство магазина не видно, товар может взять только продавец. Витрины комбинированного типа — часть торгового зала отгораживается от витрины при помощи шторы, стенки или перегородки, а часть остается обозримой.</p>\n\n<p><strong>Вывеска </strong>— одно из средств наружной рекламы, размещаемое в местах торговых точек, а также в местах расположения компаний и предприятий. Представляет собой щит, содержащий названия предприятия, фирменный стиль и пиктограммы, которые указывают на сферу деятельности предприятия. Вывески бывают рисованные или неоновые. Живописная вывеска имеет функциональную схожесть с обычной вывеской, но выполняется посредством росписи стен, используя технологии граффити или аэрографии</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1121', '<p>Квартира современного человека должна быть модной и стильной. Особенно это касается кухни, где люди проводят больше всего времени, собираясь семьей или с друзьями.<br />\nСегодня это возможно при помощи стеклянного фартука .<br />\nЭтот вариант для людей, знающих толк в красоте, и имеющих безупречное чувство стиля.<br />\nПодобные Фартуки для кухни из стекла с изображенной на них фотопечатью выгодно заменяют собой стандартную плитку.</p>\n\n<p>Даже самый дорогостоящий материал не обладает теми возможностями, которые присущи.<br />\nС его помощью легко можно создать самые невероятные картины и сделать свою кухню уникальной и неповторимой.<br />\nФартук для кухни может быть изготовлен любых размеров, поэтому он подходит для отделки и небольших и просторных помещений.<br />\nТакие панели великолепно вписываются в интерьер и становятся логическим завершением общей концепции.<br />\nНовая технология нанесения 3D-картинок позволяет получить объемный и глубокий рисунок, что делает его ярким акцентом на однотонных поверхностях или гармоничным дополнением общего декора.</p>\n\n<p>Наши дизайнеры создадут макет по вашим требования и пожеланиям, подберут цветовую гамму и при необходимости сделают коллаж из нескольких изображений.</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1122', '<p>Одним из наших профессиональных направлений является изготовление картин в интерьер.</p>\n\n<p>Это могут быть репродукции великих художников, фотографии родных или просто постер на Ваш выбор.</p>\n\n<p>На сегодняшний день, особой популярностью пользуются модульные картины или как их еще называют сегментные картины.</p>\n\n<p>Материалом для производства служат натуральный холст, натянутый на подрамник или пластик ПВХ, толщиной 3-6мм.</p>\n\n<p> </p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1126', '<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>\n\n<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>\n\n<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>\n\n<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1127', '<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>\n\n<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>\n\n<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>\n\n<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1128', '<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>\n\n<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>\n\n<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>\n\n<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1129', '<p>Материалом для производства служат натуральный холст, натянутый на подрамник или пластик ПВХ, толщиной 3-6мм</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1130', '<p>Материалом для производства служат натуральный холст, натянутый на подрамник или пластик ПВХ, толщиной 3-6мм</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1131', '<p>Материалом для производства служат натуральный холст, натянутый на подрамник или пластик ПВХ, толщиной 3-6мм</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1132', '<p>Материалом для производства служат натуральный холст, натянутый на подрамник или пластик ПВХ, толщиной 3-6мм</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('1137', '<p>Выражаем благодарность за хорошую и качественную работу, профессионализм и терпение. Много времени было потрачено на выбор специалиста, Артема мы нашли случайно и не пожалели о выборе. Наш сайт получился просто превосходным! Артема отличает не только профессионализм, но и персональный подход к делу. Сразу уловил все наши пожелания, учел все, что хотелось. Хочется отметить работу уже после завершения: всегда можно обратиться за помощью, в которой никто не отказывает, а наоборот, с энтузиазмом помогает, предлагает новые решения, при появлении каких-то вопросов. Только положительные эмоции.</p>\n\n<p>Выражаем благодарность за хорошую и качественную работу, профессионализм и терпение. Много времени было потрачено на выбор специалиста, Артема мы нашли случайно и не пожалели о выборе. Наш сайт получился просто превосходным! Артема отличает не только профессионализм, но и персональный подход к делу. Сразу уловил все наши пожелания, учел все, что хотелось. Хочется отметить работу уже после завершения: всегда можно обратиться за помощью, в которой никто не отказывает, а наоборот, с энтузиазмом помогает, предлагает новые решения, при появлении каких-то вопросов. Только положительные эмоции.</p>');
INSERT INTO `field_body` (`pages_id`, `data`) VALUES('27', '<p>Неправильно набран адрес или страницы не существует.</p>\n\n<p>Вы можете вернуться на <ins><strong><a href=\"/\">главную страницу</a></strong></ins> или воспользоваться навигационным меню для перехода на другие страниц сайта.</p>\n\n<p> </p>');

DROP TABLE IF EXISTS `field_carousel`;
CREATE TABLE `field_carousel` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_carousel_classes`;
CREATE TABLE `field_carousel_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_carousel_classes` (`pages_id`, `data`) VALUES('1', 'carousel-fade w-100 h-50');

DROP TABLE IF EXISTS `field_carousel_end`;
CREATE TABLE `field_carousel_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_carousel_id`;
CREATE TABLE `field_carousel_id` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_carousel_id` (`pages_id`, `data`) VALUES('1', 'carousel-home');

DROP TABLE IF EXISTS `field_carousel_image_desc`;
CREATE TABLE `field_carousel_image_desc` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_carousel_image_title`;
CREATE TABLE `field_carousel_image_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_carousel_images`;
CREATE TABLE `field_carousel_images` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_carousel_next_icon`;
CREATE TABLE `field_carousel_next_icon` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_carousel_next_icon` (`pages_id`, `data`) VALUES('1', 'fal fa-chevron-right custom-size');

DROP TABLE IF EXISTS `field_carousel_prew_icon`;
CREATE TABLE `field_carousel_prew_icon` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_carousel_prew_icon` (`pages_id`, `data`) VALUES('1', 'fal fa-chevron-left custom-size');

DROP TABLE IF EXISTS `field_carousel_repeater`;
CREATE TABLE `field_carousel_repeater` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_carousel_repeater` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1', '1058,1059,1060,1079', '4', '1057');

DROP TABLE IF EXISTS `field_col_generator`;
CREATE TABLE `field_col_generator` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_col_generator` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1025', '1041', '1', '1040');

DROP TABLE IF EXISTS `field_col_generator_classes`;
CREATE TABLE `field_col_generator_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_col_generator_classes` (`pages_id`, `data`) VALUES('1041', 'pt-3 pb-1 color-white text-center');

DROP TABLE IF EXISTS `field_col_generator_grid`;
CREATE TABLE `field_col_generator_grid` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_col_generator_grid` (`pages_id`, `data`) VALUES('1041', 'col-sm-12');

DROP TABLE IF EXISTS `field_contacts_fields`;
CREATE TABLE `field_contacts_fields` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_contacts_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1032', '1140,1141,1142,1143,1144,1145', '6', '1139');

DROP TABLE IF EXISTS `field_date`;
CREATE TABLE `field_date` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1127', '2019-03-01 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1126', '2019-03-01 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1052', '2018-12-07 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1085', '2018-12-09 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1088', '2018-12-09 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1089', '2018-12-09 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1090', '2018-12-09 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1091', '2018-12-09 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1092', '2018-12-09 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1093', '2018-12-09 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1094', '2018-12-09 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1095', '2018-12-09 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1096', '2018-12-09 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1054', '2018-12-10 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1108', '2018-12-17 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1031', '2018-12-19 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1109', '2018-12-20 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1110', '2018-12-20 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1125', '2019-03-01 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1029', '2019-01-16 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1116', '2019-01-16 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1146', '2019-03-08 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1117', '2019-02-12 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1118', '2019-02-12 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1119', '2019-02-12 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1120', '2019-02-12 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1121', '2019-02-12 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1122', '2019-02-12 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1128', '2019-03-01 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1133', '2019-03-03 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1134', '2019-03-05 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1135', '2019-03-05 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1136', '2019-03-05 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1137', '2019-03-05 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1147', '2019-03-08 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('27', '2019-03-08 00:00:00');

DROP TABLE IF EXISTS `field_email`;
CREATE TABLE `field_email` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_email` (`pages_id`, `data`) VALUES('41', 'vip.9900@bk.ru');

DROP TABLE IF EXISTS `field_field_classes`;
CREATE TABLE `field_field_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_classes` (`pages_id`, `data`) VALUES('1104', 'form-control rounded-0');
INSERT INTO `field_field_classes` (`pages_id`, `data`) VALUES('1105', 'custom-control-input');

DROP TABLE IF EXISTS `field_field_desc`;
CREATE TABLE `field_field_desc` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_desc` (`pages_id`, `data`) VALUES('1105', '<p>Согласие с условиями <a href=\"/politika-konfidetcialnosti/\">конфидециальности</a></p>');

DROP TABLE IF EXISTS `field_field_error`;
CREATE TABLE `field_field_error` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_error` (`pages_id`, `data`) VALUES('1097', 'Поле \"Имя\" обязательно для заполнения');
INSERT INTO `field_field_error` (`pages_id`, `data`) VALUES('1098', 'Поле \"e-mail\" обязательно для заполнения');
INSERT INTO `field_field_error` (`pages_id`, `data`) VALUES('1104', 'Поле \"Сообщение\" обязательно для заполнения');
INSERT INTO `field_field_error` (`pages_id`, `data`) VALUES('1086', 'Что-то пошло не так. Форма не отправлена');
INSERT INTO `field_field_error` (`pages_id`, `data`) VALUES('1105', 'Вы должны принять условия');

DROP TABLE IF EXISTS `field_field_hidden`;
CREATE TABLE `field_field_hidden` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_field_label`;
CREATE TABLE `field_field_label` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_label` (`pages_id`, `data`) VALUES('1097', 'Ваше Имя');
INSERT INTO `field_field_label` (`pages_id`, `data`) VALUES('1098', 'Ваш e-mail');
INSERT INTO `field_field_label` (`pages_id`, `data`) VALUES('1104', 'Напишите нам сообщение');
INSERT INTO `field_field_label` (`pages_id`, `data`) VALUES('1140', 'Адрес');
INSERT INTO `field_field_label` (`pages_id`, `data`) VALUES('1141', 'График работы');
INSERT INTO `field_field_label` (`pages_id`, `data`) VALUES('1143', 'Телефон');
INSERT INTO `field_field_label` (`pages_id`, `data`) VALUES('1145', 'E-mail');

DROP TABLE IF EXISTS `field_field_label_classes`;
CREATE TABLE `field_field_label_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_label_classes` (`pages_id`, `data`) VALUES('1097', 'mb-4 col-sm-6 d-block');
INSERT INTO `field_field_label_classes` (`pages_id`, `data`) VALUES('1098', 'mb-4 col-sm-6 d-block');
INSERT INTO `field_field_label_classes` (`pages_id`, `data`) VALUES('1104', 'd-block col-sm-12 mb-3');
INSERT INTO `field_field_label_classes` (`pages_id`, `data`) VALUES('1105', 'custom-control-label');

DROP TABLE IF EXISTS `field_field_name`;
CREATE TABLE `field_field_name` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_name` (`pages_id`, `data`) VALUES('1097', 'name');
INSERT INTO `field_field_name` (`pages_id`, `data`) VALUES('1098', 'email');
INSERT INTO `field_field_name` (`pages_id`, `data`) VALUES('1104', 'message');
INSERT INTO `field_field_name` (`pages_id`, `data`) VALUES('1105', 'agreement');

DROP TABLE IF EXISTS `field_field_on`;
CREATE TABLE `field_field_on` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_on` (`pages_id`, `data`) VALUES('1105', '1');
INSERT INTO `field_field_on` (`pages_id`, `data`) VALUES('1117', '1');

DROP TABLE IF EXISTS `field_field_options`;
CREATE TABLE `field_field_options` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_field_pattern`;
CREATE TABLE `field_field_pattern` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_pattern` (`pages_id`, `data`) VALUES('1097', '[А-Яа-яЁё ]{4,}');
INSERT INTO `field_field_pattern` (`pages_id`, `data`) VALUES('1098', '([A-z0-9_.-]{1,})@([A-z0-9_.-]{1,}).([A-z]{2,8})');

DROP TABLE IF EXISTS `field_field_placeholder`;
CREATE TABLE `field_field_placeholder` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_placeholder` (`pages_id`, `data`) VALUES('1097', 'Иванов Иван Иванович');
INSERT INTO `field_field_placeholder` (`pages_id`, `data`) VALUES('1098', 'somemail@yandex.ru');
INSERT INTO `field_field_placeholder` (`pages_id`, `data`) VALUES('1104', 'Ваше сообщение');

DROP TABLE IF EXISTS `field_field_required`;
CREATE TABLE `field_field_required` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_required` (`pages_id`, `data`) VALUES('1097', '1');
INSERT INTO `field_field_required` (`pages_id`, `data`) VALUES('1098', '1');
INSERT INTO `field_field_required` (`pages_id`, `data`) VALUES('1104', '1');
INSERT INTO `field_field_required` (`pages_id`, `data`) VALUES('1105', '1');

DROP TABLE IF EXISTS `field_field_success`;
CREATE TABLE `field_field_success` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_success` (`pages_id`, `data`) VALUES('1086', 'Форма успешно улетела нам на ящик');

DROP TABLE IF EXISTS `field_field_type`;
CREATE TABLE `field_field_type` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_field_type` (`pages_id`, `data`, `sort`) VALUES('1097', '1089', '0');
INSERT INTO `field_field_type` (`pages_id`, `data`, `sort`) VALUES('1098', '1093', '0');
INSERT INTO `field_field_type` (`pages_id`, `data`, `sort`) VALUES('1104', '1109', '0');
INSERT INTO `field_field_type` (`pages_id`, `data`, `sort`) VALUES('1105', '1110', '0');

DROP TABLE IF EXISTS `field_field_value`;
CREATE TABLE `field_field_value` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_files`;
CREATE TABLE `field_files` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_flip_card_classes`;
CREATE TABLE `field_flip_card_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_flip_card_classes` (`pages_id`, `data`) VALUES('1', 'col-sm-12 col-md-6 col-lg-4 mb-4');

DROP TABLE IF EXISTS `field_flip_cards`;
CREATE TABLE `field_flip_cards` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_flip_cards_end`;
CREATE TABLE `field_flip_cards_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_flip_cards_repeater`;
CREATE TABLE `field_flip_cards_repeater` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_flip_cards_repeater` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1', '1065,1067,1112,1113,1114,1129,1130,1131,1132', '9', '1064');

DROP TABLE IF EXISTS `field_flip_cards_section_classes`;
CREATE TABLE `field_flip_cards_section_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_flip_cards_section_image`;
CREATE TABLE `field_flip_cards_section_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_flip_cards_section_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1', 'goroda-ogni-nochnogo-477389.jpg', '0', '[null]', '2019-03-01 00:03:18', '2019-03-01 00:03:18', '');

DROP TABLE IF EXISTS `field_flip_cards_section_paralax`;
CREATE TABLE `field_flip_cards_section_paralax` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_flip_cards_section_paralax` (`pages_id`, `data`) VALUES('1', '1');

DROP TABLE IF EXISTS `field_flip_cards_title`;
CREATE TABLE `field_flip_cards_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_flip_cards_title` (`pages_id`, `data`) VALUES('1', 'Наши услуги');

DROP TABLE IF EXISTS `field_flip_cards_title_classes`;
CREATE TABLE `field_flip_cards_title_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_flip_cards_title_classes` (`pages_id`, `data`) VALUES('1', 'text-uppercase pt-5');

DROP TABLE IF EXISTS `field_footer`;
CREATE TABLE `field_footer` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_footer_end`;
CREATE TABLE `field_footer_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_footer_section_classes`;
CREATE TABLE `field_footer_section_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_form`;
CREATE TABLE `field_form` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_form_classes`;
CREATE TABLE `field_form_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_form_classes` (`pages_id`, `data`) VALUES('1086', 'p-5 text-white rgba-black-strong form');

DROP TABLE IF EXISTS `field_form_email`;
CREATE TABLE `field_form_email` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_form_email` (`pages_id`, `data`) VALUES('1086', 'muhomor.ra@mail.ru');

DROP TABLE IF EXISTS `field_form_end`;
CREATE TABLE `field_form_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_form_id`;
CREATE TABLE `field_form_id` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_form_id` (`pages_id`, `data`) VALUES('1086', 'contacts_form');

DROP TABLE IF EXISTS `field_form_image`;
CREATE TABLE `field_form_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_form_page`;
CREATE TABLE `field_form_page` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_form_page` (`pages_id`, `data`, `sort`) VALUES('1', '1086', '0');

DROP TABLE IF EXISTS `field_form_section_classes`;
CREATE TABLE `field_form_section_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_form_section_classes` (`pages_id`, `data`) VALUES('1', 'stylish-color');

DROP TABLE IF EXISTS `field_form_section_image`;
CREATE TABLE `field_form_section_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_form_section_parallax`;
CREATE TABLE `field_form_section_parallax` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_form_section_title`;
CREATE TABLE `field_form_section_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_form_section_title` (`pages_id`, `data`) VALUES('1', 'text-uppercase text-white text-center pt-5 pb-3');

DROP TABLE IF EXISTS `field_form_submit_classes`;
CREATE TABLE `field_form_submit_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_form_submit_classes` (`pages_id`, `data`) VALUES('1086', 'btn danger-color-dark');

DROP TABLE IF EXISTS `field_form_submit_text`;
CREATE TABLE `field_form_submit_text` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_form_submit_text` (`pages_id`, `data`) VALUES('1086', 'Отправить');

DROP TABLE IF EXISTS `field_form_theme`;
CREATE TABLE `field_form_theme` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_form_theme` (`pages_id`, `data`) VALUES('1086', 'Письмо с сайта Грузчики 77');

DROP TABLE IF EXISTS `field_form_title`;
CREATE TABLE `field_form_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_form_title` (`pages_id`, `data`) VALUES('1', 'Напишите нам');

DROP TABLE IF EXISTS `field_gallery`;
CREATE TABLE `field_gallery` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_gallery2`;
CREATE TABLE `field_gallery2` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_gallery2_classes`;
CREATE TABLE `field_gallery2_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery2_classes` (`pages_id`, `data`) VALUES('1', 'view light-section gallery pb-5');

DROP TABLE IF EXISTS `field_gallery2_count`;
CREATE TABLE `field_gallery2_count` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery2_count` (`pages_id`, `data`) VALUES('1', '4');

DROP TABLE IF EXISTS `field_gallery2_end`;
CREATE TABLE `field_gallery2_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_gallery2_image`;
CREATE TABLE `field_gallery2_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery2_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1', '10.jpg', '0', '[null]', '2019-03-08 23:21:57', '2019-03-08 23:21:57', '');

DROP TABLE IF EXISTS `field_gallery2_images`;
CREATE TABLE `field_gallery2_images` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_gallery2_images_classes`;
CREATE TABLE `field_gallery2_images_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery2_images_classes` (`pages_id`, `data`) VALUES('1', 'col-sm-6 col-md-4 col-xl-3');

DROP TABLE IF EXISTS `field_gallery2_img_height`;
CREATE TABLE `field_gallery2_img_height` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery2_img_height` (`pages_id`, `data`) VALUES('1', '800');

DROP TABLE IF EXISTS `field_gallery2_img_width`;
CREATE TABLE `field_gallery2_img_width` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery2_img_width` (`pages_id`, `data`) VALUES('1', '560');

DROP TABLE IF EXISTS `field_gallery2_page`;
CREATE TABLE `field_gallery2_page` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery2_page` (`pages_id`, `data`, `sort`) VALUES('1', '1029', '0');

DROP TABLE IF EXISTS `field_gallery2_parallax`;
CREATE TABLE `field_gallery2_parallax` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery2_parallax` (`pages_id`, `data`) VALUES('1', '1');

DROP TABLE IF EXISTS `field_gallery2_popup`;
CREATE TABLE `field_gallery2_popup` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_gallery2_title`;
CREATE TABLE `field_gallery2_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery2_title` (`pages_id`, `data`) VALUES('1', 'Благодарственные письма');

DROP TABLE IF EXISTS `field_gallery2_title_classes`;
CREATE TABLE `field_gallery2_title_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery2_title_classes` (`pages_id`, `data`) VALUES('1', 'text-center text-uppercase pt-5 pb-3');

DROP TABLE IF EXISTS `field_gallery_classes`;
CREATE TABLE `field_gallery_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery_classes` (`pages_id`, `data`) VALUES('1', 'view light-section gallery pb-5');

DROP TABLE IF EXISTS `field_gallery_count`;
CREATE TABLE `field_gallery_count` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery_count` (`pages_id`, `data`) VALUES('1', '8');

DROP TABLE IF EXISTS `field_gallery_end`;
CREATE TABLE `field_gallery_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_gallery_image`;
CREATE TABLE `field_gallery_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1', '1114164-new-york-street-night-1.jpg', '0', '[null]', '2019-03-01 00:09:54', '2019-03-01 00:09:54', '');

DROP TABLE IF EXISTS `field_gallery_images`;
CREATE TABLE `field_gallery_images` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_gallery_images_classes`;
CREATE TABLE `field_gallery_images_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery_images_classes` (`pages_id`, `data`) VALUES('1', 'col-sm-6 col-md-4 col-xl-3');

DROP TABLE IF EXISTS `field_gallery_page`;
CREATE TABLE `field_gallery_page` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery_page` (`pages_id`, `data`, `sort`) VALUES('1', '1116', '0');

DROP TABLE IF EXISTS `field_gallery_parallax`;
CREATE TABLE `field_gallery_parallax` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery_parallax` (`pages_id`, `data`) VALUES('1', '1');

DROP TABLE IF EXISTS `field_gallery_popup`;
CREATE TABLE `field_gallery_popup` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery_popup` (`pages_id`, `data`) VALUES('1', '1');

DROP TABLE IF EXISTS `field_gallery_title`;
CREATE TABLE `field_gallery_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery_title` (`pages_id`, `data`) VALUES('1', 'Портфолио');

DROP TABLE IF EXISTS `field_gallery_title_classes`;
CREATE TABLE `field_gallery_title_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_gallery_title_classes` (`pages_id`, `data`) VALUES('1', 'text-center text-uppercase pt-5 pb-3');

DROP TABLE IF EXISTS `field_header`;
CREATE TABLE `field_header` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_header_end`;
CREATE TABLE `field_header_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_header_navbar_classes`;
CREATE TABLE `field_header_navbar_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_header_navbar_logo`;
CREATE TABLE `field_header_navbar_logo` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_header_navbar_logo` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1025', 'pilz-coloured.png', '0', '[null]', '2019-02-27 23:53:50', '2019-02-27 23:53:50', '');

DROP TABLE IF EXISTS `field_header_section_classes`;
CREATE TABLE `field_header_section_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_image`;
CREATE TABLE `field_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1121', '16.jpg', '0', '[null]', '2019-03-01 18:24:01', '2019-03-01 18:24:01', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1067', '12.jpg', '0', '[null]', '2019-03-01 18:34:07', '2019-03-01 18:34:07', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1119', '12.jpg', '0', '[null]', '2019-03-01 18:24:26', '2019-03-01 18:24:26', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1077', 'tver1.jpg', '0', '[null]', '2018-12-06 09:05:34', '2018-12-06 09:05:34', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1076', 'flip-img3.jpg', '0', '[null]', '2018-12-06 08:58:28', '2018-12-06 08:58:28', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1078', 'tver2.jpg', '0', '[null]', '2018-12-06 09:05:34', '2018-12-06 09:05:34', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1122', 'kr.jpg', '0', '[null]', '2019-02-27 23:36:29', '2019-02-27 23:36:29', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1058', '21.jpg', '0', '[null]', '2019-03-01 00:14:19', '2019-03-01 00:14:19', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1128', '23.jpg', '0', '[null]', '2019-03-01 18:22:13', '2019-03-01 18:22:13', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1127', '2.jpg', '0', '[null]', '2019-03-01 18:21:34', '2019-03-01 18:21:34', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1126', '333.jpg', '0', '[null]', '2019-03-01 18:16:45', '2019-03-01 18:16:45', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1125', '21.jpg', '0', '[null]', '2019-03-01 18:14:34', '2019-03-01 18:14:34', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1118', 'st.jpg', '0', '[null]', '2019-02-27 23:27:21', '2019-02-27 23:27:21', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1065', 'st.jpg', '0', '[null]', '2019-02-27 23:20:02', '2019-02-27 23:20:02', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1032', '256a38feeb84cfd4f1752e8b2c.jpg', '0', '[null]', '2019-03-01 18:37:27', '2019-03-01 18:37:27', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1114', 'kr.jpg', '0', '[null]', '2019-02-27 23:20:02', '2019-02-27 23:20:02', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1059', '12.jpg', '0', '[null]', '2019-03-01 00:14:19', '2019-03-01 00:14:19', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1060', '16.jpg', '0', '[null]', '2019-03-01 00:14:19', '2019-03-01 00:14:19', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1079', '23.jpg', '0', '[null]', '2019-03-01 00:14:19', '2019-03-01 00:14:19', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1120', '9.jpg', '0', '[null]', '2019-03-01 18:22:50', '2019-03-01 18:22:50', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1112', '11.jpg', '0', '[null]', '2019-03-01 18:34:07', '2019-03-01 18:34:07', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1113', '16.jpg', '0', '[null]', '2019-03-01 18:34:07', '2019-03-01 18:34:07', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1129', '21.jpg', '0', '[null]', '2019-03-01 18:34:07', '2019-03-01 18:34:07', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1130', '333.jpg', '0', '[null]', '2019-03-01 18:34:07', '2019-03-01 18:34:07', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1131', '2.jpg', '0', '[null]', '2019-03-01 18:34:07', '2019-03-01 18:34:07', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1132', '3.jpg', '0', '[null]', '2019-03-01 18:34:07', '2019-03-01 18:34:07', '');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1137', '121-1.jpg', '0', '[null]', '2019-03-05 08:07:20', '2019-03-05 08:07:20', '');

DROP TABLE IF EXISTS `field_images`;
CREATE TABLE `field_images` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '24.jpg', '23', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '23.jpg', '22', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '22.jpg', '21', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '21.jpg', '20', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '20.jpg', '19', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '19.jpg', '18', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '18.jpg', '17', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '17.jpg', '16', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '16.jpg', '15', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '15.jpg', '14', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '14.jpg', '13', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '13.jpg', '12', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '12.jpg', '11', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '11.jpg', '10', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '10.jpg', '9', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '9.jpg', '8', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '8.jpg', '7', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '7.jpg', '6', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '6.jpg', '5', '[null]', '2019-02-28 23:22:20', '2019-02-28 23:22:20', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '5.jpg', '4', '[null]', '2019-02-28 23:20:11', '2019-02-28 23:20:11', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '4.jpg', '3', '[null]', '2019-02-28 23:20:11', '2019-02-28 23:20:11', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '3.jpg', '2', '[null]', '2019-02-28 23:20:11', '2019-02-28 23:20:11', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '2.jpg', '1', '[null]', '2019-02-28 23:20:11', '2019-02-28 23:20:11', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1116', '1.jpg', '0', '[null]', '2019-02-28 23:20:11', '2019-02-28 23:20:11', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1137', '74501.png', '1', '[null]', '2019-03-05 08:07:03', '2019-03-05 08:07:03', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1137', '121.jpg', '0', '[null]', '2019-03-05 08:07:03', '2019-03-05 08:07:03', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1137', 'tovarnyj-chek-800x551.jpeg', '2', '[null]', '2019-03-05 08:07:03', '2019-03-05 08:07:03', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1029', '121.jpg', '0', '{\"0\":null,\"1022\":\"Отзыв номер один\"}', '2019-03-12 16:09:27', '2019-03-05 08:07:43', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1029', '74501.png', '1', '{\"0\":null,\"1022\":\"Отзыв номер два\"}', '2019-03-12 16:09:27', '2019-03-05 08:07:43', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1029', 'tovarnyj-chek-800x551.jpeg', '2', '[\"Отзыв номер три\"]', '2019-03-12 16:09:27', '2019-03-05 08:07:43', '');

DROP TABLE IF EXISTS `field_images_sections_classes`;
CREATE TABLE `field_images_sections_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_images_sections_classes` (`pages_id`, `data`) VALUES('1029', 'text-white pt-3 pb-3');

DROP TABLE IF EXISTS `field_images_title`;
CREATE TABLE `field_images_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_images_title` (`pages_id`, `data`) VALUES('1029', 'Благодарственные письма');

DROP TABLE IF EXISTS `field_language`;
CREATE TABLE `field_language` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_language` (`pages_id`, `data`, `sort`) VALUES('40', '1018', '0');
INSERT INTO `field_language` (`pages_id`, `data`, `sort`) VALUES('41', '1022', '0');

DROP TABLE IF EXISTS `field_language_files`;
CREATE TABLE `field_language_files` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'site--modules--markupseo-master--markupseo-module.json', '0', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--core--fieldtype-php.json', '1', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--core--inputfield-php.json', '2', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--core--inputfieldwrapper-php.json', '3', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--core--languagefunctions-php.json', '4', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--core--pages-php.json', '5', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--core--sessioncsrf-php.json', '6', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--admintheme--adminthemedefault--adminthemedefault-module.json', '7', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--admintheme--adminthemedefault--adminthemedefaulthelpers-php.json', '8', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--admintheme--adminthemedefault--default-php.json', '9', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--admintheme--adminthemereno--adminthemereno-module.json', '10', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--admintheme--adminthemereno--adminthemerenohelpers-php.json', '11', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--admintheme--adminthemereno--debug-inc.json', '12', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypecomments--commentfilterakismet-module.json', '13', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypecomments--commentform-php.json', '14', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypecomments--commentlist-php.json', '15', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypecomments--commentnotifications-php.json', '16', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypecomments--fieldtypecomments-module.json', '17', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypecomments--inputfieldcommentsadmin-module.json', '18', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypedatetime-module.json', '19', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypefieldsettabopen-module.json', '20', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypefile-module.json', '21', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypefloat-module.json', '22', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypeinteger-module.json', '23', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypemodule-module.json', '24', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypeoptions--fieldtypeoptions-module.json', '25', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypeoptions--selectableoptionconfig-php.json', '26', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypeoptions--selectableoptionmanager-php.json', '27', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypepage-module.json', '28', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypepagetable-module.json', '29', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtyperepeater--fieldtyperepeater-module.json', '30', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtyperepeater--inputfieldrepeater-module.json', '31', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypeselector-module.json', '32', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypetext-module.json', '33', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypetextarea-module.json', '34', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--fieldtype--fieldtypeurl-module.json', '35', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldasmselect--inputfieldasmselect-module.json', '36', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldbutton-module.json', '37', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldcheckbox-module.json', '38', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldcheckboxes--inputfieldcheckboxes-module.json', '39', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldckeditor--inputfieldckeditor-module.json', '40', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfielddatetime--inputfielddatetime-module.json', '41', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldemail-module.json', '42', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldfieldset-module.json', '43', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldfile--inputfieldfile-module.json', '44', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldfloat-module.json', '45', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldform-module.json', '46', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldhidden-module.json', '47', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldicon--inputfieldicon-module.json', '48', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldimage--inputfieldimage-module.json', '49', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldinteger-module.json', '50', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldmarkup-module.json', '51', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldname-module.json', '52', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldpage--inputfieldpage-module.json', '53', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldpageautocomplete--inputfieldpageautocomplete-module.json', '54', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldpagelistselect--inputfieldpagelistselect-module.json', '55', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldpagelistselect--inputfieldpagelistselectmultiple-module.json', '56', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldpagename--inputfieldpagename-module.json', '57', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldpagetable--inputfieldpagetable-module.json', '58', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldpagetable--inputfieldpagetableajax-php.json', '59', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldpagetitle--inputfieldpagetitle-module.json', '60', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldpassword-module.json', '61', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldradios--inputfieldradios-module.json', '62', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldradios-module.json', '63', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldselect-module.json', '64', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldselectmultiple-module.json', '65', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldselector--inputfieldselector-module.json', '66', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldsubmit--inputfieldsubmit-module.json', '67', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldtext-module.json', '68', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldtextarea-module.json', '69', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldtinymce--inputfieldtinymce-module.json', '70', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--inputfield--inputfieldurl-module.json', '71', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--jquery--jquerywiretabs--jquerywiretabs-module.json', '72', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--languagesupport--languageparser-php.json', '73', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--languagesupport--languagesupport-module.json', '74', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--languagesupport--languagesupportfields-module.json', '75', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--languagesupport--languagesupportpagenames-module.json', '76', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--languagesupport--languagetabs-module.json', '77', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--languagesupport--processlanguage-module.json', '78', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--markup--markuppagefields-module.json', '79', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--markup--markuppagernav--markuppagernav-module.json', '80', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--pagepaths-module.json', '81', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--pagerender-module.json', '82', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processcommentsmanager--processcommentsmanager-module.json', '83', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processfield--processfield-module.json', '84', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processfield--processfieldexportimport-php.json', '85', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processforgotpassword-module.json', '86', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processhome-module.json', '87', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processlist-module.json', '88', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processlogger--processlogger-module.json', '89', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processlogin--processlogin-module.json', '90', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processmodule--processmodule-module.json', '91', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processmodule--processmoduleinstall-php.json', '92', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpageadd--processpageadd-module.json', '93', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpageclone-module.json', '94', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpageedit--pagebookmarks-php.json', '95', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpageedit--processpageedit-module.json', '96', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpageeditimageselect--processpageeditimageselect-module.json', '97', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpageeditlink--processpageeditlink-module.json', '98', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpagelist--processpagelist-module.json', '99', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpagelist--processpagelistactions-php.json', '100', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpagelist--processpagelistrender-php.json', '101', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpagelist--processpagelistrenderjson-php.json', '102', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpagelister--processpagelister-module.json', '103', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpagelister--processpagelisterbookmarks-php.json', '104', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpagesearch--processpagesearch-module.json', '105', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpagesort-module.json', '106', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpagetrash-module.json', '107', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpagetype--processpagetype-module.json', '108', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpageview-module.json', '109', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processpermission--processpermission-module.json', '110', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processprofile--processprofile-module.json', '111', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processrecentpages--processrecentpages-module.json', '112', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processrole--processrole-module.json', '113', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processtemplate--processtemplate-module.json', '114', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processtemplate--processtemplateexportimport-php.json', '115', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processuser--processuser-module.json', '116', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--process--processuser--processuserconfig-php.json', '117', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--session--sessionhandlerdb--processsessiondb-module.json', '118', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--session--sessionhandlerdb--sessionhandlerdb-module.json', '119', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--session--sessionloginthrottle--sessionloginthrottle-module.json', '120', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--system--systemnotifications--systemnotifications-module.json', '121', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--system--systemnotifications--systemnotificationsconfig-php.json', '122', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--system--systemupdater--systemupdater-module.json', '123', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--textformatter--textformatterentities-module.json', '124', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--textformatter--textformattermarkdownextra--markdown-php.json', '125', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--textformatter--textformattermarkdownextra--parsedown--parsedown-php.json', '126', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--modules--textformatter--textformattermarkdownextra--textformattermarkdownextra-module.json', '127', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--templates-admin--debug-inc.json', '128', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--templates-admin--default-php.json', '129', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1022', 'wire--templates-admin--topnav-inc.json', '130', '[\"\"]', '2018-11-15 22:42:09', '2018-11-15 22:42:09', '');

DROP TABLE IF EXISTS `field_language_files_site`;
CREATE TABLE `field_language_files_site` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_language_ru`;
CREATE TABLE `field_language_ru` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_languige`;
CREATE TABLE `field_languige` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_languige_end`;
CREATE TABLE `field_languige_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_links`;
CREATE TABLE `field_links` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1067', '/uslugi/oboi/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1112', '/uslugi/naruznaya-reklama/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1058', '/uslugi/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1065', '/uslugi/ukazateli/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1076', 'http://yandex.ru');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1077', 'http://mail.ru');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1078', 'http://google.com');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1059', '/uslugi/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1060', '/uslugi/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1079', '/uslugi/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1113', '/uslugi/fartuki/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1114', '/uslugi/kartini/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1129', '/uslugi/art-ob-ekty/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1130', '/uslugi/shirokoformatnaia-pechat/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1131', '/uslugi/poligrafiia/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1132', '/uslugi/brendirovanie-avtomobilei/');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1143', 'tel:89195555464');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1144', 'tel:83496345464');
INSERT INTO `field_links` (`pages_id`, `data`) VALUES('1145', 'mailto:muhomor.ra@mail.ru');

DROP TABLE IF EXISTS `field_main_copy`;
CREATE TABLE `field_main_copy` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_main_desc`;
CREATE TABLE `field_main_desc` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_main_email`;
CREATE TABLE `field_main_email` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_main_phone`;
CREATE TABLE `field_main_phone` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_map`;
CREATE TABLE `field_map` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_map_center`;
CREATE TABLE `field_map_center` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_map_center` (`pages_id`, `data`) VALUES('1', '[63.207720840077165,75.44921314418025]');
INSERT INTO `field_map_center` (`pages_id`, `data`) VALUES('1032', '[63.207720840077165,75.44921314418025]');

DROP TABLE IF EXISTS `field_map_desc`;
CREATE TABLE `field_map_desc` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_map_desc` (`pages_id`, `data`) VALUES('1', 'г.Ноябрьск, Изыскателей д. 51');
INSERT INTO `field_map_desc` (`pages_id`, `data`) VALUES('1032', 'г.Ноябрьск, Изыскателей д. 51');

DROP TABLE IF EXISTS `field_map_drag`;
CREATE TABLE `field_map_drag` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_map_drag` (`pages_id`, `data`) VALUES('1', '1');
INSERT INTO `field_map_drag` (`pages_id`, `data`) VALUES('1032', '1');

DROP TABLE IF EXISTS `field_map_end`;
CREATE TABLE `field_map_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_map_point`;
CREATE TABLE `field_map_point` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_map_point` (`pages_id`, `data`) VALUES('1', '[63.207720840077165,75.44921314418025]');
INSERT INTO `field_map_point` (`pages_id`, `data`) VALUES('1032', '[63.207720840077165,75.44921314418025]');

DROP TABLE IF EXISTS `field_map_scroll`;
CREATE TABLE `field_map_scroll` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_map_zoom`;
CREATE TABLE `field_map_zoom` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_map_zoom` (`pages_id`, `data`) VALUES('1', '15');
INSERT INTO `field_map_zoom` (`pages_id`, `data`) VALUES('1032', '15');

DROP TABLE IF EXISTS `field_menu`;
CREATE TABLE `field_menu` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_menu_end`;
CREATE TABLE `field_menu_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_num`;
CREATE TABLE `field_num` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_num` (`pages_id`, `data`) VALUES('1117', '10');

DROP TABLE IF EXISTS `field_on_off`;
CREATE TABLE `field_on_off` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_page_preview`;
CREATE TABLE `field_page_preview` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_page_preview_end`;
CREATE TABLE `field_page_preview_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_page_preview_page`;
CREATE TABLE `field_page_preview_page` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_page_preview_page` (`pages_id`, `data`, `sort`) VALUES('1', '1031', '0');

DROP TABLE IF EXISTS `field_page_preview_section_classes`;
CREATE TABLE `field_page_preview_section_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_page_preview_section_classes` (`pages_id`, `data`) VALUES('1', 'stylish-color text-white pb-4 text-center');

DROP TABLE IF EXISTS `field_page_preview_section_image`;
CREATE TABLE `field_page_preview_section_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_page_preview_section_parallax`;
CREATE TABLE `field_page_preview_section_parallax` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_page_preview_title_classes`;
CREATE TABLE `field_page_preview_title_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_page_preview_title_classes` (`pages_id`, `data`) VALUES('1', 'text-uppercase text-white pt-5 pb-3');

DROP TABLE IF EXISTS `field_pages_field`;
CREATE TABLE `field_pages_field` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_pages_field` (`pages_id`, `data`, `sort`) VALUES('1', '1029', '2');
INSERT INTO `field_pages_field` (`pages_id`, `data`, `sort`) VALUES('1', '1032', '3');
INSERT INTO `field_pages_field` (`pages_id`, `data`, `sort`) VALUES('1', '1116', '1');
INSERT INTO `field_pages_field` (`pages_id`, `data`, `sort`) VALUES('1', '1117', '0');

DROP TABLE IF EXISTS `field_pass`;
CREATE TABLE `field_pass` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` char(40) NOT NULL,
  `salt` char(32) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=ascii;

INSERT INTO `field_pass` (`pages_id`, `data`, `salt`) VALUES('41', '4w0lnfRt6PNmUrAhysYASWOXR.Rl9WS', '$2y$11$agVSlIIBfZR5tPNpn2Zuf.');
INSERT INTO `field_pass` (`pages_id`, `data`, `salt`) VALUES('40', '', '');

DROP TABLE IF EXISTS `field_permissions`;
CREATE TABLE `field_permissions` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '32', '1');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '34', '2');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '35', '3');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('37', '36', '0');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '36', '0');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '50', '4');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '51', '5');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '52', '7');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '53', '8');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '54', '6');

DROP TABLE IF EXISTS `field_process`;
CREATE TABLE `field_process` (
  `pages_id` int(11) NOT NULL DEFAULT '0',
  `data` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_process` (`pages_id`, `data`) VALUES('6', '17');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('3', '12');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('8', '12');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('9', '14');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('10', '7');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('11', '47');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('16', '48');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('300', '104');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('21', '50');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('29', '66');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('23', '10');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('304', '138');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('31', '136');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('22', '76');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('30', '68');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('303', '129');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('2', '87');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('302', '121');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('301', '109');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('28', '76');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1007', '150');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1010', '159');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1012', '161');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1017', '167');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1019', '168');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1020', '173');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1023', '180');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1106', '182');

DROP TABLE IF EXISTS `field_pub_card_classes`;
CREATE TABLE `field_pub_card_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_pub_card_classes` (`pages_id`, `data`) VALUES('1', 'col-md-6 col-lg-4 mb-4');

DROP TABLE IF EXISTS `field_pub_cards`;
CREATE TABLE `field_pub_cards` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_pub_cards_end`;
CREATE TABLE `field_pub_cards_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_pub_cards_max`;
CREATE TABLE `field_pub_cards_max` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_pub_cards_max` (`pages_id`, `data`) VALUES('1', '3');

DROP TABLE IF EXISTS `field_pub_cards_parent`;
CREATE TABLE `field_pub_cards_parent` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_pub_section_classes`;
CREATE TABLE `field_pub_section_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_pub_section_classes` (`pages_id`, `data`) VALUES('1', 'view dark-section news-cards');

DROP TABLE IF EXISTS `field_pub_section_image`;
CREATE TABLE `field_pub_section_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_pub_section_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1', 'planet.jpg', '0', '[null]', '2018-12-04 23:38:25', '2018-12-04 23:38:25', '');

DROP TABLE IF EXISTS `field_pub_section_paralax`;
CREATE TABLE `field_pub_section_paralax` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_pub_section_paralax` (`pages_id`, `data`) VALUES('1', '1');

DROP TABLE IF EXISTS `field_pub_title`;
CREATE TABLE `field_pub_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_pub_title` (`pages_id`, `data`) VALUES('1', 'Новостной блог');

DROP TABLE IF EXISTS `field_pub_title_classes`;
CREATE TABLE `field_pub_title_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_pub_title_classes` (`pages_id`, `data`) VALUES('1', 'text-white text-uppercase');

DROP TABLE IF EXISTS `field_roles`;
CREATE TABLE `field_roles` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('40', '37', '0');
INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('41', '37', '0');
INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('41', '38', '2');

DROP TABLE IF EXISTS `field_script`;
CREATE TABLE `field_script` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_section`;
CREATE TABLE `field_section` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_section_classes`;
CREATE TABLE `field_section_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1', 'pub-cards grey lighten-3 pt-5 pb-1');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1031', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1117', 'stylish-color pt-2');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1116', 'stylish-color pt-2');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1029', 'stylish-color');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1032', 'stylish-color text-white pt-2');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1136', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1137', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1147', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('27', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1125', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1119', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1122', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1121', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1118', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1120', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1126', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1127', 'stylish-color text-white');
INSERT INTO `field_section_classes` (`pages_id`, `data`) VALUES('1128', 'stylish-color text-white');

DROP TABLE IF EXISTS `field_section_end`;
CREATE TABLE `field_section_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_section_image`;
CREATE TABLE `field_section_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_section_parallax`;
CREATE TABLE `field_section_parallax` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_section_title_classes`;
CREATE TABLE `field_section_title_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_section_title_classes` (`pages_id`, `data`) VALUES('1', 'text-center text-uppercase');
INSERT INTO `field_section_title_classes` (`pages_id`, `data`) VALUES('1117', 'text-white');
INSERT INTO `field_section_title_classes` (`pages_id`, `data`) VALUES('1116', 'text-white pb-5');
INSERT INTO `field_section_title_classes` (`pages_id`, `data`) VALUES('1029', 'text-white');
INSERT INTO `field_section_title_classes` (`pages_id`, `data`) VALUES('1032', 'pb-5');

DROP TABLE IF EXISTS `field_sections`;
CREATE TABLE `field_sections` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_sections_end`;
CREATE TABLE `field_sections_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_sections_list`;
CREATE TABLE `field_sections_list` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_sections_list` (`pages_id`, `data`, `sort`) VALUES('1', '1133', '0');

DROP TABLE IF EXISTS `field_sections_list_after`;
CREATE TABLE `field_sections_list_after` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_sections_list_after` (`pages_id`, `data`, `sort`) VALUES('1', '1053', '2');
INSERT INTO `field_sections_list_after` (`pages_id`, `data`, `sort`) VALUES('1', '1054', '3');
INSERT INTO `field_sections_list_after` (`pages_id`, `data`, `sort`) VALUES('1', '1062', '0');
INSERT INTO `field_sections_list_after` (`pages_id`, `data`, `sort`) VALUES('1', '1072', '1');
INSERT INTO `field_sections_list_after` (`pages_id`, `data`, `sort`) VALUES('1', '1146', '4');

DROP TABLE IF EXISTS `field_seo`;
CREATE TABLE `field_seo` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_seo_desc`;
CREATE TABLE `field_seo_desc` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_seo_desc` (`pages_id`, `data`) VALUES('1117', 'Весь спектр услуг грузчиков: перевозки мебели и грузов, переезды квартирные и офисные, вынос и вывоз строительного мусора, такелажные работы и многое другое. Низкие цены. Порядочные специалисты');

DROP TABLE IF EXISTS `field_seo_end`;
CREATE TABLE `field_seo_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_seo_keywords`;
CREATE TABLE `field_seo_keywords` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_seo_keywords` (`pages_id`, `data`) VALUES('1117', 'услуги, грузчики, москва, московская область');

DROP TABLE IF EXISTS `field_seo_title`;
CREATE TABLE `field_seo_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_seo_title` (`pages_id`, `data`) VALUES('1117', 'Услуги грузчиков в Москве и Московской области');
INSERT INTO `field_seo_title` (`pages_id`, `data`) VALUES('1', 'Мухомор | Производство рекламы');

DROP TABLE IF EXISTS `field_share_fields`;
CREATE TABLE `field_share_fields` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_share_fields_end`;
CREATE TABLE `field_share_fields_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_short_description`;
CREATE TABLE `field_short_description` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1118', '<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1031', '<p>Рекламно-производственная компания \"Мухомор\" осуществляет полный комплекс услуг в области изготовления наружной и внутренней рекламы. Мы группа креативно мыслящих людей, авантюристов в душе и свободных в мыслях. Нешаблонный подход к поставленным задачам и нестандартные решения. Нашими клиентами становятся компании сдущие в ногу со временем и осознающие реальность работы над собственным брендом. У нас большой опыт работы, доступные цены, собственное производство.</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1125', '<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1119', '<p>Современный дизайн обоев для гостиной отличается разнообразием, для любого стиля интерьера можно найти интересные варианты, подчеркивающие вкус и индивидуальность хозяина квартиры</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1126', '<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1120', '<p>Наружная реклама — это носители рекламы, которые располагаются вне помещений, воздействуя на широкую аудиторию. Это эффективное средство, направленное в основном на продвижение потребительских товаров и услуг, брэндов и рассчитанное на восприятие широкими слоями населения.</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1121', '<p>Фартук для кухни может быть изготовлен любых размеров, поэтому он подходит для отделки и небольших и просторных помещений. Такие панели великолепно вписываются в интерьер и становятся логическим завершением общей концепции</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1122', '<p>Одним из наших профессиональных направлений является изготовление картин в интерьер. Это могут быть репродукции великих художников, фотографии родных или просто постер на Ваш выбор</p>\n\n<p> </p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1127', '<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1128', '<p>Чтобы реклама – двигатель торговли – работала на все 200%, к ее изготовлению необходимо подходить с умом, фантазией и при правильном выборе исполнителя. Рекламные стенды – действенный способ распространения информации о компании, товаре, продукции или услуге и реальная возможность привлечь внимание будущего клиента.</p>\n\n<p>Стенд – это многофункциональная конструкция, которая способна выполнять сразу несколько задач, как рекламного, так и информационного характера. И ключевой момент здесь – выбор правильного типа и модели стенда. Чтобы клиент захотел купить товар или обратиться за услугой, рекламный стенд должен предлагать товар или услугу правильно</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1134', '<p> </p>\n\n<p>Выражаем благодарность за хорошую и качественную работу, профессионализм и терпение. Много времени было потрачено на выбор специалиста, Артема мы нашли случайно и не пожалели о выборе. Наш сайт получился просто превосходным! Артема отличает не только профессионализм, но и персональный подход к делу. Сразу уловил все наши пожелания, учел все, что хотелось. Хочется отметить работу уже после завершения: всегда можно обратиться за помощью, в которой никто не отказывает, а наоборот, с энтузиазмом помогает, предлагает новые решения, при появлении каких-то вопросов. Только положительные эмоции.</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1135', '<p>Выражаем благодарность за хорошую и качественную работу, профессионализм и терпение. Много времени было потрачено на выбор специалиста, Артема мы нашли случайно и не пожалели о выборе. Наш сайт получился просто превосходным! Артема отличает не только профессионализм, но и персональный подход к делу. Сразу уловил все наши пожелания, учел все, что хотелось. Хочется отметить работу уже после завершения: всегда можно обратиться за помощью, в которой никто не отказывает, а наоборот, с энтузиазмом помогает, предлагает новые решения, при появлении каких-то вопросов. Только положительные эмоции.</p>\n\n<p>Выражаем благодарность за хорошую и качественную работу, профессионализм и терпение. Много времени было потрачено на выбор специалиста, Артема мы нашли случайно и не пожалели о выборе. Наш сайт получился просто превосходным! Артема отличает не только профессионализм, но и персональный подход к делу. Сразу уловил все наши пожелания, учел все, что хотелось. Хочется отметить работу уже после завершения: всегда можно обратиться за помощью, в которой никто не отказывает, а наоборот, с энтузиазмом помогает, предлагает новые решения, при появлении каких-то вопросов. Только положительные эмоции.</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1136', '<p>Выражаем благодарность за хорошую и качественную работу, профессионализм и терпение. Много времени было потрачено на выбор специалиста, Артема мы нашли случайно и не пожалели о выборе. Наш сайт получился просто превосходным! Артема отличает не только профессионализм, но и персональный подход к делу. Сразу уловил все наши пожелания, учел все, что хотелось. Хочется отметить работу уже после завершения: всегда можно обратиться за помощью, в которой никто не отказывает, а наоборот, с энтузиазмом помогает, предлагает новые решения, при появлении каких-то вопросов. Только положительные эмоции.</p>');
INSERT INTO `field_short_description` (`pages_id`, `data`) VALUES('1137', '<p>Выражаем благодарность за хорошую и качественную работу, профессионализм и терпение. Много времени было потрачено на выбор специалиста, Артема мы нашли случайно и не пожалели о выборе. Наш сайт получился просто превосходным! Артема отличает не только профессионализм, но и персональный подход к делу. Сразу уловил все наши пожелания, учел все, что хотелось. Хочется отметить работу уже после завершения: всегда можно обратиться за помощью, в которой никто не отказывает, а наоборот, с энтузиазмом помогает, предлагает новые решения, при появлении каких-то вопросов. Только положительные эмоции.</p>');

DROP TABLE IF EXISTS `field_sitemap_ignore`;
CREATE TABLE `field_sitemap_ignore` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_socialls_list`;
CREATE TABLE `field_socialls_list` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_socialls_list` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1025', '', '0', '1036');

DROP TABLE IF EXISTS `field_static_cards`;
CREATE TABLE `field_static_cards` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_static_cards_end`;
CREATE TABLE `field_static_cards_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_static_cards_repeator`;
CREATE TABLE `field_static_cards_repeator` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_static_cards_repeator` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1', '1076,1077,1078', '3', '1075');

DROP TABLE IF EXISTS `field_static_cards_section_classes`;
CREATE TABLE `field_static_cards_section_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_static_cards_section_image`;
CREATE TABLE `field_static_cards_section_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_static_cards_section_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1', 'tver2.jpg', '0', '[null]', '2018-12-05 23:54:29', '2018-12-05 23:54:29', '');

DROP TABLE IF EXISTS `field_static_cards_section_parallax`;
CREATE TABLE `field_static_cards_section_parallax` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_static_cards_section_parallax` (`pages_id`, `data`) VALUES('1', '1');

DROP TABLE IF EXISTS `field_static_cards_subtitle`;
CREATE TABLE `field_static_cards_subtitle` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_static_cards_subtitle` (`pages_id`, `data`) VALUES('1076', 'Маркетинг');
INSERT INTO `field_static_cards_subtitle` (`pages_id`, `data`) VALUES('1077', 'Что-то');
INSERT INTO `field_static_cards_subtitle` (`pages_id`, `data`) VALUES('1078', 'Ещё что-то');

DROP TABLE IF EXISTS `field_static_cards_title`;
CREATE TABLE `field_static_cards_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_static_cards_title` (`pages_id`, `data`) VALUES('1', 'Услуги');

DROP TABLE IF EXISTS `field_static_cards_title_classes`;
CREATE TABLE `field_static_cards_title_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_static_cards_title_classes` (`pages_id`, `data`) VALUES('1', 'text-uppercase text-white');

DROP TABLE IF EXISTS `field_string`;
CREATE TABLE `field_string` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_string` (`pages_id`, `data`) VALUES('1031', 'О компании');
INSERT INTO `field_string` (`pages_id`, `data`) VALUES('1076', 'far fa-book-open');
INSERT INTO `field_string` (`pages_id`, `data`) VALUES('1077', 'far fa-book-open');
INSERT INTO `field_string` (`pages_id`, `data`) VALUES('1078', 'far fa-book-open');
INSERT INTO `field_string` (`pages_id`, `data`) VALUES('1140', 'г. Ноябрьск, Изыскателей д. 51');
INSERT INTO `field_string` (`pages_id`, `data`) VALUES('1141', 'Будни с 9.00 до 18.00');
INSERT INTO `field_string` (`pages_id`, `data`) VALUES('1142', 'Суббота с 10.00 до 15.00, Воскресенье - выходной');
INSERT INTO `field_string` (`pages_id`, `data`) VALUES('1143', '+7-919-555-54-64');
INSERT INTO `field_string` (`pages_id`, `data`) VALUES('1144', '8-3496-34-54-64');
INSERT INTO `field_string` (`pages_id`, `data`) VALUES('1145', 'muhomor.ra@mail.ru');
INSERT INTO `field_string` (`pages_id`, `data`) VALUES('1', 'Рекламно-производственная компания \"Мухомор\"');

DROP TABLE IF EXISTS `field_tabs`;
CREATE TABLE `field_tabs` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_tabs_body_classes`;
CREATE TABLE `field_tabs_body_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_tabs_end`;
CREATE TABLE `field_tabs_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_tabs_repeater`;
CREATE TABLE `field_tabs_repeater` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_tabs_repeater` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1', '1082,1083,1084', '3', '1081');

DROP TABLE IF EXISTS `field_tabs_section_classes`;
CREATE TABLE `field_tabs_section_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_tabs_section_image`;
CREATE TABLE `field_tabs_section_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_tabs_section_parallax`;
CREATE TABLE `field_tabs_section_parallax` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_tabs_title`;
CREATE TABLE `field_tabs_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_tabs_title` (`pages_id`, `data`) VALUES('1', 'Удобные способы оплаты на выбор');

DROP TABLE IF EXISTS `field_tabs_title_classes`;
CREATE TABLE `field_tabs_title_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_tabs_title_top_classes`;
CREATE TABLE `field_tabs_title_top_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_tabs_title_top_classes` (`pages_id`, `data`) VALUES('1', 'text-uppercase pb-3');

DROP TABLE IF EXISTS `field_title`;
CREATE TABLE `field_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_title` (`pages_id`, `data`) VALUES('11', 'Templates');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('16', 'Fields');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('22', 'Setup');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('3', 'Pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('6', 'Add Page');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('8', 'Tree');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('9', 'Save Sort');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('10', 'Edit');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('21', 'Modules');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('29', 'Users');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('30', 'Roles');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('2', 'Admin');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('7', 'Trash');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('27', '404 Страница не найдена');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('302', 'Insert Link');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('23', 'Login');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('304', 'Profile');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('301', 'Empty Trash');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('300', 'Search');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('303', 'Insert Image');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('28', 'Access');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('31', 'Permissions');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('32', 'Edit pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('34', 'Delete pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('35', 'Move pages (change parent)');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('36', 'View pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('50', 'Sort child pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('51', 'Change templates on pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('52', 'Administer users');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('53', 'User can update profile/password');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('54', 'Lock or unlock a page');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1', 'Главная');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1006', 'Use Page Lister');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1007', 'Find');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1010', 'Recent');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1011', 'Can see recently edited pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1012', 'Logs');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1013', 'Can view system logs');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1014', 'Can manage system logs');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1015', 'Repeaters');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1016', 'Administer languages and static translation files');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1017', 'Languages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1018', 'Default');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1019', 'Language Translator');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1020', 'DB Backups');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1021', 'Manage database backups (recommended for superuser only)');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1022', 'Русский');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1023', 'Duplicator');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1024', 'Run the Duplicator module');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1025', 'Настройки');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1026', 'col_generator');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1029', 'Отзывы');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1116', 'Портфолио');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1031', 'О нас');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1032', 'Контакты');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1035', 'socialls_list');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1036', 'settings');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1040', 'settings');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1044', 'Списки');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1045', 'Модули');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1046', 'Карусель');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1047', 'Карточки публикаций');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1079', 'Брендование автомобилей');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1049', 'Карусель товаров');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1050', 'Блог');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1051', 'Пошаговый вывод');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1052', 'Табы');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1053', 'Галерея');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1054', 'Форма');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1055', 'Карта');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1056', 'carousel_repeater');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1057', 'home');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1058', 'Арт объекты');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1059', 'Фотообои');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1060', 'Кухонные фартуки');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1062', 'Вращающиеся карточки');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1063', 'advantages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1064', 'home');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1065', 'Таблички и указатели');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1117', 'Наши услуги');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1067', 'Фотообои');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1125', 'Арт объекты');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1072', 'Краткое описание');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1073', 'Статичные карточки');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1074', 'static_cards_repeator');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1075', 'home');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1076', 'This is card title');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1077', 'Вторая карточка');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1078', 'Третья карточка');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1080', 'tabs_repeater');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1081', 'home');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1082', 'Наличные');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1083', 'Безналичный  расчет');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1084', 'Банковская карта');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1085', 'Формы');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1086', 'Контакты');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1087', 'field_options');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1088', 'Типы текстовых полей');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1089', 'text');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1090', 'password');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1091', 'date');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1092', 'datetime');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1093', 'email');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1094', 'number');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1095', 'range');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1096', 'tel');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1097', 'Имя');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1098', 'E-mail');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1104', 'Сообщение');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1105', 'Согласие с условиями');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1106', 'Upgrades');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1108', 'select');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1109', 'textarea');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1110', 'checkbox');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1111', 'Письма');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1112', 'Вывески');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1113', 'Фартуки');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1114', 'Картины');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1118', 'Таблички и указатели');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1119', 'Фотообои');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1120', 'Вывески');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1121', 'Фартуки');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1122', 'Картины');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1126', 'Широкоформатная печать');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1127', 'Полиграфия');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1128', 'Брендирование автомобилей');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1129', 'Арт объекты');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1130', 'Широкоформатная печать');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1131', 'Полиграфия');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1132', 'Брендирование автомобилей');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1133', 'Видео');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1134', 'Отзыв от Ксении');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1135', 'Отзыв номер два');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1136', 'Отзыв номер три');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1137', 'Отзыв с переходом, превью и файлами');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1138', 'contacts_fields');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1139', 'kontakty');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1147', 'Политика конфидециальности');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1146', 'Галерея2');

DROP TABLE IF EXISTS `field_video`;
CREATE TABLE `field_video` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_video_end`;
CREATE TABLE `field_video_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_video_file`;
CREATE TABLE `field_video_file` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_video_file` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1', 'header.mp4', '0', '[null]', '2019-03-05 08:01:59', '2019-03-05 08:01:59', '');

DROP TABLE IF EXISTS `field_video_logo`;
CREATE TABLE `field_video_logo` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_video_logo` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1', 'muhomor-1-e1525699952464.png', '0', '[null]', '2019-03-04 23:39:39', '2019-03-04 23:39:39', '');

DROP TABLE IF EXISTS `field_video_preview`;
CREATE TABLE `field_video_preview` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_video_section_classes`;
CREATE TABLE `field_video_section_classes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_video_section_classes` (`pages_id`, `data`) VALUES('1', 'elegant-color-dark');

DROP TABLE IF EXISTS `fieldgroups`;
CREATE TABLE `fieldgroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

INSERT INTO `fieldgroups` (`id`, `name`) VALUES('2', 'admin');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('3', 'user');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('4', 'role');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('5', 'permission');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('1', 'home');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('83', 'basic-page');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('97', 'language');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('98', 'settings');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('99', 'repeater_col_generator');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('100', 'repeater_socialls_list');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('101', 'repeater_carousel_repeater');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('102', 'repeater_flip_cards_repeater');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('103', 'repeater_static_cards_repeator');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('104', 'repeater_tabs_repeater');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('105', 'form');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('106', 'field_text');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('107', 'field_textarea');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('108', 'field_select');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('109', 'field_checkbox');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('110', 'field_radio');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('111', 'repeater_field_options');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('112', 'mail');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('113', 'contacts');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('114', 'repeater_contacts_fields');

DROP TABLE IF EXISTS `fieldgroups_fields`;
CREATE TABLE `fieldgroups_fields` (
  `fieldgroups_id` int(10) unsigned NOT NULL DEFAULT '0',
  `fields_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sort` int(11) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`fieldgroups_id`,`fields_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('2', '2', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '4', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('4', '5', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('5', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '97', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '105', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('97', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '92', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '137', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '102', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '134', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '128', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('2', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '140', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '131', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '141', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('97', '98', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '109', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '130', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '100', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '102', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '127', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('97', '99', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '102', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '136', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '105', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '109', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '109', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '135', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '102', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '138', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '132', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '226', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '237', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '237', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '230', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '239', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '230', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('111', '230', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '240', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '239', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '232', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '239', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '233', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '255', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '232', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '238', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '237', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '229', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '242', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '233', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '233', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '232', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '227', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '105', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '237', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '229', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '229', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('111', '234', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '3', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '228', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '122', '131', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '125', '132', '{\"label1022\":\"\\u041e\\u0431\\u0449\\u0438\\u0435 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '128', '133', '{\"label\":\"Site name\",\"label1022\":\"\\u0418\\u043c\\u044f \\u0441\\u0430\\u0439\\u0442\\u0430\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '110', '134', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '111', '135', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '112', '136', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '113', '137', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '126', '138', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '230', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '236', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '235', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '239', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '244', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '139', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '255', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '255', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '230', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '253', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '240', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '241', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '123', '130', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '121', '129', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '124', '128', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '120', '127', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '115', '126', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '234', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '230', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '233', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '255', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '240', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '254', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '255', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '118', '125', '{\"description1022\":\"\\u0414\\u043e\\u0431\\u0430\\u0432\\u044c\\u0442\\u0435 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0435 \\u043d\\u0430\\u0434\\u043e \\u043e\\u0442\\u043e\\u0431\\u0440\\u0430\\u0437\\u0438\\u0442\\u044c \\u0432 \\u043c\\u0435\\u043d\\u044e\",\"label1022\":\"\\u041c\\u0435\\u043d\\u044e\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '114', '124', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '108', '123', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '129', '122', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '107', '121', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '150', '120', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '257', '119', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '263', '118', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '262', '117', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '260', '116', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '261', '115', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '259', '114', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '239', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '231', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '232', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '258', '113', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '256', '112', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '250', '111', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '248', '110', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '105', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '128', '3', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0438\\u043a\\u043e\\u043d\\u043a\\u0438\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '185', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '169', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '251', '109', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '252', '108', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '246', '107', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '247', '106', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '245', '105', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '249', '104', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '277', '103', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '289', '102', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '288', '101', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('111', '117', '2', '{\"label1022\":\"\\u041f\\u043e \\u0443\\u043c\\u043e\\u043b\\u0447\\u0430\\u043d\\u0438\\u044e\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '233', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '240', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '243', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '128', '1', '{\"label1022\":\"\\u0410\\u043b\\u044c\\u0442\\u0435\\u0440\\u043d\\u0430\\u0442\\u0438\\u0432\\u043d\\u044b\\u0439 \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0434\\u043b\\u044f \\u043c\\u0435\\u043d\\u044e\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '103', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '186', '3', '{\"description1022\":\"\",\"label1022\":\"\\u0410\\u0432\\u0442\\u043e\\u0440\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '127', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '102', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '105', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '273', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '274', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '106', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '109', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '119', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '117', '12', '{\"label1022\":\"\\u041f\\u043e\\u043a\\u0430\\u0437\\u044b\\u0432\\u0430\\u0442\\u044c \\u0434\\u043e\\u0447\\u0435\\u0440\\u043d\\u0438\\u0435 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b (\\u0435\\u0441\\u043b\\u0438 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u0430 \\u043a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0438\\u044f)\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '238', '13', '{\"label1022\":\"\\u0425\\u043b\\u0435\\u0431\\u043d\\u044b\\u0435 \\u043a\\u0440\\u043e\\u0448\\u043a\\u0438 \\u0434\\u043b\\u044f \\u0434\\u043e\\u0447\\u0435\\u0440\\u043d\\u0438\\u0445 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446 (\\u0435\\u0441\\u043b\\u0438 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u0430 \\u043a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0438\\u044f)\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '116', '14', '{\"label1022\":\"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0441\\u0435\\u0441\\u0442\\u0432\\u043e \\u0432\\u044b\\u043f\\u0430\\u0434\\u0430\\u044e\\u0449\\u0438\\u0445 \\u043f\\u0443\\u043d\\u043a\\u0442\\u043e\\u0432 \\u0432 \\u043c\\u0435\\u043d\\u044e \\u0434\\u043b\\u044f \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b \\u043a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0438\\u0438\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '194', '15', '{\"label1022\":\"\\u041c\\u0430\\u043a\\u0441\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439 (\\u0434\\u043b\\u044f \\u043a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0438\\u0439)\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '120', '16', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '124', '17', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '121', '18', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '123', '19', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '122', '20', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '220', '21', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '222', '22', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '223', '23', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '224', '24', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '225', '25', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '221', '26', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '233', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '282', '100', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '279', '99', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '283', '98', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '281', '97', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '285', '96', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '284', '95', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '280', '94', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '278', '93', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '287', '92', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '286', '91', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '276', '90', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '205', '89', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '212', '88', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '266', '87', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '265', '86', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '210', '85', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '211', '84', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '209', '83', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '208', '82', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '207', '81', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '218', '80', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '206', '79', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '204', '78', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '196', '77', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '203', '76', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '202', '75', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '201', '74', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '199', '73', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '198', '72', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '197', '71', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '217', '70', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '200', '69', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '195', '68', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '189', '67', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '193', '66', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '194', '65', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '191', '64', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '190', '63', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '187', '62', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '238', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '216', '61', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '192', '60', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '188', '59', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '179', '58', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '182', '57', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '180', '56', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '184', '55', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '181', '54', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '215', '53', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '183', '52', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '178', '51', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '173', '50', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '177', '49', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '175', '48', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '264', '47', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '176', '46', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '174', '45', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '172', '44', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '163', '43', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '170', '42', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '169', '41', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '168', '40', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '166', '39', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '165', '38', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '167', '37', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '214', '36', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '164', '35', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '162', '34', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '157', '33', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '155', '32', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '171', '31', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '160', '30', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '158', '29', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '123', '17', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '259', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '261', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '263', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '258', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '128', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '102', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '275', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '159', '28', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '213', '27', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '161', '26', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('114', '109', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('114', '186', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '121', '16', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '262', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '256', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '119', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '105', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '156', '25', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '269', '24', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '272', '23', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '271', '22', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '270', '21', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '267', '20', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '268', '19', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '148', '18', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '145', '17', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '144', '16', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '154', '15', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '143', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '142', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '147', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '219', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '151', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '149', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '221', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '224', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '223', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '124', '15', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '120', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '257', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '260', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '152', '1', '{\"label\":\"H1 home title\",\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a H1 \\u0434\\u043b\\u044f \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '102', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '220', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '225', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '222', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('114', '237', '0', '{\"label1022\":\"\\u041d\\u0430\\u0438\\u043c\\u0435\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u0430\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('114', '128', '1', '{\"label1022\":\"\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u043d\\u044b\\u0435 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0435\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '102', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '109', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '133', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '122', '18', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '220', '19', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '222', '20', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '223', '21', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '224', '22', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '225', '23', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '221', '24', NULL);

DROP TABLE IF EXISTS `fields`;
CREATE TABLE `fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(128) CHARACTER SET ascii NOT NULL,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `label` varchar(250) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=290 DEFAULT CHARSET=utf8;

INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('1', 'FieldtypePageTitle', 'title', '13', 'Title', '{\"required\":1,\"textformatters\":[\"TextformatterEntities\"],\"size\":0,\"maxlength\":255,\"collapsed\":0,\"minlength\":0,\"showCount\":0,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"header\",\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('2', 'FieldtypeModule', 'process', '25', 'Process', '{\"description\":\"The process that is executed on this page. Since this is mostly used by ProcessWire internally, it is recommended that you don\'t change the value of this unless adding your own pages in the admin.\",\"collapsed\":1,\"required\":1,\"moduleTypes\":[\"Process\"],\"permanent\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('3', 'FieldtypePassword', 'pass', '24', 'Set Password', '{\"collapsed\":1,\"size\":50,\"maxlength\":128}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('5', 'FieldtypePage', 'permissions', '24', 'Permissions', '{\"derefAsPage\":0,\"parent_id\":31,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldCheckboxes\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('4', 'FieldtypePage', 'roles', '24', 'Roles', '{\"derefAsPage\":0,\"parent_id\":30,\"labelFieldName\":\"name\",\"inputfield\":\"InputfieldCheckboxes\",\"description\":\"User will inherit the permissions assigned to each role. You may assign multiple roles to a user. When accessing a page, the user will only inherit permissions from the roles that are also assigned to the page\'s template.\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('92', 'FieldtypeEmail', 'email', '9', 'E-Mail Address', '{\"size\":70,\"maxlength\":255}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('97', 'FieldtypeModule', 'admin_theme', '8', 'Admin Theme', '{\"moduleTypes\":[\"AdminTheme\"],\"labelField\":\"title\",\"inputfieldClass\":\"InputfieldRadios\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('98', 'FieldtypeFile', 'language_files_site', '24', 'Site Translation Files', '{\"extensions\":\"json csv\",\"maxFiles\":0,\"inputfieldClass\":\"InputfieldFile\",\"unzip\":1,\"description\":\"Use this field for translations specific to your site (like files in \\/site\\/templates\\/ for example).\",\"descriptionRows\":0,\"fileSchema\":6}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('99', 'FieldtypeFile', 'language_files', '24', 'Core Translation Files', '{\"extensions\":\"json csv\",\"maxFiles\":0,\"inputfieldClass\":\"InputfieldFile\",\"unzip\":1,\"description\":\"Use this field for [language packs](http:\\/\\/modules.processwire.com\\/categories\\/language-pack\\/). To delete all files, double-click the trash can for any file, then save.\",\"descriptionRows\":0,\"fileSchema\":6}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('100', 'FieldtypePage', 'language', '24', 'Language', '{\"derefAsPage\":1,\"parent_id\":1017,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldRadios\",\"required\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('101', 'FieldtypeCheckbox', 'sitemap_ignore', '0', 'Hide page from XML sitemap', '{\"description\":\"Hide this page and its children from the XML sitemap\",\"collapsed\":0,\"tags\":\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('102', 'FieldtypeTextarea', 'body', '0', 'Content', '{\"label1019\":\"\\u0422\\u0435\\u043a\\u0441\\u0442 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"textformatters\":[\"TextformatterEntities\"],\"inputfieldClass\":\"InputfieldCKEditor\",\"showCount\":1,\"rows\":5,\"toolbar\":\"Format, Styles, -, Bold, Italic, -, RemoveFormat\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"pencil\",\"label1024\":\"\\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u043e\\u0435 \\u0441\\u043e\\u0434\\u0435\\u0440\\u0436\\u0438\\u043c\\u043e\\u0435 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"label1022\":\"\\u0421\\u043e\\u0434\\u0435\\u0440\\u0436\\u0438\\u043c\\u043e\\u0435 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"contentType\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"inlineMode\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('103', 'FieldtypeDatetime', 'date', '0', 'Date', '{\"label1019\":\"\\u0414\\u0430\\u0442\\u0430\",\"dateOutputFormat\":\"d.m.Y\",\"size\":25,\"datepicker\":3,\"dateInputFormat\":\"d.m.Y\",\"defaultToday\":1,\"tags\":\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435\",\"icon\":\"calendar\",\"label1024\":\"\\u0414\\u0430\\u0442\\u0430\",\"label1022\":\"\\u0414\\u0430\\u0442\\u0430\",\"collapsed\":0,\"timeInputSelect\":0,\"columnWidth\":32}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('104', 'FieldtypeFile', 'files', '0', 'Files', '{\"label1019\":\"\\u0424\\u0430\\u0439\\u043b\\u044b\",\"extensions\":\"pdf doc docx xls xlsx gif jpg jpeg png mp4\",\"inputfieldClass\":\"InputfieldFile\",\"descriptionRows\":1,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"file-word-o\",\"fileSchema\":6,\"label1024\":\"\\u0424\\u0430\\u0439\\u043b\\u044b\",\"label1022\":\"\\u0424\\u0430\\u0439\\u043b\\u044b\",\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('105', 'FieldtypeImage', 'image', '0', 'Image', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"picture-o\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1200,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"description1022\":\"\\u0414\\u043e 1200\\u0440\\u0445 \\u043f\\u043e \\u0448\\u0438\\u0440\\u0438\\u043d\\u0435\",\"defaultValuePage\":0,\"useTags\":0,\"collapsed\":0,\"resizeServer\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('106', 'FieldtypeImage', 'images', '0', 'Images', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f\",\"extensions\":\"gif jpg jpeg png\",\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"camera\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f\",\"description1024\":\"\\u041f\\u0435\\u0440\\u0435\\u0442\\u0430\\u0441\\u043a\\u0438\\u0432\\u0430\\u044f \\u0438\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f \\u0432\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u043c\\u0435\\u043d\\u044f\\u0442\\u044c \\u0438\\u0445 \\u043f\\u043e\\u0441\\u043b\\u0435\\u0434\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0441\\u0442\\u044c \\u043d\\u0430 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u0435. \\u0414\\u043e\\u0431\\u0430\\u0432\\u0438\\u0432 \\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0432\\u044b \\u0443\\u0432\\u0438\\u0434\\u0438\\u0442\\u0435 \\u0435\\u0433\\u043e \\u043d\\u0430 \\u043f\\u043e\\u043b\\u043d\\u043e\\u043c\\u0430\\u0441\\u0448\\u0442\\u0430\\u0431\\u043d\\u043e\\u043c \\u0438\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0438 \\u043f\\u0440\\u0438 \\u043a\\u043b\\u0438\\u043a\\u0435 \\u043d\\u0430 \\u043c\\u0438\\u043d\\u0438\\u0430\\u0442\\u044e\\u0440\\u0435.\",\"maxWidth\":3000,\"focusMode\":\"on\",\"clientQuality\":100,\"maxFiles\":0,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f\",\"description1022\":\"\\u041d\\u0435\\u0441\\u043a\\u043e\\u043b\\u044c\\u043a\\u043e \\u0438\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0439. \\u041c\\u043e\\u0436\\u043d\\u043e \\u043f\\u0435\\u0440\\u0435\\u0442\\u0430\\u0441\\u043a\\u0438\\u0432\\u0430\\u044f \\u0438\\u043a\\u043e\\u043d\\u043a\\u0438 \\u043c\\u0435\\u043d\\u044f\\u0442\\u044c \\u0438\\u0445 \\u043c\\u0435\\u0441\\u0442\\u0430\\u043c\\u0438\",\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"collapsed\":0,\"resizeServer\":0,\"maxHeight\":3000}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('107', 'FieldtypeFieldsetTabOpen', 'languige', '0', 'Language constants', '{\"label1024\":\"\\u041a\\u043e\\u043d\\u0441\\u0442\\u0430\\u043d\\u0442\\u044b\",\"tags\":\"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f\",\"icon\":\"trademark\",\"closeFieldID\":138,\"label1019\":\"\\u041a\\u043e\\u043d\\u0441\\u0442\\u0430\\u043d\\u0442\\u044b\",\"label1022\":\"\\u041a\\u043e\\u043d\\u0441\\u0442\\u0430\\u043d\\u0442\\u044b\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('108', 'FieldtypeFieldsetClose', 'languige_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'languige\'. It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"tags\":\"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f\",\"icon\":\"trademark\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('109', 'FieldtypeURL', 'links', '0', 'Link', '{\"label1019\":\"\\u0410\\u0434\\u0440\\u0435\\u0441 \\u0441\\u0441\\u044b\\u043b\\u043a\\u0438\",\"textformatters\":[\"TextformatterEntities\"],\"addRoot\":1,\"maxlength\":1024,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"link\",\"label1022\":\"\\u0421\\u0441\\u044b\\u043b\\u043a\\u0430\",\"noRelative\":0,\"allowIDN\":0,\"allowQuotes\":0,\"collapsed\":0,\"minlength\":0,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('110', 'FieldtypeText', 'main_copy', '0', 'Copy', '{\"label1019\":\"\\u041a\\u043e\\u043f\\u0438\\u0440\\u0430\\u0439\\u0442\",\"textformatters\":[\"TextformatterEntities\"],\"maxlength\":2048,\"tags\":\"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f\",\"icon\":\"copyright\",\"label1024\":\"\\u041a\\u043e\\u043f\\u0438\\u0440\\u0430\\u0439\\u0442\",\"description1024\":\"\\u0412\\u044b\\u0432\\u043e\\u0434 \\u043f\\u043e\\u043b\\u044f \\u043a\\u043e\\u043f\\u0438\\u0440\\u0430\\u0439\\u0442\\u0430 \\u0432 \\u043f\\u043e\\u0434\\u0432\\u0430\\u043b\\u0435 \\u0441\\u0430\\u0439\\u0442\\u0430\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('111', 'FieldtypeText', 'main_desc', '0', 'Site short description', '{\"label1019\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435 \\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0441\\u0430\\u0439\\u0442\\u0430\",\"textformatters\":[\"TextformatterEntities\"],\"maxlength\":2048,\"tags\":\"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f\",\"icon\":\"pencil\",\"label1022\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435 \\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"collapsed\":0,\"minlength\":0,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('112', 'FieldtypeEmail', 'main_email', '0', 'Email', '{\"label1019\":\"\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0447\\u0442\\u0430\",\"textformatters\":[\"TextformatterEntities\"],\"maxlength\":512,\"tags\":\"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f\",\"icon\":\"envelope-o\",\"label1024\":\"\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0447\\u0442\\u0430\",\"description1024\":\"\\u041e\\u0442\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u043e\\u0439 \\u043f\\u043e\\u0447\\u0442\\u044b \\u0432 \\u043f\\u043e\\u0434\\u0432\\u0430\\u043b\\u0435 \\u0441\\u0430\\u0439\\u0442\\u0430\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('113', 'FieldtypeText', 'main_phone', '0', 'Phone', '{\"label1019\":\"\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\",\"textformatters\":[\"TextformatterEntities\"],\"maxlength\":2048,\"tags\":\"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f\",\"icon\":\"phone\",\"label1024\":\"\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\",\"description1024\":\"\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d \\u0432 \\u0448\\u0430\\u043f\\u043a\\u0435 \\u0438 \\u043f\\u043e\\u0434\\u0432\\u0430\\u043b\\u0435 \\u0441\\u0430\\u0439\\u0442\\u0430\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('114', 'FieldtypeFieldsetTabOpen', 'menu', '0', 'Menu Tab', '{\"label1024\":\"\\u041c\\u0435\\u043d\\u044e\",\"tags\":\"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f\",\"icon\":\"bars\",\"closeFieldID\":158,\"label1019\":\"\\u041c\\u0435\\u043d\\u044e\",\"label1022\":\"\\u041c\\u0435\\u043d\\u044e\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('115', 'FieldtypeFieldsetClose', 'menu_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'menu\'. It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"tags\":\"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f\",\"icon\":\"bars\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('116', 'FieldtypeInteger', 'num', '0', 'Number', '{\"label1019\":\"\\u0427\\u0438\\u0441\\u043b\\u043e\",\"inputType\":\"text\",\"size\":10,\"max\":999,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"plus\",\"label1022\":\"\\u041d\\u043e\\u043c\\u0435\\u0440\",\"zeroNotEmpty\":0,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('117', 'FieldtypeCheckbox', 'on_off', '0', 'On/off checkbox', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435\",\"icon\":\"check\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0412\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('118', 'FieldtypePage', 'pages_field', '0', 'Pages listing', '{\"label1024\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"inputfield\":\"InputfieldPageListSelectMultiple\",\"parent_id\":1,\"labelFieldName\":\"title\",\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"files-o\",\"label1019\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"label1022\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"derefAsPage\":0,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('119', 'FieldtypeTextarea', 'script', '0', 'Script or non formating text', '{\"label1019\":\"\\u041f\\u043e\\u043b\\u0435 \\u0431\\u0435\\u0437 \\u0444\\u043e\\u0440\\u043c\\u0430\\u0442\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u044f\",\"inputfieldClass\":\"InputfieldTextarea\",\"rows\":5,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"code\",\"label1024\":\"\\u0421\\u043a\\u0440\\u0438\\u043f\\u0442\\u044b \\u0438\\u043b\\u0438 \\u043d\\u0435 \\u0444\\u043e\\u0440\\u043c\\u0430\\u0442\\u0438\\u0440\\u0443\\u0435\\u043c\\u044b\\u0439 \\u0442\\u0435\\u043a\\u0441\\u0442\",\"label1022\":\"\\u0421\\u043a\\u0440\\u0438\\u043f\\u0442\\u044b \\u0438\\u043b\\u0438 \\u043d\\u0435 \\u0444\\u043e\\u0440\\u043c\\u0430\\u0442\\u0438\\u0440\\u0443\\u0435\\u043c\\u044b\\u0439 \\u0442\\u0435\\u043a\\u0441\\u0442\",\"contentType\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('120', 'FieldtypeFieldsetTabOpen', 'seo', '0', 'SEO fields', '{\"label1019\":\"SEO \\u043f\\u043e\\u043b\\u044f\",\"tags\":\"SEO\",\"icon\":\"file-o\",\"label1024\":\"SEO \\u043f\\u043e\\u043b\\u044f\",\"closeFieldID\":146,\"label1022\":\"SEO\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('121', 'FieldtypeText', 'seo_desc', '0', 'SEO description', '{\"label1019\":\"SEO \\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"maxlength\":2048,\"tags\":\"SEO\",\"icon\":\"globe\",\"label1022\":\"SEO \\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"description1022\":\"\\u041e\\u0434\\u043d\\u043e \\u0438\\u043b\\u0438 \\u043d\\u0435\\u0441\\u043a\\u043e\\u043b\\u044c\\u043a\\u043e \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0439 \\u043e\\u043f\\u0438\\u0441\\u044b\\u0432\\u0430\\u044e\\u0449\\u0438\\u0445 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u0443.\",\"collapsed\":0,\"minlength\":0,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('122', 'FieldtypeFieldsetClose', 'seo_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'seo\'. It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"tags\":\"SEO\",\"icon\":\"file-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('123', 'FieldtypeText', 'seo_keywords', '0', 'SEO keywords', '{\"label1019\":\"SEO \\u043a\\u043b\\u044e\\u0447\\u0435\\u0432\\u0438\\u043a\\u0438\",\"maxlength\":2048,\"tags\":\"SEO\",\"icon\":\"globe\",\"label1022\":\"SEO \\u043a\\u043b\\u044e\\u0447\\u0435\\u0432\\u044b\\u0435 \\u0441\\u043b\\u043e\\u0432\\u0430\",\"description1022\":\"\\u0414\\u043e \\u0434\\u0435\\u0441\\u044f\\u0442\\u0438 \\u043a\\u043b\\u044e\\u0447\\u0435\\u0432\\u044b\\u0445 \\u0441\\u043b\\u043e\\u0432 \\u0438\\u043b\\u0438 \\u0444\\u0440\\u0430\\u0437 \\u0447\\u0435\\u0440\\u0435\\u0437 \\u0437\\u0430\\u043f\\u044f\\u0442\\u0443\\u044e\",\"collapsed\":0,\"minlength\":0,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('124', 'FieldtypeText', 'seo_title', '0', 'SEO title', '{\"label1019\":\"SEO \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a\",\"maxlength\":2048,\"tags\":\"SEO\",\"icon\":\"globe\",\"label1022\":\"SEO \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a\",\"description1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b \\u0434\\u043b\\u044f \\u043f\\u043e\\u0438\\u0441\\u043a\\u043e\\u0432\\u044b\\u0445 \\u043c\\u0430\\u0448\\u0438\\u043d\",\"collapsed\":0,\"minlength\":0,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('125', 'FieldtypeFieldsetTabOpen', 'share_fields', '0', 'Share fields', '{\"label1019\":\"\\u041e\\u0431\\u0449\\u0438\\u0435 \\u043f\\u043e\\u043b\\u044f\",\"tags\":\"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f\",\"icon\":\"file-o\",\"label1024\":\"\\u041e\\u0431\\u0449\\u0438\\u0435 \\u043f\\u043e\\u043b\\u044f\",\"closeFieldID\":152,\"label1022\":\"\\u041e\\u0431\\u0449\\u0438\\u0435 \\u043f\\u043e\\u043b\\u044f\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('126', 'FieldtypeFieldsetClose', 'share_fields_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'share_fields\'. It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"tags\":\"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f\",\"icon\":\"file-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('127', 'FieldtypeTextarea', 'short_description', '0', 'Short description', '{\"label1019\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435 \\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"textformatters\":[\"TextformatterEntities\"],\"inputfieldClass\":\"InputfieldCKEditor\",\"rows\":5,\"toolbar\":\"Format, Styles, -, Bold, Italic, -, RemoveFormat\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"pencil\",\"label1022\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435 \\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"contentType\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"inlineMode\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('128', 'FieldtypeText', 'string', '0', 'String', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"textformatters\":[\"TextformatterEntities\"],\"maxlength\":2048,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"pencil\",\"label1022\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"collapsed\":0,\"minlength\":0,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('129', 'FieldtypeYaml', 'language_ru', '0', 'Lang', '{\"label1024\":\"\\u042f\\u0437\\u044b\\u043a\\u043e\\u0432\\u044b\\u0435 \\u043a\\u043e\\u043d\\u0441\\u0442\\u0430\\u043d\\u0442\\u044b \\u0434\\u043b\\u044f \\u0448\\u0430\\u0431\\u043b\\u043e\\u043d\\u0430\",\"inputfieldClass\":\"InputfieldTextarea\",\"yamlParseAs\":2,\"rows\":30,\"tags\":\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435\",\"icon\":\"trademark\",\"label1019\":\"\\u041a\\u043e\\u043d\\u0441\\u0442\\u0430\\u043d\\u0442\\u044b\",\"stripTags\":1,\"columnWidth\":70,\"label1022\":\"\\u042f\\u0437\\u044b\\u043a\\u043e\\u0432\\u044b\\u0435 \\u043a\\u043e\\u043d\\u0441\\u0442\\u0430\\u043d\\u0442\\u044b\",\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('130', 'FieldtypeText', 'header_section_classes', '0', 'Header section classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u0448\\u0430\\u043f\\u043a\\u0438\",\"description\":\"https:\\/\\/mdbootstrap.com\\/docs\\/jquery\\/css\\/colors\\/\",\"description1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0446\\u0432\\u0435\\u0442\\u0430 \\u0444\\u043e\\u043d\\u0430 \\u0438 \\u0442\\u0435\\u043a\\u0441\\u0442\\u0430 \\u043c\\u043e\\u0436\\u043d\\u043e \\u043d\\u0430\\u0439\\u0442\\u0438 \\u043d\\u0430 \\u0434\\u0430\\u043d\\u043d\\u043e\\u043c \\u0441\\u0430\\u0439\\u0442\\u0435:\\nhttps:\\/\\/mdbootstrap.com\\/docs\\/jquery\\/css\\/colors\\/\",\"collapsed\":1,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438\",\"icon\":\"wrench\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('131', 'FieldtypeText', 'header_navbar_classes', '0', 'header navbar classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0432\\u0435\\u0440\\u0445\\u043d\\u0435\\u0433\\u043e \\u043c\\u0435\\u043d\\u044e \\u0432 \\u0448\\u0430\\u043f\\u043a\\u0435\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438\",\"icon\":\"wrench\",\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('132', 'FieldtypeImage', 'header_navbar_logo', '0', 'header navbar logo', '{\"label1022\":\"\\u041b\\u043e\\u0433\\u043e\\u0442\\u0438\\u043f \\u0432 \\u0448\\u0430\\u043f\\u043a\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"collapsed\":1,\"descriptionRows\":1,\"gridMode\":\"list\",\"focusMode\":\"on\",\"maxWidth\":600,\"maxHeight\":600,\"resizeServer\":0,\"clientQuality\":90,\"minWidth\":50,\"minHeight\":50,\"tags\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438\",\"icon\":\"wrench\",\"fileSchema\":6}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('133', 'FieldtypeFieldsetOpen', 'header', '0', 'Header', '{\"label1022\":\"\\u0428\\u0430\\u043f\\u043a\\u0430 \\u0441\\u0430\\u0439\\u0442\\u0430\",\"closeFieldID\":134,\"collapsed\":1,\"tags\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('134', 'FieldtypeFieldsetClose', 'header_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'header\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":133,\"tags\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438\",\"icon\":\"folder-o\",\"label1022\":\"\\u0428\\u0430\\u043f\\u043a\\u0430 \\u0441\\u0430\\u0439\\u0442\\u0430\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('135', 'FieldtypeText', 'footer_section_classes', '0', 'footer section classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u043f\\u043e\\u0434\\u0432\\u0430\\u043b\\u0430\",\"collapsed\":1,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438\",\"icon\":\"wrench\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('136', 'FieldtypeRepeater', 'col_generator', '0', 'Content column generator', '{\"label1022\":\"\\u0413\\u0435\\u043d\\u0435\\u0440\\u0430\\u0442\\u043e\\u0440 \\u043a\\u043e\\u043b\\u043e\\u043d\\u043e\\u043a \\u0432 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"description1022\":\"\\u041a\\u0430\\u0436\\u0434\\u044b\\u0439 \\u043d\\u043e\\u0432\\u044b\\u0439 \\u0431\\u043b\\u043e\\u043a - \\u043e\\u0442\\u0434\\u0435\\u043b\\u044c\\u043d\\u0430\\u044f \\u043a\\u043e\\u043b\\u043e\\u043d\\u043a\\u0430. \\u0441\\u043e \\u0441\\u0432\\u043e\\u0438\\u043c \\u043a\\u043b\\u0430\\u0441\\u0441\\u043e\\u043c \\u0438 \\u043a\\u043e\\u043d\\u0442\\u0435\\u043d\\u0442\\u043e\\u043c. \\u041a\\u043e\\u043b\\u043e\\u043d\\u043e\\u043a \\u0432 \\u043e\\u0431\\u0449\\u0435\\u0439 \\u0441\\u043b\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u0438 \\u043d\\u0435 \\u0434\\u043e\\u043b\\u0436\\u043d\\u043e \\u0431\\u044b\\u0442\\u044c \\u0431\\u043e\\u043b\\u0435\\u0435 12\",\"template_id\":45,\"parent_id\":1026,\"repeaterFields\":[141,140,102,105],\"repeaterTitle\":\"#n: {\\u0411\\u043b\\u043e\\u043a \\u043a\\u043e\\u043d\\u0442\\u0435\\u043d\\u0442\\u0430}\",\"repeaterAddLabel\":\"Add column\",\"repeaterAddLabel1022\":\"\\u0414\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c \\u043a\\u043e\\u043b\\u043e\\u043d\\u043a\\u0443\",\"repeaterCollapse\":1,\"repeaterLoading\":1,\"rememberOpen\":1,\"accordionMode\":1,\"collapsed\":1,\"tags\":\"\\u0413\\u0435\\u043d\\u0435\\u0440\\u0430\\u0442\\u043e\\u0440-\\u043a\\u043e\\u043b\\u043e\\u043d\\u043e\\u043a\",\"icon\":\"files-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('137', 'FieldtypeFieldsetOpen', 'footer', '0', 'Footer', '{\"label1022\":\"\\u041f\\u043e\\u0434\\u0432\\u0430\\u043b \\u0441\\u0430\\u0439\\u0442\\u0430\",\"closeFieldID\":138,\"collapsed\":1,\"tags\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('138', 'FieldtypeFieldsetClose', 'footer_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'footer\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":137,\"label1022\":\"\\u041f\\u043e\\u0434\\u0432\\u0430\\u043b \\u0441\\u0430\\u0439\\u0442\\u0430\",\"tags\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438\",\"icon\":\"folder-open-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('139', 'FieldtypeRepeater', 'socialls_list', '0', 'List sicialls icons', '{\"label1022\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u043e\\u0446\\u0441\\u0435\\u0442\\u0435\\u0439\",\"template_id\":46,\"parent_id\":1035,\"repeaterFields\":[128,109],\"repeaterTitle\":\"#n: {\\u0421\\u043e\\u0446\\u0441\\u0435\\u0442\\u044c}\",\"repeaterAddLabel\":\"Add social\",\"repeaterAddLabel1022\":\"\\u0414\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c \\u0441\\u043e\\u0446\\u0441\\u0435\\u0442\\u044c\",\"repeaterCollapse\":1,\"repeaterLoading\":1,\"accordionMode\":1,\"repeaterMaxItems\":10,\"collapsed\":1,\"tags\":\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435\",\"icon\":\"vk\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('140', 'FieldtypeText', 'col_generator_classes', '0', 'Option classes', '{\"label1022\":\"\\u0414\\u043e\\u043f. \\u043a\\u043b\\u0430\\u0441\\u0441\\u044b \\u043a\\u043e\\u043b\\u043e\\u043d\\u043a\\u0438\",\"collapsed\":1,\"columnWidth\":69,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0413\\u0435\\u043d\\u0435\\u0440\\u0430\\u0442\\u043e\\u0440-\\u043a\\u043e\\u043b\\u043e\\u043d\\u043e\\u043a\",\"icon\":\"files-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('141', 'FieldtypeText', 'col_generator_grid', '0', 'Grid class', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441 \\u0441\\u0435\\u0442\\u043a\\u0438 \\u043a\\u043e\\u043b\\u043e\\u043d\\u043a\\u0438\",\"collapsed\":1,\"columnWidth\":29,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0413\\u0435\\u043d\\u0435\\u0440\\u0430\\u0442\\u043e\\u0440-\\u043a\\u043e\\u043b\\u043e\\u043d\\u043e\\u043a\",\"icon\":\"files-o\",\"description\":\"https:\\/\\/mdbootstrap.com\\/docs\\/jquery\\/layout\\/grid-examples\\/\",\"description1022\":\"https:\\/\\/mdbootstrap.com\\/docs\\/jquery\\/layout\\/grid-examples\\/\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('142', 'FieldtypeText', 'carousel_id', '0', 'Carousel id', '{\"label1022\":\"\\u0423\\u043d\\u0438\\u043a\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 ID \\u043a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u0438\",\"collapsed\":0,\"columnWidth\":49,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u044c\",\"icon\":\"picture-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('143', 'FieldtypeText', 'carousel_classes', '0', 'Carousel classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u043a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u0438\",\"columnWidth\":49,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u044c\",\"icon\":\"picture-o\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('144', 'FieldtypeText', 'carousel_prew_icon', '0', 'Carousel prew icon classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0438\\u043a\\u043e\\u043d\\u043a\\u0438 \\u043f\\u0435\\u0440\\u0435\\u043c\\u043e\\u0442\\u043a\\u0438 \\u043d\\u0430\\u0437\\u0430\\u0434\",\"columnWidth\":49,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u044c\",\"icon\":\"picture-o\",\"description\":\"https:\\/\\/fontawesome.com\\/icons?d=gallery\",\"description1022\":\"https:\\/\\/fontawesome.com\\/icons?d=gallery\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('145', 'FieldtypeText', 'carousel_next_icon', '0', 'Carousel next icon classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0438\\u043a\\u043e\\u043d\\u043a\\u0438 \\u043f\\u0435\\u0440\\u0435\\u043c\\u043e\\u0442\\u043a\\u0438 \\u0432\\u043f\\u0435\\u0440\\u0435\\u0434\",\"columnWidth\":48,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u044c\",\"icon\":\"picture-o\",\"description\":\"https:\\/\\/fontawesome.com\\/icons?d=gallery\",\"description1022\":\"https:\\/\\/fontawesome.com\\/icons?d=gallery\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('146', 'FieldtypeImage', 'carousel_images', '0', 'Carousel images', '{\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f \\u0434\\u043b\\u044f \\u043a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u0438\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":30,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"maxWidth\":2000,\"maxHeight\":1500,\"resizeServer\":0,\"clientQuality\":90,\"tags\":\"\\u041a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u044c\",\"icon\":\"file-image-o\",\"fileSchema\":6,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('147', 'FieldtypeFieldsetOpen', 'carousel', '0', 'Carousel', '{\"label1022\":\"\\u041a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u044c\",\"closeFieldID\":148,\"collapsed\":1,\"tags\":\"\\u041a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u044c\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('148', 'FieldtypeFieldsetClose', 'carousel_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'carousel\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":147,\"tags\":\"\\u041a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u044c\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('149', 'FieldtypeFieldsetTabOpen', 'sections', '0', 'Modules', '{\"label1022\":\"\\u041c\\u043e\\u0434\\u0443\\u043b\\u0438\",\"closeFieldID\":150,\"collapsed\":0,\"tags\":\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('150', 'FieldtypeFieldsetClose', 'sections_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'sections\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":149,\"tags\":\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('151', 'FieldtypePage', 'sections_list', '0', 'Modules list', '{\"label1022\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u043c\\u043e\\u0434\\u0443\\u043b\\u0435\\u0439 \\u0434\\u043e \\u0442\\u0435\\u043b\\u0430 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"derefAsPage\":0,\"inputfield\":\"InputfieldAsmSelect\",\"parent_id\":1045,\"labelFieldName\":\"title\",\"collapsed\":0,\"tags\":\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435\",\"icon\":\"cubes\",\"usePageEdit\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('152', 'FieldtypeText', 'carousel_image_title', '0', 'carousel image title', '{\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0434\\u043b\\u044f \\u0438\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f \\u0441\\u043b\\u0430\\u0439\\u0434\\u0430\",\"columnWidth\":49,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u044c\",\"icon\":\"picture-o\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('153', 'FieldtypeTextarea', 'carousel_image_desc', '0', 'carousel image description', '{\"label1022\":\"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0441\\u043b\\u0430\\u0439\\u0434\\u0430\",\"textformatters\":[\"TextformatterEntities\"],\"inputfieldClass\":\"InputfieldTextarea\",\"contentType\":0,\"collapsed\":0,\"columnWidth\":49,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"rows\":5,\"tags\":\"\\u041a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u044c\",\"icon\":\"file-image-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('154', 'FieldtypeRepeater', 'carousel_repeater', '0', 'Carousel slides generator', '{\"label1022\":\"\\u0421\\u043b\\u0430\\u0439\\u0434\\u044b \\u0434\\u043b\\u044f \\u043a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u0438\",\"description1022\":\"\\u0414\\u043e\\u0431\\u0430\\u0432\\u044c\\u0442\\u0435 \\u043e\\u0434\\u0438\\u043d \\u0438\\u043b\\u0438 \\u043d\\u0435\\u0441\\u043a\\u043e\\u043b\\u044c\\u043a\\u043e \\u0441\\u0430\\u043b\\u0439\\u0434\\u043e\\u0432\",\"template_id\":47,\"parent_id\":1056,\"repeaterFields\":[1,105,102,109],\"repeaterTitle\":\"#n: {\\u0421\\u043b\\u0430\\u0439\\u0434}\",\"repeaterAddLabel\":\"Add slide\",\"repeaterAddLabel1022\":\"\\u0414\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c \\u0441\\u043b\\u0430\\u0439\\u0434\",\"repeaterCollapse\":0,\"repeaterLoading\":1,\"collapsed\":0,\"tags\":\"\\u041a\\u0430\\u0440\\u0443\\u0441\\u0435\\u043b\\u044c\",\"icon\":\"file-image-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('155', 'FieldtypeRepeater', 'flip_cards_repeater', '0', 'Flip cards', '{\"label1022\":\"\\u0412\\u0440\\u0430\\u0449\\u0430\\u044e\\u0449\\u0438\\u0435\\u0441\\u044f \\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"template_id\":48,\"parent_id\":1063,\"repeaterFields\":[1,127,105,109,102],\"repeaterTitle\":\"#n: {\\u041f\\u0440\\u0435\\u0438\\u043c\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u043e}\",\"repeaterAddLabel\":\"Add advantage\",\"repeaterAddLabel1022\":\"\\u0414\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c \\u043f\\u0440\\u0435\\u0438\\u043c\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u043e\",\"repeaterCollapse\":0,\"repeaterLoading\":1,\"collapsed\":1,\"tags\":\"\\u0424\\u043b\\u0438\\u043f-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"hand-o-up\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('156', 'FieldtypeFieldsetOpen', 'flip_cards', '0', 'Flip cards', '{\"label1022\":\"\\u0412\\u0440\\u0430\\u0449\\u0430\\u044e\\u0449\\u0438\\u0435\\u0441\\u044f \\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"closeFieldID\":157,\"collapsed\":1,\"tags\":\"\\u0424\\u043b\\u0438\\u043f-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('157', 'FieldtypeFieldsetClose', 'flip_cards_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'flip_cards\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":156,\"tags\":\"\\u0424\\u043b\\u0438\\u043f-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('158', 'FieldtypeText', 'flip_cards_section_classes', '0', 'Clip cards section classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u0432\\u0440\\u0430\\u0449\\u0430\\u044e\\u0449\\u0438\\u0445\\u0441\\u044f \\u043a\\u0430\\u0440\\u0442\",\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043b\\u0438\\u043f-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"hand-pointer-o\",\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('159', 'FieldtypeImage', 'flip_cards_section_image', '0', 'flip cards section parallax', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0424\\u043b\\u0438\\u043f-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"hand-pointer-o\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0444\\u043e\\u043d\\u0430 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('160', 'FieldtypeCheckbox', 'flip_cards_section_paralax', '0', 'flip cards section paralax', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u0424\\u043b\\u0438\\u043f-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"check\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0430\\u0440\\u0430\\u043b\\u043b\\u0430\\u043a\\u0441\\u0430 \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"collapsed\":0,\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('161', 'FieldtypeText', 'flip_cards_title', '0', 'flip cards title', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043b\\u0438\\u043f-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"hand-pointer-o\",\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u0432\\u0440\\u0430\\u0449\\u0430\\u044e\\u0449\\u0438\\u0445\\u0441\\u044f \\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u0435\\u043a\",\"minlength\":0,\"showCount\":0,\"size\":0,\"collapsed\":0,\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('162', 'FieldtypeFieldsetOpen', 'pub_cards', '0', 'Public cards', '{\"label1022\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438 \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"closeFieldID\":163,\"collapsed\":1,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438-\\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('163', 'FieldtypeFieldsetClose', 'pub_cards_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'pub_cards\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":162,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438-\\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('164', 'FieldtypeText', 'pub_title', '0', 'publications title', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438-\\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"icon\":\"th-large\",\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0434\\u043b\\u044f \\u043a\\u0430\\u0440\\u0442 \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"minlength\":0,\"showCount\":0,\"size\":0,\"collapsed\":0,\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('165', 'FieldtypeText', 'pub_section_classes', '0', 'Public cards section classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438  \\u043a\\u0430\\u0440\\u0442 \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438-\\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"icon\":\"th-large\",\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('166', 'FieldtypeImage', 'pub_section_image', '0', 'Public cards section image', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438-\\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"icon\":\"th-large\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0444\\u043e\\u043d\\u0430 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":1,\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('167', 'FieldtypeCheckbox', 'pub_section_paralax', '0', 'pub cards section parallax', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438-\\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"icon\":\"th-large\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0430\\u0440\\u0430\\u043b\\u043b\\u0430\\u043a\\u0441\\u0430 \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('168', 'FieldtypePage', 'pub_cards_parent', '0', 'cards parent', '{\"label1024\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"inputfield\":\"InputfieldPageListSelect\",\"parent_id\":1,\"labelFieldName\":\"title\",\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438-\\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"icon\":\"th-large\",\"label1019\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"label1022\":\"\\u0420\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044c\\u0441\\u043a\\u0430\\u044f \\u043a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0438\\u044f \\u0434\\u043b\\u044f \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"derefAsPage\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('169', 'FieldtypeText', 'pub_card_classes', '0', 'Public cards classes', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438-\\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"icon\":\"th-large\",\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u043a\\u0430\\u0436\\u0434\\u043e\\u0439 \\u043a\\u0430\\u0440\\u0442\\u044b \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0438\",\"minlength\":0,\"showCount\":0,\"size\":0,\"collapsed\":0,\"columnWidth\":55}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('170', 'FieldtypeInteger', 'pub_cards_max', '0', 'max number publications', '{\"label1019\":\"\\u0427\\u0438\\u0441\\u043b\\u043e\",\"inputType\":\"text\",\"size\":10,\"max\":24,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438-\\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"icon\":\"th-large\",\"label1022\":\"\\u041c\\u0430\\u043a\\u0441\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":35,\"min\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('171', 'FieldtypeText', 'flip_card_classes', '0', 'Card classes', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043b\\u0438\\u043f-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"hand-pointer-o\",\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u043a\\u0430\\u0436\\u0434\\u043e\\u0439 \\u043a\\u0430\\u0440\\u0442\\u044b\",\"minlength\":0,\"showCount\":0,\"size\":0,\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('175', 'FieldtypeImage', 'page_preview_section_image', '0', 'page preview section image', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435-\\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"icon\":\"file-text-o\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0444\\u043e\\u043d\\u0430 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('172', 'FieldtypeFieldsetOpen', 'page_preview', '0', 'page preview', '{\"label1022\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435 \\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"closeFieldID\":173,\"collapsed\":1,\"tags\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435-\\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('173', 'FieldtypeFieldsetClose', 'page_preview_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'page_preview\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":172,\"tags\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435-\\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('174', 'FieldtypeText', 'page_preview_section_classes', '0', 'section classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435-\\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"icon\":\"file-text-o\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('176', 'FieldtypeCheckbox', 'page_preview_section_parallax', '0', 'page preview section parallax', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435-\\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"icon\":\"file-text-o\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0430\\u0440\\u0430\\u043b\\u043b\\u0430\\u043a\\u0441\\u0430 \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('177', 'FieldtypePage', 'page_preview_page', '0', 'page preview page', '{\"label1024\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"inputfield\":\"InputfieldPageListSelect\",\"parent_id\":1,\"labelFieldName\":\"title\",\"tags\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435-\\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"icon\":\"file-text-o\",\"label1019\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"label1022\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u0430 \\u0434\\u043b\\u044f \\u0432\\u044b\\u0432\\u043e\\u0434\\u0430 \\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u044f\",\"derefAsPage\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('178', 'FieldtypeFieldsetOpen', 'static_cards', '0', 'static cards', '{\"label1022\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0447\\u043d\\u044b\\u0435 \\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"closeFieldID\":179,\"collapsed\":1,\"tags\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0447\\u043d\\u044b\\u0435-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('179', 'FieldtypeFieldsetClose', 'static_cards_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'static_cards\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":178,\"tags\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0447\\u043d\\u044b\\u0435-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('180', 'FieldtypeText', 'static_cards_section_classes', '0', 'static cards section classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0447\\u043d\\u044b\\u0435-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"list-alt\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('181', 'FieldtypeImage', 'static_cards_section_image', '0', 'static cards section image', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0447\\u043d\\u044b\\u0435-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"list-alt\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0444\\u043e\\u043d\\u0430 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":1,\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('182', 'FieldtypeCheckbox', 'static_cards_section_parallax', '0', 'static cards section parallax', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0447\\u043d\\u044b\\u0435-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"list-alt\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0430\\u0440\\u0430\\u043b\\u043b\\u0430\\u043a\\u0441\\u0430 \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('183', 'FieldtypeText', 'static_cards_title', '0', 'static cards title', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0447\\u043d\\u044b\\u0435-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"list-alt\",\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"minlength\":0,\"showCount\":0,\"size\":0,\"collapsed\":0,\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('184', 'FieldtypeRepeater', 'static_cards_repeator', '0', 'static cards repeator', '{\"label1022\":\"\\u0413\\u0435\\u043d\\u0435\\u0440\\u0430\\u0442\\u043e\\u0440 \\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u0435\\u043a\",\"template_id\":49,\"parent_id\":1074,\"repeaterFields\":[1,169,105,128,185,102,109],\"repeaterTitle\":\"#n: {\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0430}\",\"repeaterAddLabel\":\"Add card\",\"repeaterAddLabel1022\":\"\\u0414\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c \\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0443\",\"repeaterCollapse\":0,\"repeaterLoading\":1,\"repeaterMaxItems\":24,\"repeaterMinItems\":1,\"collapsed\":1,\"tags\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0447\\u043d\\u044b\\u0435-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"list-alt\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('185', 'FieldtypeText', 'static_cards_subtitle', '0', 'static cards subtitle', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0447\\u043d\\u044b\\u0435-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"list-alt\",\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0440\\u044f\\u0434\\u043e\\u043c \\u0441 \\u0438\\u043a\\u043e\\u043d\\u043a\\u043e\\u0439\",\"minlength\":0,\"showCount\":0,\"size\":0,\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('186', 'FieldtypeText', 'author', '0', 'Author', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"textformatters\":[\"TextformatterEntities\"],\"maxlength\":2048,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"address-book\",\"label1022\":\"\\u0418\\u0434\\u0435\\u043d\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442\\u043e\\u0440 Schema.org\",\"minlength\":0,\"showCount\":0,\"size\":0,\"collapsed\":0,\"columnWidth\":32,\"description1022\":\"http:\\/\\/schema.org\\/Organization\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('187', 'FieldtypeText', 'blog_section_classes', '0', 'blog section classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0411\\u043b\\u043e\\u0433\",\"icon\":\"list\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('188', 'FieldtypeFieldsetOpen', 'blog', '0', 'Blog', '{\"label1022\":\"\\u0411\\u043b\\u043e\\u0433\",\"closeFieldID\":189,\"collapsed\":1,\"tags\":\"\\u0411\\u043b\\u043e\\u0433\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('189', 'FieldtypeFieldsetClose', 'blog_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'blog\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":188,\"tags\":\"\\u0411\\u043b\\u043e\\u0433\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('190', 'FieldtypeImage', 'blog_section_image', '0', 'blog section image', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0411\\u043b\\u043e\\u0433\",\"icon\":\"list-ul\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0444\\u043e\\u043d\\u0430 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('191', 'FieldtypeCheckbox', 'blog_section_parallax', '0', 'blog section parallax', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u0411\\u043b\\u043e\\u0433\",\"icon\":\"list-ul\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0430\\u0440\\u0430\\u043b\\u043b\\u0430\\u043a\\u0441\\u0430 \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('192', 'FieldtypeText', 'blog_title', '0', 'blog title', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0411\\u043b\\u043e\\u0433\",\"icon\":\"list\",\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u0431\\u043b\\u043e\\u0433\\u0430\",\"minlength\":0,\"showCount\":0,\"size\":0,\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('193', 'FieldtypePage', 'blog_parent', '0', 'Partnt category', '{\"label1024\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"inputfield\":\"InputfieldPageListSelect\",\"parent_id\":1,\"labelFieldName\":\"title\",\"tags\":\"\\u0411\\u043b\\u043e\\u0433\",\"icon\":\"list-ul\",\"label1019\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"label1022\":\"\\u0420\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044c\\u0441\\u043a\\u0430\\u044f \\u043a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0438\\u044f \\u0434\\u043b\\u044f \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"derefAsPage\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('194', 'FieldtypeInteger', 'blog_max', '0', 'max number publications', '{\"label1019\":\"\\u0427\\u0438\\u0441\\u043b\\u043e\",\"inputType\":\"text\",\"size\":10,\"max\":24,\"tags\":\"\\u0411\\u043b\\u043e\\u0433\",\"icon\":\"list-ul\",\"label1022\":\"\\u041c\\u0430\\u043a\\u0441\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"zeroNotEmpty\":0,\"columnWidth\":49,\"min\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('195', 'FieldtypeFieldsetOpen', 'tabs', '0', 'tabs', '{\"label1022\":\"\\u0422\\u0430\\u0431\\u044b\",\"closeFieldID\":196,\"collapsed\":1,\"tags\":\"\\u0422\\u0430\\u0431\\u044b\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('196', 'FieldtypeFieldsetClose', 'tabs_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'tabs\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":195,\"tags\":\"\\u0422\\u0430\\u0431\\u044b\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('197', 'FieldtypeText', 'tabs_section_classes', '0', 'tabs section classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0422\\u0430\\u0431\\u044b\",\"icon\":\"align-justify\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('198', 'FieldtypeImage', 'tabs_section_image', '0', 'tabs section images', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0422\\u0430\\u0431\\u044b\",\"icon\":\"align-justify\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0444\\u043e\\u043d\\u0430 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('199', 'FieldtypeCheckbox', 'tabs_section_parallax', '0', 'tabs preview section parallax', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u0422\\u0430\\u0431\\u044b\",\"icon\":\"align-justify\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0430\\u0440\\u0430\\u043b\\u043b\\u0430\\u043a\\u0441\\u0430 \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('200', 'FieldtypeText', 'tabs_title', '0', 'tabs title', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0422\\u0430\\u0431\\u044b\",\"icon\":\"align-justify\",\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u0442\\u0430\\u0431\\u043e\\u0432\",\"minlength\":0,\"showCount\":0,\"size\":0,\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('201', 'FieldtypeRepeater', 'tabs_repeater', '0', 'tabs repeater', '{\"label1022\":\"\\u0413\\u0435\\u043d\\u0435\\u0440\\u0430\\u0442\\u043e\\u0440 \\u0442\\u0430\\u0431\\u043e\\u0432\",\"template_id\":50,\"parent_id\":1080,\"repeaterFields\":[1,102],\"repeaterTitle\":\"#n: {\\u0422\\u0430\\u0431}\",\"repeaterAddLabel\":\"Add tab\",\"repeaterAddLabel1022\":\"\\u0414\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c \\u0442\\u0430\\u0431\",\"repeaterCollapse\":0,\"repeaterLoading\":1,\"collapsed\":0,\"tags\":\"\\u0422\\u0430\\u0431\\u044b\",\"icon\":\"align-justify\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('202', 'FieldtypeText', 'tabs_title_classes', '0', 'tabs title classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u043f\\u043b\\u0430\\u0448\\u043a\\u0438 \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043a\\u043e\\u0432 \\u0442\\u0430\\u0431\\u043e\\u0432\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0422\\u0430\\u0431\\u044b\",\"icon\":\"align-justify\",\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('203', 'FieldtypeText', 'tabs_body_classes', '0', 'tabs body classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u043a\\u043e\\u043d\\u0442\\u0435\\u043d\\u0442\\u043d\\u043e\\u0439 \\u0447\\u0430\\u0441\\u0442\\u0438 \\u0442\\u0430\\u0431\\u043e\\u0432\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0422\\u0430\\u0431\\u044b\",\"icon\":\"align-justify\",\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('204', 'FieldtypeFieldsetOpen', 'gallery', '0', 'Gallery', '{\"label1022\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"closeFieldID\":205,\"collapsed\":1,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"folder-o\",\"clone_field\":\"gallery2\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('205', 'FieldtypeFieldsetClose', 'gallery_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'gallery\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":204,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('206', 'FieldtypeText', 'gallery_title', '0', 'Gallery title', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"camera\",\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u0444\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"minlength\":0,\"showCount\":0,\"size\":0,\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('207', 'FieldtypeText', 'gallery_classes', '0', 'gallery classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"camera\",\"columnWidth\":49,\"collapsed\":0,\"clone_field\":\"gallery2_classes\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('208', 'FieldtypeImage', 'gallery_image', '0', 'gallery image', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"camera\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0444\\u043e\\u043d\\u0430 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":1,\"clone_field\":\"gallery2_image\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('209', 'FieldtypeCheckbox', 'gallery_parallax', '0', 'gallery section parallax', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"camera\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0430\\u0440\\u0430\\u043b\\u043b\\u0430\\u043a\\u0441\\u0430 \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"columnWidth\":49,\"collapsed\":0,\"clone_field\":\"gallery2_parallax\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('210', 'FieldtypeImage', 'gallery_images', '0', 'Images', '{\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"collapsed\":1,\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"camera\",\"fileSchema\":6,\"clone_field\":\"gallery2_images\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('211', 'FieldtypeCheckbox', 'gallery_popup', '0', 'Gallery popup checkbox', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"camera\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u043e\\u043f\\u0430\\u043f\\u0430 \\u0434\\u043b\\u044f \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"collapsed\":0,\"columnWidth\":49,\"clone_field\":\"gallery2_popup\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('212', 'FieldtypeText', 'gallery_images_classes', '0', 'gallery images classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0431\\u043b\\u043e\\u043a\\u0430 \\u0438\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"camera\",\"columnWidth\":49,\"collapsed\":0,\"clone_field\":\"gallery2_images_classes\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('213', 'FieldtypeText', 'flip_cards_title_classes', '0', 'flip cards title classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043a\\u0430\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043b\\u0438\\u043f-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"hand-pointer-o\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('214', 'FieldtypeText', 'pub_title_classes', '0', 'pub title classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043a\\u0430\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438-\\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439\",\"icon\":\"th-large\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('215', 'FieldtypeText', 'static_cards_title_classes', '0', 'static cards title classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043a\\u0430\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0421\\u0442\\u0430\\u0442\\u0438\\u0447\\u043d\\u044b\\u0435-\\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043a\\u0438\",\"icon\":\"list-alt\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('216', 'FieldtypeText', 'blog_title_classes', '0', 'blog title classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043a\\u0430\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0411\\u043b\\u043e\\u0433\",\"icon\":\"list\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('217', 'FieldtypeText', 'tabs_title_top_classes', '0', 'tabs top title classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043a\\u0430\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0422\\u0430\\u0431\\u044b\",\"icon\":\"align-justify\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('218', 'FieldtypeText', 'gallery_title_classes', '0', 'gallery title classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043a\\u0430\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"camera\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('219', 'FieldtypePage', 'sections_list_after', '0', 'section list after body', '{\"label1022\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u043c\\u043e\\u0434\\u0443\\u043b\\u0435\\u0439 \\u043f\\u043e\\u0441\\u043b\\u0435 \\u0442\\u0435\\u043b\\u0430 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"derefAsPage\":0,\"inputfield\":\"InputfieldAsmSelect\",\"parent_id\":1045,\"labelFieldName\":\"title\",\"tags\":\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435\",\"icon\":\"cubes\",\"usePageEdit\":0,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('220', 'FieldtypeFieldsetTabOpen', 'section', '0', 'Section', '{\"label1022\":\"\\u0421\\u0435\\u043a\\u0446\\u0438\\u044f\",\"closeFieldID\":221,\"collapsed\":0,\"tags\":\"\\u0421\\u0435\\u043a\\u0446\\u0438\\u044f\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('221', 'FieldtypeFieldsetClose', 'section_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'section\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":220,\"tags\":\"\\u0421\\u0435\\u043a\\u0446\\u0438\\u044f\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('222', 'FieldtypeText', 'section_classes', '0', 'section classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0421\\u0435\\u043a\\u0446\\u0438\\u044f\",\"icon\":\"minus-square-o\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('223', 'FieldtypeImage', 'section_image', '0', 'section image', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0421\\u0435\\u043a\\u0446\\u0438\\u044f\",\"icon\":\"minus-square-o\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0444\\u043e\\u043d\\u0430 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('224', 'FieldtypeCheckbox', 'section_parallax', '0', 'section parallax', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u0421\\u0435\\u043a\\u0446\\u0438\\u044f\",\"icon\":\"minus-square-o\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0430\\u0440\\u0430\\u043b\\u043b\\u0430\\u043a\\u0441\\u0430 \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('225', 'FieldtypeText', 'section_title_classes', '0', 'section title classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043a\\u0430\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0421\\u0435\\u043a\\u0446\\u0438\\u044f\",\"icon\":\"minus-square-o\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('226', 'FieldtypeText', 'form_classes', '0', 'form classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0444\\u043e\\u0440\\u043c\\u044b\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('227', 'FieldtypeText', 'form_email', '0', 'form email', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1022\":\"\\u0410\\u0434\\u0440\\u0435\\u0441 \\u043d\\u0430 \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0439 \\u0431\\u0443\\u0434\\u0435\\u0442 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0430 \\u0444\\u043e\\u0440\\u043c\\u0430\",\"minlength\":0,\"showCount\":0,\"size\":0,\"collapsed\":0,\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('228', 'FieldtypeText', 'form_theme', '0', 'form theme', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1022\":\"\\u0422\\u0435\\u043c\\u0430 \\u0441\\u043e\\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u044f\",\"minlength\":0,\"showCount\":0,\"size\":0,\"collapsed\":0,\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('229', 'FieldtypeText', 'field_placeholder', '0', 'field placeholder', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1022\":\"\\u041f\\u043b\\u044d\\u0439\\u0441\\u0445\\u043e\\u043b\\u0434\\u0435\\u0440 \\u0434\\u043b\\u044f \\u043f\\u043e\\u043b\\u044f\",\"minlength\":0,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('230', 'FieldtypeText', 'field_name', '0', 'field name', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1022\":\"\\u0438\\u043c\\u044f \\u043f\\u043e\\u043b\\u044f\",\"minlength\":0,\"showCount\":0,\"size\":0,\"description\":\"non repeat for one form\",\"description1022\":\"\\u041d\\u0435 \\u0434\\u043e\\u043b\\u0436\\u043d\\u043e \\u043f\\u043e\\u0432\\u0442\\u043e\\u0440\\u044f\\u0442\\u044c\\u0441\\u044f \\u0434\\u043b\\u044f \\u043e\\u0434\\u043d\\u043e\\u0439 \\u0444\\u043e\\u0440\\u043c\\u044b!\",\"collapsed\":0,\"required\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('231', 'FieldtypeText', 'field_pattern', '0', 'field pattern', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1022\":\"\\u041f\\u0430\\u0442\\u0442\\u0435\\u0440\\u043d \\u0434\\u043b\\u044f \\u043f\\u043e\\u043b\\u044f\",\"minlength\":0,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('232', 'FieldtypeCheckbox', 'field_required', '0', 'field required', '{\"label1022\":\"\\u041e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043f\\u043e\\u043b\\u0435\",\"collapsed\":0,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('233', 'FieldtypeText', 'field_error', '0', 'field error', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1022\":\"\\u0422\\u0435\\u043a\\u0441\\u0442 \\u043e\\u0448\\u0438\\u0431\\u043a\\u0438 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u0438\\u044f\",\"minlength\":0,\"showCount\":0,\"size\":0,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('234', 'FieldtypeText', 'field_value', '0', 'field value', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1022\":\"\\u0417\\u043d\\u0430\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u043e\\u043b\\u044f\",\"minlength\":0,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('235', 'FieldtypeCheckbox', 'field_hidden', '0', 'field hidden', '{\"label1022\":\"\\u0421\\u043a\\u0440\\u044b\\u0442\\u043e\\u0435 \\u043f\\u043e\\u043b\\u0435\",\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('236', 'FieldtypeRepeater', 'field_options', '0', 'field options', '{\"label1022\":\"\\u0413\\u0435\\u043d\\u0435\\u0440\\u0430\\u0442\\u043e\\u0440 \\u044d\\u043b\\u0435\\u043c\\u0435\\u043d\\u0442\\u043e\\u0432 \\u0434\\u043b\\u044f \\u0441\\u043f\\u0438\\u0441\\u043a\\u0430\",\"template_id\":57,\"parent_id\":1087,\"repeaterFields\":[230,234,117],\"repeaterTitle\":\"#n: {\\u044d\\u043b\\u0435\\u043c\\u0435\\u043d\\u0442}\",\"repeaterAddLabel\":\"Add option\",\"repeaterAddLabel1022\":\"\\u0414\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c \\u044d\\u043b\\u0435\\u043c\\u0435\\u043d\\u0442\",\"repeaterCollapse\":0,\"repeaterLoading\":1,\"collapsed\":0,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('237', 'FieldtypeText', 'field_label', '0', 'field label', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1022\":\"\\u041b\\u044d\\u0439\\u0431\\u043b \\u043f\\u043e\\u043b\\u044f\",\"minlength\":0,\"showCount\":0,\"size\":0,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('238', 'FieldtypeCheckbox', 'field_on', '0', 'field checked/selected', '{\"label1022\":\"\\u041f\\u043e\\u043b\\u0435 \\u0432\\u044b\\u0431\\u0440\\u0430\\u043d\\u043e \\u043f\\u043e \\u0443\\u043c\\u043e\\u043b\\u0447\\u0430\\u043d\\u0438\\u044e\",\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('239', 'FieldtypeText', 'field_classes', '0', 'field classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u043f\\u043e\\u043b\\u044f\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('240', 'FieldtypePage', 'field_type', '0', 'field type', '{\"label1022\":\"\\u0422\\u0438\\u043f \\u0442\\u0435\\u043a\\u0441\\u0442\\u043e\\u0432\\u043e\\u0433\\u043e \\u043f\\u043e\\u043b\\u044f\",\"derefAsPage\":1,\"inputfield\":\"InputfieldPageListSelect\",\"parent_id\":1088,\"labelFieldName\":\"title\",\"collapsed\":0,\"required\":1,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('241', 'FieldtypeText', 'field_success', '0', 'field success', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1022\":\"\\u0422\\u0435\\u043a\\u0441\\u0442 \\u043f\\u0440\\u0438 \\u0443\\u0441\\u043f\\u0435\\u0448\\u043d\\u043e\\u043c \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u0438\\u0438\",\"minlength\":0,\"showCount\":0,\"size\":0,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('242', 'FieldtypeText', 'form_id', '0', 'form id', '{\"label1022\":\"ID \\u0444\\u043e\\u0440\\u043c\\u044b\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('243', 'FieldtypeTextarea', 'field_desc', '0', 'field description', '{\"label1019\":\"\\u0422\\u0435\\u043a\\u0441\\u0442 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"textformatters\":[\"TextformatterEntities\"],\"inputfieldClass\":\"InputfieldCKEditor\",\"showCount\":1,\"rows\":5,\"toolbar\":\"Format, Styles, -, Bold, Italic, -, RemoveFormat\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1024\":\"\\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u043e\\u0435 \\u0441\\u043e\\u0434\\u0435\\u0440\\u0436\\u0438\\u043c\\u043e\\u0435 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"label1022\":\"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u043f\\u043e\\u043b\\u044f \\u0441 html \\u043e\\u0444\\u043e\\u0440\\u043c\\u043b\\u0435\\u043d\\u0438\\u0435\\u043c\",\"contentType\":0,\"minlength\":0,\"maxlength\":0,\"inlineMode\":0,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('244', 'FieldtypeImage', 'form_image', '0', 'form image', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0444\\u043e\\u043d\\u0430 \\u0444\\u043e\\u0440\\u043c\\u044b\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('245', 'FieldtypeText', 'form_section_classes', '0', 'Form module classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u043c\\u043e\\u0434\\u0443\\u043b\\u044f \\u0444\\u043e\\u0440\\u043c\\u044b\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('246', 'FieldtypeImage', 'form_section_image', '0', 'form section image', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0444\\u043e\\u043d\\u0430 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('247', 'FieldtypeCheckbox', 'form_section_parallax', '0', 'form section parallax', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0430\\u0440\\u0430\\u043b\\u043b\\u0430\\u043a\\u0441\\u0430 \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('248', 'FieldtypePage', 'form_page', '0', 'Form page', '{\"label1022\":\"\\u0412\\u044b\\u0431\\u043e\\u0440 \\u043a\\u043e\\u043d\\u043a\\u0440\\u0435\\u0442\\u043d\\u043e\\u0439 \\u0444\\u043e\\u0440\\u043c\\u044b \\u0434\\u043b\\u044f \\u043c\\u043e\\u0434\\u0443\\u043b\\u044f\",\"derefAsPage\":1,\"inputfield\":\"InputfieldPageListSelect\",\"parent_id\":1085,\"labelFieldName\":\"title\",\"collapsed\":0,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('249', 'FieldtypeFieldsetOpen', 'form', '0', 'form', '{\"label1022\":\"\\u0424\\u043e\\u0440\\u043c\\u0430 \\u0441\\u0432\\u044f\\u0437\\u0438\",\"closeFieldID\":250,\"collapsed\":1,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('250', 'FieldtypeFieldsetClose', 'form_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'form\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":249,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('251', 'FieldtypeText', 'form_section_title', '0', 'section title classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043a\\u0430\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('253', 'FieldtypeText', 'form_submit_text', '0', 'form submit text', '{\"label1022\":\"\\u0422\\u0435\\u043a\\u0441\\u0442 \\u043a\\u043d\\u043e\\u043f\\u043a\\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043a\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('252', 'FieldtypeText', 'form_title', '0', 'Form section title', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u0444\\u043e\\u0440\\u043c\\u044b\",\"minlength\":0,\"showCount\":0,\"size\":0,\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('254', 'FieldtypeText', 'form_submit_classes', '0', 'form submit classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u043a\\u043d\\u043e\\u043f\\u043a\\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043a\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('255', 'FieldtypeText', 'field_label_classes', '0', 'field label classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u043b\\u044d\\u0439\\u0431\\u043b\\u0430\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"icon\":\"envelope-o\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('256', 'FieldtypeFieldsetOpen', 'map', '0', 'map', '{\"label1022\":\"\\u041a\\u0430\\u0440\\u0442\\u0430\",\"closeFieldID\":257,\"collapsed\":1,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u0430\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('257', 'FieldtypeFieldsetClose', 'map_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'map\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":256,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u0430\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('258', 'FieldtypeText', 'map_center', '0', 'map center', '{\"label1022\":\"\\u0426\\u0435\\u043d\\u0442\\u0440 \\u043a\\u0430\\u0440\\u0442\\u044b\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u0430\",\"icon\":\"map-o\",\"columnWidth\":49,\"description\":\"https:\\/\\/yandex.ru\\/map-constructor\\/location-tool\\/\",\"description1022\":\"https:\\/\\/yandex.ru\\/map-constructor\\/location-tool\\/\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('259', 'FieldtypeText', 'map_point', '0', 'map point', '{\"label1022\":\"\\u0422\\u043e\\u0447\\u043a\\u0430 \\u043d\\u0430 \\u043a\\u0430\\u0440\\u0442\\u0435\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u0430\",\"icon\":\"map-o\",\"columnWidth\":49,\"description\":\"https:\\/\\/yandex.ru\\/map-constructor\\/location-tool\\/\",\"description1022\":\"https:\\/\\/yandex.ru\\/map-constructor\\/location-tool\\/\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('260', 'FieldtypeInteger', 'map_zoom', '0', 'map zoom', '{\"label1022\":\"\\u041c\\u0430\\u0441\\u0448\\u0442\\u0430\\u0431 \\u043a\\u0430\\u0440\\u0442\\u044b\",\"zeroNotEmpty\":0,\"collapsed\":0,\"inputType\":\"number\",\"size\":16,\"min\":1,\"max\":19,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u0430\",\"icon\":\"map-o\",\"columnWidth\":32}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('261', 'FieldtypeTextarea', 'map_desc', '0', 'map description', '{\"label1019\":\"\\u0422\\u0435\\u043a\\u0441\\u0442 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"inputfieldClass\":\"InputfieldTextarea\",\"showCount\":1,\"rows\":5,\"toolbar\":\"Format, Styles, -, Bold, Italic, -, RemoveFormat\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u0430\",\"icon\":\"map-o\",\"label1024\":\"\\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u043e\\u0435 \\u0441\\u043e\\u0434\\u0435\\u0440\\u0436\\u0438\\u043c\\u043e\\u0435 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"label1022\":\"\\u0410\\u0434\\u0440\\u0435\\u0441 \\u0438\\u043b\\u0438 \\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u043d\\u0430 \\u043a\\u0430\\u0440\\u0442\\u0435\",\"contentType\":0,\"minlength\":0,\"maxlength\":0,\"inlineMode\":0,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('262', 'FieldtypeCheckbox', 'map_scroll', '0', 'map scroll', '{\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0412\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c \\u0441\\u043a\\u0440\\u043e\\u043b\\u043b \\u043a\\u0430\\u0440\\u0442\\u044b \\u043c\\u044b\\u0448\\u043a\\u043e\\u0439\",\"collapsed\":0,\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u0430\",\"icon\":\"map-o\",\"columnWidth\":32}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('263', 'FieldtypeCheckbox', 'map_drag', '0', 'map drag', '{\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0412\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c \\u0441\\u043a\\u0440\\u043e\\u043b\\u043b \\u043a\\u0430\\u0440\\u0442\\u044b \\u043f\\u0430\\u043b\\u044c\\u0446\\u0435\\u043c \\u043d\\u0430 \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0435\",\"tags\":\"\\u041a\\u0430\\u0440\\u0442\\u0430\",\"icon\":\"map-o\",\"columnWidth\":32,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('264', 'FieldtypeText', 'page_preview_title_classes', '0', 'title classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043a\\u0430\",\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u041a\\u0440\\u0430\\u0442\\u043a\\u043e\\u0435-\\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"icon\":\"file-text-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('265', 'FieldtypePage', 'gallery_page', '0', 'Gallery page', '{\"label1024\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"inputfield\":\"InputfieldPageListSelect\",\"parent_id\":1,\"labelFieldName\":\"title\",\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"camera\",\"label1019\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"label1022\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u0430 \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"derefAsPage\":1,\"collapsed\":0,\"columnWidth\":49,\"clone_field\":\"gallery2_page\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('266', 'FieldtypeInteger', 'gallery_count', '0', 'Gallery counter', '{\"label1022\":\"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u0444\\u043e\\u0442\\u043e\",\"zeroNotEmpty\":0,\"collapsed\":0,\"inputType\":\"number\",\"size\":10,\"min\":1,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"icon\":\"camera\",\"columnWidth\":49,\"clone_field\":\"gallery2_count\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('267', 'FieldtypeText', 'video_section_classes', '0', 'Video section classes', '{\"label1022\":\"\\u041a\\u0430\\u043b\\u0430\\u0441\\u0441\\u044b \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u0432\\u0438\\u0434\\u0435\\u043e\",\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0412\\u0438\\u0434\\u0435\\u043e\",\"icon\":\"caret-square-o-right\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('268', 'FieldtypeFieldsetOpen', 'video', '0', 'video', '{\"closeFieldID\":269,\"label1022\":\"\\u0412\\u0438\\u0434\\u0435\\u043e\",\"collapsed\":1,\"tags\":\"\\u0412\\u0438\\u0434\\u0435\\u043e\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('269', 'FieldtypeFieldsetClose', 'video_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'video\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":268,\"tags\":\"\\u0412\\u0438\\u0434\\u0435\\u043e\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('270', 'FieldtypeImage', 'video_preview', '0', 'video preview', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0412\\u0438\\u0434\\u0435\\u043e\",\"icon\":\"caret-square-o-right\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u041f\\u0440\\u0435\\u0432\\u044c\\u044e \\u0432\\u0438\\u0434\\u0435\\u043e\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":0,\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('271', 'FieldtypeFile', 'video_file', '0', 'Video file', '{\"label1022\":\"\\u0412\\u0438\\u0434\\u0435\\u043e\",\"extensions\":\"avi mpeg mp4\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldFile\",\"descriptionRows\":1,\"tags\":\"\\u0412\\u0438\\u0434\\u0435\\u043e\",\"icon\":\"caret-square-o-right\",\"fileSchema\":6,\"collapsed\":0,\"columnWidth\":50}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('272', 'FieldtypeImage', 'video_logo', '0', 'Video logo', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0412\\u0438\\u0434\\u0435\\u043e\",\"icon\":\"caret-square-o-right\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u041b\\u043e\\u0433\\u043e\\u0442\\u0438\\u043f \\u0432 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u0432\\u0438\\u0434\\u0435\\u043e\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('273', 'FieldtypeText', 'images_title', '0', 'Images gallery title', '{\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0434\\u043b\\u044f \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"info-circle\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('274', 'FieldtypeText', 'images_sections_classes', '0', 'Images sections classes', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0431\\u043b\\u043e\\u043a\\u0430 \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438 \\u0438\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0439\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"info-circle\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('275', 'FieldtypeRepeater', 'contacts_fields', '0', 'Contacts fields', '{\"label1022\":\"\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u043d\\u044b\\u0435 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0435\",\"template_id\":60,\"parent_id\":1138,\"repeaterFields\":[237,128,109,186],\"repeaterTitle\":\"#n: {\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442}\",\"repeaterAddLabel\":\"Add column\",\"repeaterAddLabel1022\":\"\\u0414\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\",\"repeaterCollapse\":1,\"repeaterLoading\":1,\"rememberOpen\":1,\"accordionMode\":1,\"collapsed\":1,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"icon\":\"address-card-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('276', 'FieldtypeFieldsetOpen', 'gallery2', '0', 'Gallery (copy)', '{\"label1022\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f\",\"closeFieldID\":277,\"collapsed\":1,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('277', 'FieldtypeFieldsetClose', 'gallery2_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'gallery2\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":276,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('278', 'FieldtypeText', 'gallery2_classes', '0', 'gallery classes (copy)', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('279', 'FieldtypeInteger', 'gallery2_count', '0', 'Gallery counter (copy)', '{\"label1022\":\"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u0444\\u043e\\u0442\\u043e\",\"zeroNotEmpty\":0,\"collapsed\":0,\"inputType\":\"number\",\"size\":10,\"min\":1,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('280', 'FieldtypeImage', 'gallery2_image', '0', 'gallery image (copy)', '{\"label1019\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":2,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"fileSchema\":6,\"label1024\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435\",\"maxWidth\":1920,\"focusMode\":\"on\",\"clientQuality\":90,\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0444\\u043e\\u043d\\u0430 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"defaultValuePage\":0,\"useTags\":0,\"resizeServer\":0,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('281', 'FieldtypeImage', 'gallery2_images', '0', 'Images (copy)', '{\"label1022\":\"\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u0434\\u043b\\u044f \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"collapsed\":1,\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"fileSchema\":6}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('282', 'FieldtypeText', 'gallery2_images_classes', '0', 'gallery images classes (copy)', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0431\\u043b\\u043e\\u043a\\u0430 \\u0438\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('283', 'FieldtypePage', 'gallery2_page', '0', 'Gallery page (copy)', '{\"label1024\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"inputfield\":\"InputfieldPageListSelect\",\"parent_id\":1,\"labelFieldName\":\"title\",\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"label1019\":\"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\",\"label1022\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u0430 \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"derefAsPage\":1,\"collapsed\":0,\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('284', 'FieldtypeCheckbox', 'gallery2_parallax', '0', 'gallery section parallax (copy)', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0430\\u0440\\u0430\\u043b\\u043b\\u0430\\u043a\\u0441\\u0430 \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('285', 'FieldtypeCheckbox', 'gallery2_popup', '0', 'Gallery popup checkbox (copy)', '{\"label1024\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\\/\\u0432\\u044b\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u044c\",\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"label1022\":\"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u043e\\u043f\\u0430\\u043f\\u0430 \\u0434\\u043b\\u044f \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"collapsed\":0,\"columnWidth\":49}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('286', 'FieldtypeText', 'gallery2_title', '0', 'Gallery title (copy)', '{\"label1019\":\"\\u0421\\u0442\\u0440\\u043e\\u043a\\u0430\",\"maxlength\":2048,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"label1022\":\"\\u0417\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043e\\u043a \\u0434\\u043b\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u0444\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"minlength\":0,\"showCount\":0,\"size\":0,\"columnWidth\":49,\"collapsed\":0,\"clone_field\":\"gallery2_title\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('287', 'FieldtypeText', 'gallery2_title_classes', '0', 'gallery title classes (copy)', '{\"label1022\":\"\\u041a\\u043b\\u0430\\u0441\\u0441\\u044b \\u0434\\u043b\\u044f \\u0437\\u0430\\u0433\\u043e\\u043b\\u043e\\u0432\\u043a\\u0430\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"columnWidth\":49,\"collapsed\":0,\"clone_field\":\"gallery2_title_classes\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('288', 'FieldtypeText', 'gallery2_img_height', '0', 'Gallery thumbs height', '{\"label1022\":\"\\u0412\\u044b\\u0441\\u043e\\u0442\\u0430 \\u043f\\u0440\\u0435\\u0432\\u044c\\u044e \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"columnWidth\":49,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('289', 'FieldtypeText', 'gallery2_img_width', '0', 'Gallery thumbs width', '{\"label1022\":\"\\u0428\\u0438\\u0440\\u0438\\u043d\\u0430 \\u043f\\u0440\\u0435\\u0432\\u044c\\u044e \\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u0438\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"\\u0424\\u043e\\u0442\\u043e\\u0433\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f2\",\"icon\":\"camera\",\"columnWidth\":49,\"collapsed\":0}');

DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(128) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `class` (`class`)
) ENGINE=MyISAM AUTO_INCREMENT=184 DEFAULT CHARSET=utf8;

INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('1', 'FieldtypeTextarea', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('2', 'FieldtypeNumber', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('3', 'FieldtypeText', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('4', 'FieldtypePage', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('30', 'InputfieldForm', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('6', 'FieldtypeFile', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('7', 'ProcessPageEdit', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('10', 'ProcessLogin', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('12', 'ProcessPageList', '0', '{\"pageLabelField\":\"title\",\"paginationLimit\":25,\"limit\":50}', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('121', 'ProcessPageEditLink', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('14', 'ProcessPageSort', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('15', 'InputfieldPageListSelect', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('117', 'JqueryUI', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('17', 'ProcessPageAdd', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('125', 'SessionLoginThrottle', '11', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('122', 'InputfieldPassword', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('25', 'InputfieldAsmSelect', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('116', 'JqueryCore', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('27', 'FieldtypeModule', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('28', 'FieldtypeDatetime', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('29', 'FieldtypeEmail', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('108', 'InputfieldURL', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('32', 'InputfieldSubmit', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('33', 'InputfieldWrapper', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('34', 'InputfieldText', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('35', 'InputfieldTextarea', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('36', 'InputfieldSelect', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('37', 'InputfieldCheckbox', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('38', 'InputfieldCheckboxes', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('39', 'InputfieldRadios', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('40', 'InputfieldHidden', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('41', 'InputfieldName', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('43', 'InputfieldSelectMultiple', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('45', 'JqueryWireTabs', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('46', 'ProcessPage', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('47', 'ProcessTemplate', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('48', 'ProcessField', '32', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('50', 'ProcessModule', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('114', 'PagePermissions', '3', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('97', 'FieldtypeCheckbox', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('115', 'PageRender', '3', '{\"clearCache\":1}', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('55', 'InputfieldFile', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('56', 'InputfieldImage', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('57', 'FieldtypeImage', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('60', 'InputfieldPage', '0', '{\"inputfieldClasses\":[\"InputfieldSelect\",\"InputfieldSelectMultiple\",\"InputfieldCheckboxes\",\"InputfieldRadios\",\"InputfieldAsmSelect\",\"InputfieldPageListSelect\",\"InputfieldPageListSelectMultiple\",\"InputfieldPageAutocomplete\"]}', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('61', 'TextformatterEntities', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('66', 'ProcessUser', '0', '{\"showFields\":[\"name\",\"email\",\"roles\"]}', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('67', 'MarkupAdminDataTable', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('68', 'ProcessRole', '0', '{\"showFields\":[\"name\"]}', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('76', 'ProcessList', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('78', 'InputfieldFieldset', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('79', 'InputfieldMarkup', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('80', 'InputfieldEmail', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('89', 'FieldtypeFloat', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('83', 'ProcessPageView', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('84', 'FieldtypeInteger', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('85', 'InputfieldInteger', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('86', 'InputfieldPageName', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('87', 'ProcessHome', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('90', 'InputfieldFloat', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('94', 'InputfieldDatetime', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('98', 'MarkupPagerNav', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('129', 'ProcessPageEditImageSelect', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('103', 'JqueryTableSorter', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('104', 'ProcessPageSearch', '1', '{\"searchFields\":\"title\",\"displayField\":\"title path\"}', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('105', 'FieldtypeFieldsetOpen', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('106', 'FieldtypeFieldsetClose', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('107', 'FieldtypeFieldsetTabOpen', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('109', 'ProcessPageTrash', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('111', 'FieldtypePageTitle', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('112', 'InputfieldPageTitle', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('113', 'MarkupPageArray', '3', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('131', 'InputfieldButton', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('133', 'FieldtypePassword', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('134', 'ProcessPageType', '33', '{\"showFields\":[]}', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('135', 'FieldtypeURL', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('136', 'ProcessPermission', '1', '{\"showFields\":[\"name\",\"title\"]}', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('137', 'InputfieldPageListSelectMultiple', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('138', 'ProcessProfile', '1', '{\"profileFields\":[\"pass\",\"email\",\"admin_theme\",\"admin_theme\",\"language\"]}', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('139', 'SystemUpdater', '1', '{\"systemVersion\":16,\"coreVersion\":\"3.0.98\"}', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('148', 'AdminThemeDefault', '10', '{\"colors\":\"classic\"}', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('149', 'InputfieldSelector', '42', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('150', 'ProcessPageLister', '32', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('151', 'JqueryMagnific', '1', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('155', 'InputfieldCKEditor', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('156', 'MarkupHTMLPurifier', '0', '', '2018-11-15 22:23:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('159', 'ProcessRecentPages', '1', '', '2018-11-15 22:24:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('160', 'AdminThemeUikit', '10', '', '2018-11-15 22:24:21');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('161', 'ProcessLogger', '1', '', '2018-11-15 22:24:33');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('162', 'InputfieldIcon', '0', '', '2018-11-15 22:24:33');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('163', 'AdminThemeReno', '10', '{\"colors\":\"\",\"avatar_field_user\":\"\",\"userFields_user\":\"name\",\"notices\":\"fa-bell\",\"home\":\"fa-home\",\"signout\":\"fa-power-off\",\"page\":\"fa-file-text\",\"setup\":\"fa-wrench\",\"module\":\"fa-briefcase\",\"access\":\"fa-unlock\"}', '2018-11-15 22:25:16');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('164', 'FieldtypeRepeater', '35', '{\"repeatersRootPageID\":1015}', '2018-11-15 22:26:10');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('165', 'InputfieldRepeater', '0', '', '2018-11-15 22:26:10');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('166', 'LanguageSupport', '35', '{\"languagesPageID\":1017,\"defaultLanguagePageID\":1018,\"otherLanguagePageIDs\":[1022],\"languageTranslatorPageID\":1019}', '2018-11-15 22:26:44');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('167', 'ProcessLanguage', '1', '', '2018-11-15 22:26:44');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('168', 'ProcessLanguageTranslator', '1', '', '2018-11-15 22:26:45');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('169', 'LanguageSupportFields', '3', '', '2018-11-15 22:28:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('170', 'FieldtypeTextLanguage', '1', '', '2018-11-15 22:28:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('171', 'FieldtypePageTitleLanguage', '1', '', '2018-11-15 22:28:01');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('172', 'FieldtypeTextareaLanguage', '1', '', '2018-11-15 22:28:01');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('173', 'ProcessDatabaseBackups', '1', '', '2018-11-15 22:29:52');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('174', 'HelperFieldLinks', '3', '', '2018-11-15 22:30:42');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('175', 'WireMailSmtp', '0', '{\"localhost\":\"fcs\",\"smtp_host\":\"smtp.yandex.ru\",\"smtp_port\":465,\"smtp_user\":\"fcs69@yandex.ru\",\"smtp_password\":\"20181115\",\"smtp_start_tls\":\"\",\"smtp_ssl\":1,\"smtp_certificate\":1,\"authentication_mechanism\":\"\",\"realm\":\"\",\"workstation\":\"\",\"sender_email\":\"fcs69@yandex.ru\",\"sender_name\":\"FCS\",\"sender_reply\":\"\",\"sender_errors_to\":\"\",\"sender_signature\":\"\",\"sender_signature_html\":\"\",\"send_sender_signature\":\"1\",\"valid_recipients\":\"\",\"extra_headers\":\"\"}', '2018-11-15 22:31:35');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('176', 'MarkupSitemapXML', '3', '', '2018-11-15 22:39:24');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('177', 'MarkupCache', '3', '', '2018-11-15 22:39:24');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('178', 'FieldtypeYaml', '1', '', '2018-11-15 23:01:39');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('179', 'Duplicator', '11', '{\"packageName\":\"\",\"backupWire\":\"\",\"backups\":1,\"backupLogs\":1,\"backupSessions\":1,\"backupCache\":1,\"backupFiles\":\"\",\"ignoredPath\":\"\",\"ignoredExtensions\":[],\"maxPackages\":10,\"removeBackups\":1,\"cronMode\":\"none\",\"cycle\":\"everyHour\",\"deadline\":\"\",\"archiveFlush\":\"\",\"useLocalFolder\":1,\"path\":\"\",\"useFTP\":\"\",\"ftpHostname\":\"\",\"ftpUsername\":\"\",\"ftpPassword\":\"\",\"ftpPort\":\"\",\"ftpTimeout\":\"\",\"ftpSSL\":null,\"ftpPassive\":null,\"ftpDirectory\":\"\",\"useDropbox\":\"\",\"dropboxAccessToken\":\"\",\"useGoogleDrive\":\"\",\"googleKeyFile\":\"\",\"shareWithEmail\":\"\",\"useAmazonS3\":\"\",\"awsAccessKey\":\"\",\"awsSecretKey\":\"\",\"awsBucketName\":\"\",\"awsRegion\":\"\"}', '2018-11-15 23:28:47');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('180', 'ProcessDuplicator', '11', '', '2018-11-15 23:30:46');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('181', 'InputfieldPageAutocomplete', '0', '', '2018-12-10 19:13:48');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('182', 'ProcessWireUpgrade', '1', '', '2018-12-10 19:25:22');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('183', 'ProcessWireUpgradeCheck', '11', '', '2018-12-10 19:25:23');

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `templates_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET ascii NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '1',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '2',
  `created` timestamp NOT NULL DEFAULT '2015-12-18 06:09:00',
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '2',
  `published` datetime DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_parent_id` (`name`,`parent_id`),
  KEY `parent_id` (`parent_id`),
  KEY `templates_id` (`templates_id`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `status` (`status`),
  KEY `published` (`published`)
) ENGINE=MyISAM AUTO_INCREMENT=1148 DEFAULT CHARSET=utf8;

INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1', '0', '1', 'home', '9', '2019-03-12 16:02:58', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('2', '1', '2', 'rusich', '1035', '2018-11-15 22:24:22', '40', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '11');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('3', '2', '2', 'page', '21', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('6', '3', '2', 'add', '21', '2018-11-15 22:24:41', '40', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('7', '1', '2', 'trash', '1039', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '12');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('8', '3', '2', 'list', '21', '2018-11-15 22:24:45', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('9', '3', '2', 'sort', '1047', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('10', '3', '2', 'edit', '1045', '2018-11-15 22:24:43', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('11', '22', '2', 'template', '21', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('16', '22', '2', 'field', '21', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('21', '2', '2', 'module', '21', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('22', '2', '2', 'setup', '21', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('23', '2', '2', 'login', '1035', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('27', '1', '29', 'http404', '1035', '2019-03-08 23:40:50', '41', '2018-11-15 22:23:21', '3', '2018-11-15 22:23:21', '10');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('28', '2', '2', 'access', '13', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('29', '28', '2', 'users', '29', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('30', '28', '2', 'roles', '29', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('31', '28', '2', 'permissions', '29', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('32', '31', '5', 'page-edit', '25', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('34', '31', '5', 'page-delete', '25', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('35', '31', '5', 'page-move', '25', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('36', '31', '5', 'page-view', '25', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('37', '30', '4', 'guest', '25', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('38', '30', '4', 'superuser', '25', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('41', '29', '3', 'admin', '1', '2018-11-15 22:42:46', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('40', '29', '3', 'guest', '25', '2018-11-15 22:26:45', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('50', '31', '5', 'page-sort', '25', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('51', '31', '5', 'page-template', '25', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('52', '31', '5', 'user-admin', '25', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '10');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('53', '31', '5', 'profile-edit', '1', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '13');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('54', '31', '5', 'page-lock', '1', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '8');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('300', '3', '2', 'search', '1045', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('301', '3', '2', 'trash', '1047', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('302', '3', '2', 'link', '1041', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '7');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('303', '3', '2', 'image', '1041', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '2', '2018-11-15 22:23:21', '8');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('304', '2', '2', 'profile', '1025', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '41', '2018-11-15 22:23:21', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1006', '31', '5', 'page-lister', '1', '2018-11-15 22:23:21', '40', '2018-11-15 22:23:21', '40', '2018-11-15 22:23:21', '9');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1007', '3', '2', 'lister', '1', '2018-11-15 22:23:21', '40', '2018-11-15 22:23:21', '40', '2018-11-15 22:23:21', '9');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1010', '3', '2', 'recent-pages', '1', '2018-11-15 22:24:21', '40', '2018-11-15 22:24:21', '40', '2018-11-15 22:24:21', '10');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1011', '31', '5', 'page-edit-recent', '1', '2018-11-15 22:24:21', '40', '2018-11-15 22:24:21', '40', '2018-11-15 22:24:21', '10');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1012', '22', '2', 'logs', '1', '2018-11-15 22:24:33', '40', '2018-11-15 22:24:33', '40', '2018-11-15 22:24:33', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1013', '31', '5', 'logs-view', '1', '2018-11-15 22:24:33', '40', '2018-11-15 22:24:33', '40', '2018-11-15 22:24:33', '11');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1014', '31', '5', 'logs-edit', '1', '2018-11-15 22:24:33', '40', '2018-11-15 22:24:33', '40', '2018-11-15 22:24:33', '12');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1015', '2', '2', 'repeaters', '1036', '2018-11-15 22:26:10', '41', '2018-11-15 22:26:10', '41', '2018-11-15 22:26:10', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1016', '31', '5', 'lang-edit', '1', '2018-11-15 22:26:44', '41', '2018-11-15 22:26:44', '41', '2018-11-15 22:26:44', '13');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1017', '22', '2', 'languages', '16', '2018-11-15 22:26:45', '41', '2018-11-15 22:26:45', '41', '2018-11-15 22:26:45', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1018', '1017', '43', 'default', '16', '2018-11-15 22:26:45', '41', '2018-11-15 22:26:45', '41', '2018-11-15 22:26:45', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1019', '22', '2', 'language-translator', '1040', '2018-11-15 22:26:45', '41', '2018-11-15 22:26:45', '41', '2018-11-15 22:26:45', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1020', '22', '2', 'db-backups', '1', '2018-11-15 22:29:52', '41', '2018-11-15 22:29:52', '41', '2018-11-15 22:29:52', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1021', '31', '5', 'db-backup', '1', '2018-11-15 22:29:52', '41', '2018-11-15 22:29:52', '41', '2018-11-15 22:29:52', '14');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1022', '1017', '43', 'rus', '1', '2018-11-15 22:42:27', '41', '2018-11-15 22:41:34', '41', '2018-11-15 22:41:34', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1023', '22', '2', 'duplicator', '1', '2018-11-15 23:30:46', '41', '2018-11-15 23:30:46', '41', '2018-11-15 23:30:46', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1024', '31', '5', 'duplicator', '1', '2018-11-15 23:30:46', '41', '2018-11-15 23:30:46', '41', '2018-11-15 23:30:46', '15');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1025', '1', '44', 'settings', '1025', '2019-03-05 08:15:46', '41', '2018-11-29 11:58:42', '41', '2018-11-29 11:58:50', '9');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1026', '1015', '2', 'for-field-136', '17', '2018-11-29 13:42:01', '41', '2018-11-29 13:42:01', '41', '2018-11-29 13:42:01', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1126', '1117', '29', 'shirokoformatnaia-pechat', '1', '2019-03-08 23:42:38', '41', '2019-03-01 18:16:08', '41', '2019-03-01 18:16:45', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1029', '1', '29', 'comments', '1', '2019-03-12 16:09:27', '41', '2018-11-29 22:21:24', '41', '2018-11-29 22:21:27', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1121', '1117', '29', 'fartuki', '1', '2019-03-08 23:42:11', '41', '2019-02-12 23:31:57', '41', '2019-02-12 23:32:18', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1031', '1', '29', 'o-nas', '1', '2019-03-03 22:11:43', '41', '2018-11-29 22:22:07', '41', '2018-11-29 22:22:10', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1032', '1', '59', 'kontakty', '1', '2019-03-09 00:18:36', '41', '2018-11-29 22:22:21', '41', '2018-11-29 22:22:24', '8');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1122', '1117', '29', 'kartini', '1', '2019-03-08 23:42:03', '41', '2019-02-12 23:32:41', '41', '2019-02-12 23:32:57', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1035', '1015', '2', 'for-field-139', '17', '2018-11-29 23:31:04', '41', '2018-11-29 23:31:04', '41', '2018-11-29 23:31:04', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1036', '1035', '2', 'for-page-1025', '17', '2018-11-29 23:34:20', '41', '2018-11-29 23:34:20', '41', '2018-11-29 23:34:20', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1117', '1', '29', 'uslugi', '1', '2019-03-12 16:19:59', '41', '2019-02-12 23:22:54', '41', '2019-02-12 23:23:30', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1118', '1117', '29', 'ukazateli', '1', '2019-03-08 23:42:19', '41', '2019-02-12 23:23:56', '41', '2019-02-12 23:26:40', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1119', '1117', '29', 'oboi', '1', '2019-03-08 23:41:52', '41', '2019-02-12 23:27:43', '41', '2019-02-12 23:28:16', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1040', '1026', '2', 'for-page-1025', '17', '2018-11-30 08:35:51', '41', '2018-11-30 08:35:51', '41', '2018-11-30 08:35:51', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1041', '1040', '45', '1543556152-071-1', '1', '2019-03-05 08:15:46', '41', '2018-11-30 08:35:52', '41', '2018-11-30 08:52:15', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1045', '1044', '29', 'moduli', '1', '2019-03-08 23:13:38', '41', '2018-11-30 19:26:52', '41', '2018-11-30 19:26:55', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1044', '1025', '29', 'spiski', '1', '2018-11-30 19:26:34', '41', '2018-11-30 19:26:26', '41', '2018-11-30 19:26:34', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1046', '1045', '29', 'carousel', '1', '2018-11-30 19:27:35', '41', '2018-11-30 19:27:31', '41', '2018-11-30 19:27:35', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1047', '1045', '29', 'pub-cards', '1', '2018-12-04 23:51:29', '41', '2018-11-30 19:28:50', '41', '2018-11-30 19:28:54', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1080', '1015', '2', 'for-field-201', '17', '2018-12-07 23:15:37', '41', '2018-12-07 23:15:37', '41', '2018-12-07 23:15:37', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1049', '1045', '29', 'products-carousel', '1', '2018-11-30 19:30:28', '41', '2018-11-30 19:30:26', '41', '2018-11-30 19:30:28', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1050', '1045', '29', 'blog', '1', '2018-11-30 19:30:47', '41', '2018-11-30 19:30:46', '41', '2018-11-30 19:30:47', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1051', '1045', '29', 'content-stepper', '1', '2018-11-30 19:31:25', '41', '2018-11-30 19:31:23', '41', '2018-11-30 19:31:25', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1052', '1045', '29', 'tabs', '1', '2018-12-07 23:28:04', '41', '2018-11-30 19:31:44', '41', '2018-11-30 19:31:50', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1053', '1045', '29', 'gallery', '1', '2018-11-30 19:32:12', '41', '2018-11-30 19:32:08', '41', '2018-11-30 19:32:12', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1054', '1045', '29', 'form', '1', '2018-12-10 19:15:04', '41', '2018-11-30 19:32:35', '41', '2018-11-30 19:32:37', '8');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1055', '1045', '29', 'map', '1', '2018-11-30 19:32:54', '41', '2018-11-30 19:32:52', '41', '2018-11-30 19:32:54', '9');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1056', '1015', '2', 'for-field-154', '17', '2018-12-03 19:11:44', '41', '2018-12-03 19:11:44', '41', '2018-12-03 19:11:44', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1057', '1056', '2', 'for-page-1', '17', '2018-12-03 19:13:47', '41', '2018-12-03 19:13:47', '41', '2018-12-03 19:13:47', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1058', '1057', '47', '1543854056-3396-1', '1', '2019-03-01 00:14:19', '41', '2018-12-03 19:20:56', '41', '2018-12-03 19:22:18', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1059', '1057', '47', '1543854103-2817-1', '1', '2019-03-01 00:14:19', '41', '2018-12-03 19:21:43', '41', '2018-12-03 19:22:18', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1060', '1057', '47', '1543854122-2847-1', '1', '2019-03-01 00:14:19', '41', '2018-12-03 19:22:02', '41', '2018-12-03 19:22:18', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1062', '1045', '29', 'flip-cards', '1', '2018-12-03 23:33:42', '41', '2018-12-03 23:16:17', '41', '2018-12-03 23:16:25', '10');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1063', '1015', '2', 'for-field-155', '17', '2018-12-03 23:18:22', '41', '2018-12-03 23:18:22', '41', '2018-12-03 23:18:22', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1064', '1063', '2', 'for-page-1', '17', '2018-12-03 23:21:46', '41', '2018-12-03 23:21:46', '41', '2018-12-03 23:21:46', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1065', '1064', '48', '1543868548-4667-1', '1', '2019-03-05 23:15:12', '41', '2018-12-03 23:22:28', '41', '2018-12-03 23:25:24', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1120', '1117', '29', 'naruznaya-reklama', '1', '2019-03-08 23:42:29', '41', '2019-02-12 23:28:53', '41', '2019-02-12 23:28:55', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1067', '1064', '48', '1543868679-3168-1', '1', '2019-03-05 23:15:12', '41', '2018-12-03 23:24:39', '41', '2018-12-03 23:25:24', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1127', '1117', '29', 'poligrafiia', '1', '2019-03-08 23:42:46', '41', '2019-03-01 18:20:11', '41', '2019-03-01 18:21:34', '7');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1072', '1045', '29', 'page-preview', '1', '2018-12-05 19:06:11', '41', '2018-12-05 19:02:47', '41', '2018-12-05 19:03:13', '11');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1073', '1045', '29', 'static-cards', '1', '2018-12-05 23:09:55', '41', '2018-12-05 23:09:46', '41', '2018-12-05 23:09:55', '12');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1074', '1015', '2', 'for-field-184', '17', '2018-12-05 23:21:35', '41', '2018-12-05 23:21:35', '41', '2018-12-05 23:21:35', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1075', '1074', '2', 'for-page-1', '17', '2018-12-05 23:49:23', '41', '2018-12-05 23:49:23', '41', '2018-12-05 23:49:23', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1076', '1075', '49', '1544042964-3741-1', '1', '2018-12-06 09:05:34', '41', '2018-12-05 23:49:24', '41', '2018-12-05 23:58:52', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1077', '1075', '49', '1544076223-9492-1', '1', '2018-12-06 23:27:19', '41', '2018-12-06 09:03:43', '41', '2018-12-06 09:05:34', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1078', '1075', '49', '1544076292-3001-1', '1', '2018-12-06 23:27:19', '41', '2018-12-06 09:04:52', '41', '2018-12-06 09:05:34', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1079', '1057', '47', '1544130768-1084-1', '1', '2019-03-01 00:14:19', '41', '2018-12-07 00:12:48', '41', '2018-12-07 00:13:22', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1081', '1080', '2', 'for-page-1', '17', '2018-12-07 23:24:55', '41', '2018-12-07 23:24:55', '41', '2018-12-07 23:24:55', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1082', '1081', '50', '1544214347-1442-1', '1', '2019-01-16 23:51:51', '41', '2018-12-07 23:25:47', '41', '2018-12-07 23:26:30', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1083', '1081', '50', '1544214368-9535-1', '1', '2019-01-16 23:51:51', '41', '2018-12-07 23:26:08', '41', '2018-12-07 23:26:30', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1084', '1081', '50', '1544214376-4979-1', '1', '2019-01-16 23:51:51', '41', '2018-12-07 23:26:16', '41', '2018-12-07 23:26:30', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1085', '1025', '29', 'forms', '1', '2018-12-09 22:44:39', '41', '2018-12-09 22:44:30', '41', '2018-12-09 22:44:39', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1086', '1085', '51', 'contacts', '1', '2019-03-01 17:37:21', '41', '2018-12-09 22:45:26', '41', '2018-12-09 22:45:32', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1087', '1015', '2', 'for-field-236', '17', '2018-12-09 23:27:49', '41', '2018-12-09 23:27:49', '41', '2018-12-09 23:27:49', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1088', '1044', '29', 'inputs', '1', '2018-12-09 23:46:23', '41', '2018-12-09 23:46:19', '41', '2018-12-09 23:46:23', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1089', '1088', '29', 'text', '1', '2018-12-09 23:46:40', '41', '2018-12-09 23:46:37', '41', '2018-12-09 23:46:40', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1090', '1088', '29', 'password', '1', '2018-12-09 23:46:56', '41', '2018-12-09 23:46:52', '41', '2018-12-09 23:46:56', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1091', '1088', '29', 'date', '1', '2018-12-09 23:47:18', '41', '2018-12-09 23:47:15', '41', '2018-12-09 23:47:18', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1092', '1088', '29', 'datetime', '1', '2018-12-09 23:47:34', '41', '2018-12-09 23:47:31', '41', '2018-12-09 23:47:34', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1093', '1088', '29', 'email', '1', '2018-12-09 23:47:54', '41', '2018-12-09 23:47:52', '41', '2018-12-09 23:47:54', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1094', '1088', '29', 'number', '1', '2018-12-09 23:48:11', '41', '2018-12-09 23:48:09', '41', '2018-12-09 23:48:11', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1095', '1088', '29', 'range', '1', '2018-12-09 23:48:30', '41', '2018-12-09 23:48:27', '41', '2018-12-09 23:48:30', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1096', '1088', '29', 'tel', '1', '2018-12-09 23:48:51', '41', '2018-12-09 23:48:48', '41', '2018-12-09 23:48:51', '7');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1097', '1086', '52', 'name', '1', '2018-12-20 22:58:28', '41', '2018-12-10 00:00:49', '41', '2018-12-10 00:02:51', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1098', '1086', '52', 'email', '1', '2018-12-20 22:58:44', '41', '2018-12-10 00:03:23', '41', '2018-12-10 00:04:48', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1104', '1086', '53', 'message', '1', '2019-02-26 23:33:54', '41', '2018-12-10 00:08:28', '41', '2018-12-10 00:09:11', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1105', '1086', '55', 'agreement', '1', '2019-03-08 23:33:44', '41', '2018-12-10 19:09:30', '41', '2018-12-10 19:10:33', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1106', '22', '2', 'upgrades', '1', '2018-12-10 19:25:22', '41', '2018-12-10 19:25:22', '41', '2018-12-10 19:25:22', '7');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1108', '1088', '29', 'select', '1', '2018-12-17 10:17:22', '41', '2018-12-17 10:17:17', '41', '2018-12-17 10:17:22', '8');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1109', '1088', '29', 'textarea', '1', '2018-12-20 08:38:29', '41', '2018-12-20 08:38:25', '41', '2018-12-20 08:38:29', '9');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1110', '1088', '29', 'checkbox', '1', '2018-12-20 08:38:46', '41', '2018-12-20 08:38:42', '41', '2018-12-20 08:38:46', '10');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1111', '1025', '58', 'mail', '1', '2018-12-20 23:00:35', '41', '2018-12-20 23:00:23', '41', '2018-12-20 23:00:32', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1112', '1064', '48', '1547671410-3801-1', '1', '2019-03-05 23:15:12', '41', '2019-01-16 23:43:30', '41', '2019-01-16 23:46:35', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1113', '1064', '48', '1547671506-2017-1', '1', '2019-03-05 23:15:12', '41', '2019-01-16 23:45:06', '41', '2019-01-16 23:46:35', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1114', '1064', '48', '1547671540-4344-1', '1', '2019-03-05 23:15:12', '41', '2019-01-16 23:45:40', '41', '2019-01-16 23:46:35', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1116', '1', '29', 'portfolio', '1', '2019-03-09 00:17:16', '41', '2019-01-16 23:56:23', '41', '2019-01-16 23:56:25', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1125', '1117', '29', 'art-ob-ekty', '1', '2019-03-08 23:41:35', '41', '2019-03-01 18:09:16', '41', '2019-03-01 18:14:34', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1128', '1117', '29', 'brendirovanie-avtomobilei', '1', '2019-03-08 23:42:54', '41', '2019-03-01 18:21:48', '41', '2019-03-01 18:22:13', '8');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1129', '1064', '48', '1551454061-6191-1', '1', '2019-03-05 23:15:12', '41', '2019-03-01 18:27:41', '41', '2019-03-01 18:34:08', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1130', '1064', '48', '1551454102-0022-1', '1', '2019-03-05 23:15:12', '41', '2019-03-01 18:28:22', '41', '2019-03-01 18:34:08', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1131', '1064', '48', '1551454142-8288-1', '1', '2019-03-05 23:15:12', '41', '2019-03-01 18:29:02', '41', '2019-03-01 18:34:08', '7');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1132', '1064', '48', '1551454402-215-1', '1', '2019-03-05 23:15:12', '41', '2019-03-01 18:33:22', '41', '2019-03-01 18:34:08', '8');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1133', '1045', '29', 'video', '1', '2019-03-03 22:19:31', '41', '2019-03-03 22:19:28', '41', '2019-03-03 22:19:31', '13');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1134', '1029', '29', 'otzyv-ot-klienta-1', '1', '2019-03-12 16:04:31', '41', '2019-03-05 08:02:49', '41', '2019-03-05 08:04:35', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1135', '1029', '29', 'otzyv-s-bol-shim-chislom-slov', '1', '2019-03-12 16:13:43', '41', '2019-03-05 08:05:23', '41', '2019-03-05 08:05:33', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1136', '1029', '29', 'otzyv-s-perekhodom-na-stranitcu-otzyva', '1', '2019-03-05 23:05:19', '41', '2019-03-05 08:06:00', '41', '2019-03-05 08:06:09', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1137', '7', '29', '1137.1029.3_otzyv-s-perekhodom-prev-iu-i-failami', '8193', '2019-03-05 23:05:29', '41', '2019-03-05 08:06:37', '41', '2019-03-05 08:07:03', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1138', '1015', '2', 'for-field-275', '17', '2019-03-05 23:37:22', '41', '2019-03-05 23:37:22', '41', '2019-03-05 23:37:22', '7');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1139', '1138', '2', 'for-page-1032', '17', '2019-03-05 23:46:24', '41', '2019-03-05 23:46:24', '41', '2019-03-05 23:46:24', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1140', '1139', '60', '1551818788-8215-1', '1', '2019-03-06 00:00:38', '41', '2019-03-05 23:46:28', '41', '2019-03-05 23:51:19', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1144', '1139', '60', '1551819297-9504-1', '1', '2019-03-05 23:59:16', '41', '2019-03-05 23:54:57', '41', '2019-03-05 23:59:16', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1141', '1139', '60', '1551819089-009-1', '1', '2019-03-06 00:00:38', '41', '2019-03-05 23:51:29', '41', '2019-03-05 23:54:18', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1142', '1139', '60', '1551819140-2321-1', '1', '2019-03-05 23:56:27', '41', '2019-03-05 23:52:20', '41', '2019-03-05 23:56:27', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1143', '1139', '60', '1551819228-7198-1', '1', '2019-03-06 00:00:38', '41', '2019-03-05 23:53:48', '41', '2019-03-05 23:54:18', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1145', '1139', '60', '1551819354-9159-1', '1', '2019-03-06 00:00:38', '41', '2019-03-05 23:55:54', '41', '2019-03-05 23:56:27', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1146', '1045', '29', 'gallery2', '1', '2019-03-08 23:13:38', '41', '2019-03-08 23:13:26', '41', '2019-03-08 23:13:29', '7');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1147', '1', '29', 'politika-konfidetcialnosti', '1', '2019-03-08 23:34:11', '41', '2019-03-08 23:30:21', '41', '2019-03-08 23:30:39', '9');

DROP TABLE IF EXISTS `pages_access`;
CREATE TABLE `pages_access` (
  `pages_id` int(11) NOT NULL,
  `templates_id` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pages_id`),
  KEY `templates_id` (`templates_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('37', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('38', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('32', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('34', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('35', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('36', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('50', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('51', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('52', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('53', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('54', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1006', '2', '2018-11-15 22:23:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1011', '2', '2018-11-15 22:24:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1013', '2', '2018-11-15 22:24:33');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1014', '2', '2018-11-15 22:24:33');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1016', '2', '2018-11-15 22:26:44');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1018', '2', '2018-11-15 22:26:45');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1021', '2', '2018-11-15 22:29:52');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1022', '2', '2018-11-15 22:41:35');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1024', '2', '2018-11-15 23:30:46');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1025', '1', '2018-11-29 11:58:42');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1125', '1', '2019-03-01 18:09:16');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1029', '1', '2018-11-29 22:21:24');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1116', '1', '2019-01-16 23:56:23');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1031', '1', '2018-11-29 22:22:07');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1032', '1', '2018-11-29 22:22:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1122', '1', '2019-02-12 23:32:41');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1117', '1', '2019-02-12 23:22:54');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1119', '1', '2019-02-12 23:27:43');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1118', '1', '2019-02-12 23:23:56');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1041', '2', '2018-11-30 08:35:52');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1045', '1', '2018-11-30 19:26:52');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1044', '1', '2018-11-30 19:26:26');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1046', '1', '2018-11-30 19:27:31');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1047', '1', '2018-11-30 19:28:50');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1079', '2', '2018-12-07 00:12:48');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1049', '1', '2018-11-30 19:30:26');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1050', '1', '2018-11-30 19:30:46');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1051', '1', '2018-11-30 19:31:23');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1052', '1', '2018-11-30 19:31:44');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1053', '1', '2018-11-30 19:32:08');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1054', '1', '2018-11-30 19:32:35');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1055', '1', '2018-11-30 19:32:52');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1058', '2', '2018-12-03 19:20:56');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1059', '2', '2018-12-03 19:21:43');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1060', '2', '2018-12-03 19:22:02');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1062', '1', '2018-12-03 23:16:17');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1065', '2', '2018-12-03 23:22:28');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1120', '1', '2019-02-12 23:28:53');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1067', '2', '2018-12-03 23:24:39');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1127', '1', '2019-03-01 18:20:11');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1126', '1', '2019-03-01 18:16:08');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1072', '1', '2018-12-05 19:06:11');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1073', '1', '2018-12-05 23:09:46');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1076', '2', '2018-12-05 23:49:24');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1077', '2', '2018-12-06 09:03:43');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1078', '2', '2018-12-06 09:04:52');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1082', '2', '2018-12-07 23:25:47');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1083', '2', '2018-12-07 23:26:08');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1084', '2', '2018-12-07 23:26:16');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1085', '1', '2018-12-09 22:44:30');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1086', '1', '2018-12-09 22:45:26');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1088', '1', '2018-12-09 23:46:19');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1089', '1', '2018-12-09 23:46:37');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1090', '1', '2018-12-09 23:46:52');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1091', '1', '2018-12-09 23:47:15');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1092', '1', '2018-12-09 23:47:31');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1093', '1', '2018-12-09 23:47:52');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1094', '1', '2018-12-09 23:48:09');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1095', '1', '2018-12-09 23:48:27');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1096', '1', '2018-12-09 23:48:48');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1097', '1', '2018-12-10 00:00:49');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1098', '1', '2018-12-10 00:03:23');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1104', '1', '2018-12-10 00:08:28');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1105', '1', '2018-12-10 19:09:30');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1108', '1', '2018-12-17 10:17:17');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1109', '1', '2018-12-20 08:38:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1110', '1', '2018-12-20 08:38:42');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1111', '1', '2018-12-20 23:00:23');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1112', '2', '2019-01-16 23:43:30');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1113', '2', '2019-01-16 23:45:06');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1114', '2', '2019-01-16 23:45:40');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1121', '1', '2019-02-12 23:31:57');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1128', '1', '2019-03-01 18:21:48');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1129', '2', '2019-03-01 18:27:41');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1130', '2', '2019-03-01 18:28:22');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1131', '2', '2019-03-01 18:29:02');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1132', '2', '2019-03-01 18:33:22');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1133', '1', '2019-03-03 22:19:28');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1134', '1', '2019-03-05 08:02:49');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1135', '1', '2019-03-05 08:05:23');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1136', '1', '2019-03-05 08:06:00');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1137', '2', '2019-03-05 23:05:29');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1140', '2', '2019-03-05 23:46:28');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1141', '2', '2019-03-05 23:51:29');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1142', '2', '2019-03-05 23:52:20');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1143', '2', '2019-03-05 23:53:48');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1144', '2', '2019-03-05 23:54:57');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1145', '2', '2019-03-05 23:55:54');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1146', '1', '2019-03-08 23:13:26');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1147', '1', '2019-03-08 23:30:21');

DROP TABLE IF EXISTS `pages_parents`;
CREATE TABLE `pages_parents` (
  `pages_id` int(10) unsigned NOT NULL,
  `parents_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`parents_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('2', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('3', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('3', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('22', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('22', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('28', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('28', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1015', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1017', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1017', '22');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1026', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1026', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1035', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1035', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1036', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1036', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1036', '1035');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1040', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1040', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1040', '1026');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1044', '1025');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1045', '1025');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1045', '1044');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1056', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1056', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1057', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1057', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1057', '1056');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1063', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1063', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1064', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1064', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1064', '1063');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1074', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1074', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1075', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1075', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1075', '1074');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1080', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1080', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1081', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1081', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1081', '1080');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1085', '1025');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1086', '1025');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1086', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1087', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1087', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1088', '1025');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1088', '1044');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1138', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1138', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1139', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1139', '1015');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1139', '1138');

DROP TABLE IF EXISTS `pages_sortfields`;
CREATE TABLE `pages_sortfields` (
  `pages_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sortfield` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `session_login_throttle`;
CREATE TABLE `session_login_throttle` (
  `name` varchar(128) NOT NULL,
  `attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `last_attempt` int(10) unsigned NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `session_login_throttle` (`name`, `attempts`, `last_attempt`) VALUES('admin', '1', '1552417684');

DROP TABLE IF EXISTS `templates`;
CREATE TABLE `templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  `fieldgroups_id` int(10) unsigned NOT NULL DEFAULT '0',
  `flags` int(11) NOT NULL DEFAULT '0',
  `cache_time` mediumint(9) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fieldgroups_id` (`fieldgroups_id`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('2', 'admin', '2', '8', '0', '{\"useRoles\":1,\"parentTemplates\":[2],\"allowPageNum\":1,\"redirectLogin\":23,\"slashUrls\":1,\"noGlobal\":1,\"compile\":3,\"modified\":1549827827,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('3', 'user', '3', '8', '0', '{\"useRoles\":1,\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"User\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('4', 'role', '4', '8', '0', '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Role\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('5', 'permission', '5', '8', '0', '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"guestSearchable\":1,\"pageClass\":\"Permission\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('1', 'home', '1', '0', '0', '{\"useRoles\":1,\"noParents\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-home title\",\"compile\":3,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"modified\":1552076321,\"ns\":\"\\\\\",\"roles\":[37]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('29', 'basic-page', '83', '0', '0', '{\"allowPageNum\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-sticky-note-o title\",\"compile\":3,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"modified\":1552078256,\"ns\":\"\\\\\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('43', 'language', '97', '8', '0', '{\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Language\",\"pageLabelField\":\"name\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noChangeTemplate\":1,\"noUnpublish\":1,\"compile\":3,\"nameContentTab\":1,\"modified\":1542310005}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('44', 'settings', '98', '0', '0', '{\"noParents\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-wrench title\",\"compile\":3,\"tags\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438\",\"modified\":1549827827,\"ns\":\"\\\\\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('45', 'repeater_col_generator', '99', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1543488121}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('46', 'repeater_socialls_list', '100', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1543523464}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('47', 'repeater_carousel_repeater', '101', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1543853504}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('48', 'repeater_flip_cards_repeater', '102', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1543869647}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('49', 'repeater_static_cards_repeator', '103', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1544041295}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('50', 'repeater_tabs_repeater', '104', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1544213737}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('51', 'form', '105', '0', '0', '{\"childTemplates\":[55,56,54,52,53],\"slashUrls\":1,\"pageLabelField\":\"fa-envelope-o title\",\"compile\":3,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"modified\":1544472653}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('52', 'field_text', '106', '0', '0', '{\"noChildren\":1,\"parentTemplates\":[51],\"slashUrls\":1,\"pageLabelField\":\"fa-envelope-o title\",\"compile\":3,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"modified\":1544388788}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('53', 'field_textarea', '107', '0', '0', '{\"noChildren\":1,\"parentTemplates\":[51],\"slashUrls\":1,\"pageLabelField\":\"fa-envelope-o title\",\"compile\":3,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"modified\":1545284248}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('54', 'field_select', '108', '0', '0', '{\"noChildren\":1,\"parentTemplates\":[51],\"slashUrls\":1,\"pageLabelField\":\"fa-envelope-o title\",\"compile\":3,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"modified\":1545031073}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('55', 'field_checkbox', '109', '0', '0', '{\"noChildren\":1,\"parentTemplates\":[51],\"slashUrls\":1,\"pageLabelField\":\"fa-envelope-o title\",\"compile\":3,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"modified\":1545284360}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('56', 'field_radio', '110', '0', '0', '{\"noChildren\":1,\"parentTemplates\":[51],\"slashUrls\":1,\"pageLabelField\":\"fa-envelope-o title\",\"compile\":3,\"tags\":\"\\u0424\\u043e\\u0440\\u043c\\u044b\",\"modified\":1544388844}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('57', 'repeater_field_options', '111', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1544387269}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('58', 'mail', '112', '0', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"compile\":3,\"tags\":\"\\u041d\\u0430\\u0441\\u0442\\u0440\\u043e\\u0439\\u043a\\u0438\",\"modified\":1549827827,\"noAppendTemplateFile\":1,\"ns\":\"\\\\\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('59', 'contacts', '113', '0', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-address-card-o title\",\"compile\":3,\"tags\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u044b\",\"modified\":1551900987,\"ns\":\"\\\\\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('60', 'repeater_contacts_fields', '114', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1551818242}');

# --- /WireDatabaseBackup {"numTables":210,"numCreateTables":210,"numInserts":1889,"numSeconds":0}