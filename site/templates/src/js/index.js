import '../sass/style.scss';
import $ from 'jquery';
import './modules/mdb/popper.min';
import './modules/mdb/bootstrap';
import './modules/mdb/jarallax';
import './modules/mdb/mdb';
import Vue from 'vue/dist/vue';
import axios from 'axios';
import VueTheMask from 'vue-the-mask';
// import Test from './modules/test';
import 'lightbox2';

const { yandexMap, ymapMarker } = require('vue-yandex-maps');
Vue.use(VueTheMask);

new Vue({
  components: { yandexMap, ymapMarker, axios },
  el: '#site',
  data() {
    return {
      mapCenter: window.map_center,
      mapPoint: window.map_point,
      mapZoom: window.map_zoom,
      mapDesc: window.map_desc,
      mapDrag: window.map_drag ? 'drag' : '',
      mapScroll: window.map_scroll ? 'scrollZoom' : '',
      mapIcon: {color: 'red', glyph: 'home'},
      form: {},
    };
  },
  methods: {
    getFields() {
      let fields = [];
      for(let key in this.form) {
        if (key.indexOf("field_") !== -1) {
          fields.push(this.form[key]);
        }
      }
      return fields;
    },
    formValidation() {
      let error = [];
      const fields = this.getFields();
      for(let i = 0; i < fields.length; i++) {
        if (!fields[i].fieldValue) {
          error.push(fields[i]);
        }
      }
      return !error.length;
    },
    clearForm(form) {
      for (var key in form) {
        if (key.indexOf("field_") !== -1 ) {
          form[key].fieldValue = '';
        }
      }
    },
    sendForm() {
      const app = this;
      const fields = this.getFields().map(item => {
          return {
            fieldError: item.fieldError,
            fieldID: item.fieldID,
            fieldTitle: item.fieldTitle,
            fieldName: item.fieldName,
            fieldRequired: item.fieldRequired,
            fieldType: item.fieldType,
            fieldValue: item.fieldValue,
          };
        }
      );
      const form = {
        formId: app.form.id,
        formEmail: app.form.email,
        formTheme: app.form.theme,
        fields: fields,
        formSuccess: app.formSuccess,
        formError: app.formError,
      };

      $.post("/settings/mail/", form)
        .done((data) => {
          // this.clearForm(this.form);
          // this.form.modal.class = 'modal-info';
          this.form.modal.message = this.form.success;
          $('#formModal').modal('show');
        })
        .fail((error) => {
          console.log('status:', error.status, ', text: ', error.statusText);
          // this.form.modal.class = 'modal-danger';
          this.form.modal.message = this.form.error;
          $('#formModal').modal('show');
        })
        .always(() => {
          // console.log('allways');
        });
    },
  },
  mounted() {
    this.form = window.form || {};
  }
});

$(function() {
  $('body').addClass('skobar');
  jarallax(document.querySelectorAll('.jarallax'));
});

// const someFoo = (varik) => {console.log(varik);}
// someFoo(Test.data);

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

/** video */
// const video = $("#video");
// const min_width = 768;
// const section_video = $('#video-section');
// const video_url = video.data('video');
//
// function getVideo() {
//   if ($(window).width() > min_width) {
//     video.html(`<video class="video__player" width="100%" height="auto" autoplay="autoplay" loop="loop"><source src="${video_url}" type="video/mp4"></video>`);
//   }
// }
//
// function changeSection() {
//   if ($(window).width() > min_width) {
//     section_video.height($(window).height());
//   } else {
//     section_video.removeAttr('style');
//   }
// }
//
// getVideo();
// changeSection();
//
// $(window).resize(function(){
//   changeSection();
// });
