<?php if (($page->title && $page->body) || $page->carousel_image_title) : ?>
  <section
    class="<?= $page->section_parallax ? 'jarallax ' : '' ?><?= $page->section_classes ? $page->section_classes : 'pub-cards grey lighten-3 pt-5 pb-5' ?>"
    style='<?= ($page->section_image->url && !$page->section_parallax) ? "background-image: url({$page->section_image->url})" : '' ?>'
  >
    <?php if ($page->section_image->url && $page->section_parallax): ?>
      <img class="jarallax-img" src="<?= $page->section_image->url ?>" alt="">
    <?php endif; ?>
    <div class="container dev">
      <div class="row">
        <div class="col-md-12">
          <h1 class="<?= $page->section_title_classes ? $page->section_title_classes : 'text-uppercase' ?>">
            <?= $page->carousel_image_title ? $page->carousel_image_title : $page->title ?>
          </h1>
        </div>
        <?php if ($page->body) : ?>
          <div class="col-md-12">
            <?= htmlspecialchars_decode($page->body) ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
<?php endif; ?>
