<section
  class="<?= $page->section_parallax ? 'jarallax ' : '' ?><?= $page->section_classes ? ' page-block '.$page->section_classes : ' page-block' ?>"
  style='<?= ($page->section_image->url && !$page->section_parallax) ? "background-image: url({$page->section_image->url})" : '' ?>'
>
  <?php if ($page->section_image->url && $page->section_parallax): ?>
    <img class="jarallax-img" src="<?= $page->section_image->url ?>" alt="">
  <?php endif; ?>
  <div class="container <?=$page->name?>">
    <div class="row">
      <div class="col-md-12 pt-5">
        <h1 class="<?= $page->section_title_classes ? 'text-uppercase '.$page->section_title_classes : 'text-uppercase' ?>">
          <?= $page->title ?>
        </h1>
      </div>
        <?php if ($page->image->url): ?>
        <div class="col-lg-4 pb-3">
          <img src="<?=$page->image->url?>" alt="<?= $page->title ?>">
        </div>
        <div class="col-lg-8">
        <?php else: ?>
        <div class="col-lg-12">
        <?php endif; ?>
        <?= $page->body ? htmlspecialchars_decode($page->body) : '' ?>

        <?php if ($page->contacts_fields->count) : ?>
        <div itemscope="" itemtype="http://schema.org/Organization">
          <meta itemprop="name" content="<?= $home->string?>">
            <table class="table contact-table">
              <tbody>
            <?php foreach ($page->contacts_fields as $item): ?>
                <tr>
                  <th scope="row"><?= $item->field_label?></th>
                  <td>
                    <?php if ($item->links) : ?>
                    <a href="<?= $item->links?>">
                    <?php endif; ?>
                      <?php if ($item->author == 'streetAddress') : ?>
                        <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                      <?php endif; ?>
                          <span<?= $item->author ? " itemprop='{$item->author}'" : ''?>><?= $item->string?></span>
                      <?php if ($item->author == 'streetAddress') : ?>
                        </span>
                      <?php endif; ?>
                    <?php if ($item->links) : ?>
                    </a>
                    <?php endif; ?>
                  </td>
                </tr>
            <?php endforeach; ?>
              </tbody>
            </table>
        </div>
        <?php endif; ?>
        </div>

        <?php if ($page->images->count): ?>
          <section class="col-sm-12<?= $page->images_sections_classes ? " {$page->images_sections_classes}" : ''?>">
            <div class="row">
              <div class="col-sm-12">
                <?php if ($page->images_title): ?>
                  <h3><?= $page->images_title?></h3>
                <?php else: ?>
                  <h3>Галерея</h3>
                <?php endif; ?>
              </div>
              <?php foreach ($page->images as $item): ?>
                <figure class="col-sm-6 col-md-4 col-xl-3">
                  <a class="view overlay zoom" href="<?= $item->url ?>" data-size="1920x1080"
                     data-lightbox="<?= $item->name ?>" title="<?= $item->description ? $item->description : '' ?>">
                    <img class="img-fluid" src="<?= $item->size(800, 800)->url ?>" alt="gallery">
                    <?php if ($item->description): ?>
                      <figcaption class="figure-caption"><?= $item->description ?></figcaption>
                    <?php endif; ?>
                  </a>
                </figure>
              <?php endforeach; ?>
            </div>
          </section>
        <?php endif; ?>
      </div>

    </div>
    <?php if ($page->map_center) : ?>
      <section class="map pt-5">
        <yandex-map
          :coords="mapCenter"
          :zoom="mapZoom"
          style="width: 100%; height: 600px;"
          :cluster-options="{1: {clusterDisableClickZoom: false}}"
          :behaviors="['ruler', mapDrag, mapScroll]"
          :controls="['zoomControl']"
          map-type="map"
        >
          <ymap-marker
            marker-id="1",
            marker-type="placemark"
            :coords="mapPoint"
            :hint-content="mapDesc"
            :icon="mapIcon"
          ></ymap-marker>
        </yandex-map>
      </section>
    <?php endif; ?>
</section>

