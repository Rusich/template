<!-- carousel section-->
<?php
/**
 * carousel_id - уникальный ID карусели
 * carousel_classes - лассы оболочки карусели
 * carousel_images - массив изображений
 * carousel_prew_icon - класс иконки "назад"
 * carousel_next_icon - класс иконки "вперёд"
 */
?>
<?php if ($page->carousel_images->count || $page->carousel_repeater->count): ?>
  <section class="carousel">
    <div
      class="carousel slide<?= ($page->carousel_classes) ? ' ' . $page->carousel_classes : ' carousel-fade w-100 h-50' ?>"
      id="<?= $page->carousel_id ?>"
      data-ride="carousel"
    >
      <ol class="carousel-indicators">
        <?php
        $count = 0;
        if ($page->carousel_images->count && !$page->carousel_repeater->count) :
          foreach ($page->carousel_images as $indicator) :
            ?>
            <li <?= $count === 0 ? 'class="active"' : '' ?> data-target="#<?= $page->carousel_id ?>"
                                                            data-slide-to="<?= $count ?>"></li>
            <?php
            $count++;
          endforeach;
        else :
          foreach ($page->carousel_repeater as $indicator) :
            ?>
            <li <?= $count === 0 ? 'class="active"' : '' ?> data-target="#<?= $page->carousel_id ?>"
                                                            data-slide-to="<?= $count ?>"></li>
            <?php
            $count++;
          endforeach;
        endif;
        ?>
      </ol>
      <div class="carousel-inner" role="listbox">

        <?php
        $count = 0;
        if ($page->carousel_images->count && !$page->carousel_repeater->count) :
          foreach ($page->carousel_images as $img) :
            ?>
            <div class="carousel-item<?= $count === 0 ? ' active' : '' ?>">
              <div class="view">
                <img class="carousel__image d-block w-100" src="<?= $img->url ?>" alt="<?= $img->description || '' ?>">
                <div class="mask rgba-black-light"></div>
              </div>
              <?php if ($img->description) : ?>
                <div class="carousel-caption">
                  <h3 class="h3-responsive"><?= $img->description ?></h3>
                </div>
              <?php endif; ?>
            </div>
            <?php
            $count++;
          endforeach;
        else :
          foreach ($page->carousel_repeater as $item) :
            ?>
            <div class="carousel-item<?= $count === 0 ? ' active' : '' ?>">
              <?php if($item->links) : ?>
              <a class="carousel__link" href="<?=$item->links?>">
              <?php endif; ?>
              <div class="view">
                <img class="carousel__image d-block w-100" src="<?= $item->image->url ?>" alt="<?= $item->title ?>">
                <div class="mask rgba-black-light"></div>
              </div>
              <div class="carousel-caption">
                <h3 class="h3-responsive"><?= $item->title ?></h3>
                <?= htmlspecialchars_decode($item->body) ?>
              </div>
              <?php if($item->links) : ?>
              </a>
              <?php endif; ?>
            </div>
            <?php
            $count++;
          endforeach;
        endif;
        ?>

      </div>
      <a class="carousel-control-prev" href="#<?= $page->carousel_id ?>" role="button" data-slide="prev">
        <i
          class="<?= ($page->carousel_prew_icon) ? $page->carousel_prew_icon : 'fal fa-chevron-left custom-size' ?>"></i>
      </a>
      <a class="carousel-control-next" href="#<?= $page->carousel_id ?>" role="button" data-slide="next">
        <i
          class="<?= ($page->carousel_next_icon) ? $page->carousel_next_icon : 'fal fa-chevron-right custom-size' ?>"></i>
      </a>
    </div>
  </section>
<?php endif; ?>
<!-- /carousel section-->
