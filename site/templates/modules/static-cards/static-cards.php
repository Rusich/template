<!-- static-cards-->
<section
  class="<?= $page->static_cards_section_parallax ? 'jarallax ' : '' ?><?= $page->static_cards_section_classes ? $page->static_cards_section_classes : 'static-cards grey lighten-1' ?>"
  style='<?= ($page->static_cards_section_image->url && !$page->static_cards_section_parallax) ? "background-image: url({$page->static_cards_section_image->url})" : '' ?>'
>
  <?php if ($page->static_cards_section_image->url && $page->static_cards_section_parallax): ?>
    <img class="jarallax-img" src="<?= $page->static_cards_section_image->url ?>" alt="">
  <?php endif; ?>
  <div class="container pt-5 pb-5">
    <div class="row">
      <?php if ($page->static_cards_title): ?>
      <div class="col-sm-12 pb-3 text-center">
        <h2 class="<?= $page->static_cards_title_classes ? $page->static_cards_title_classes : 'text-uppercase' ?>">
          <?= $page->static_cards_title ?>
        </h2>
      </div>
      <?php endif; ?>
      <?php if ($page->static_cards_repeator->count): ?>
        <div class="col-sm-12 clearfix">
          <div class="row justify-content-center">
            <?php foreach ($page->static_cards_repeator as $item) : ?>
              <div class="<?= $item->pub_card_classes ? $item->pub_card_classes : 'col-md-6 col-lg-4 mb-4' ?>">
                <div
                  <?php if ($item->image->url): ?>
                    class="card card-image"
                    style="background-image: url(<?= $item->image->size(800, 800)->url ?>);"
                  <?php else: ?>
                    class="card"
                  <?php endif; ?>
                >
                  <div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4 rounded">
                    <div>
                      <h5 class="pink-text">
                        <i
                          class="<?= $item->string ? $item->string : 'far fa-book-open' ?>"> <?= $item->static_cards_subtitle ?></i>
                      </h5>
                      <h3 class="card-title pt-2"><strong><?= $item->title ?></strong></h3>
                      <?= htmlspecialchars_decode($item->body) ?>
                      <a href="<?= $item->links ?>" class="btn btn-elegant"><i class="fal fa-clone left"> Подробнее</i></a>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>
<!-- /static-cards-->
