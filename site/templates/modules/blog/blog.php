<!-- blog-->
<section
  class="<?= $page->blog_section_parallax ? 'jarallax ' : '' ?><?= $page->blog_section_classes ? $page->blog_section_classes : 'pub-cards grey lighten-3 pt-5 pb-5' ?>"
  style='<?= ($page->blog_section_image->url && !$page->blog_section_parallax) ? "background-image: url({$page->blog_section_image->url})" : '' ?>'
>
  <?php if ($page->blog_section_image->url && $page->blog_section_parallax): ?>
    <img class="jarallax-img" src="<?= $page->blog_section_image->url ?>" alt="">
  <?php endif; ?>
  <div class="container blog pt-5">
    <div class="row">
      <?php if ($page->blog_title) : ?>
        <div class="col-sm-12">
          <h2 class="<?= $page->blog_title_classes ? $page->blog_title_classes : 'text-uppercase' ?>">
            <?= $page->blog_title ?>
          </h2>
        </div>
      <?php endif; ?>

      <?php if ($page->blog_parent->children()->count) : ?>
      <?php foreach ($page->blog_parent->children('limit=' . $page->blog_max) as $item): ?>
      <div class="mx-3 px-3">
        <div class="row rgba-white-stronger pt-3 mb-5 rounded z-depth-2">
          <?php if ($item->image->url): ?>
          <div class="col-lg-5 col-xl-4 mb-3">
            <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4">
              <img class="img-fluid" alt="Sample image" src="<?= $item->image->size(800, 600)->url ?>">
              <a href="<?= $item->url ?>">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
          </div>
          <?php endif; ?>
          <div class="<?= $item->image->url ? 'col-lg-7 col-xl-8' : 'col-sm-12'?>">
            <h4 class="font-weight-bold mb-3"><strong><?= $item->title ?></strong></h4>
            <?= htmlspecialchars_decode($item->short_description) ?>
            <p>
              <?php if ($item->author): ?>
                Автор: <span class="font-weight-bold"><?= $item->author ?></span>&nbsp;
              <?php endif; ?>
              <span class="badge badge-light"><?= $item->date ?></span>
            </p>
            <a href="<?= $item->url ?>" class="btn btn-primary btn-md waves-light btn btn-primary" type="button">
              Подробнее
              <?php//<span class="badge badge-danger ml-2">4</span>?>
            </a>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
      <?php endif; ?>

        <div class="col-sm-12 text-center mb-5">
          <a class="btn danger-color-dark" type="button" href="<?= $page->blog_parent->url ?>">Все новости</a>
        </div>
      </div>
    </div>
</section>
<!-- /blog-->