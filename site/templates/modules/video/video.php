<?php
/**
 * section_classes - Классы оболочки видео секции
 */
?>
  <section id="video-section" class="video-section video-section--loading"<?= $page->video_preview ? " style='background-image: url({$page->video_preview->url})'" : "" ?>>
    <video class="video-player" width="100%" height="auto" autoplay="" muted="" loop="loop">
      <source src="<?= $page->video_file->url?>" type="video/mp4">
    </video>
    <?php if ($page->video_logo) : ?>
    <div class="video-section__logo" style="background-image: url(<?= $page->video_logo->url?>)"></div>
    <?php endif; ?>
  </section>
