<!--flip-cards-->
<section
  class="<?= $page->flip_cards_section_paralax ? 'jarallax light-section ' : '' ?><?= $page->flip_cards_section_classes ? $page->flip_cards_section_classes : 'flip-cards grey lighten-3 pb-5' ?>"
  style='<?= ($page->flip_cards_section_image->url && !$page->flip_cards_section_paralax) ? "background-image: url({$page->flip_cards_section_image->url})" : '' ?>'
>
  <?php if ($page->flip_cards_section_image->url && $page->flip_cards_section_paralax): ?>
    <img class="jarallax-img" src="<?= $page->flip_cards_section_image->url ?>" alt="">
  <?php endif; ?>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="wrapper">
          <?php if ($page->flip_cards_title): ?>
            <div class="pb-3 text-center">
              <h2 class="<?= $page->flip_cards_title_classes ? $page->flip_cards_title_classes : 'text-uppercase' ?>">
                <?= $page->flip_cards_title ?>
              </h2>
            </div>
          <?php endif; ?>
          <?php if ($page->flip_cards_repeater->count): ?>
            <div class="clearfix">
              <div class="cols">
                <?php foreach ($page->flip_cards_repeater as $item) : ?>
                  <a href="<?= $item->links?>" class="flip-col <?= $page->flip_card_classes ? $page->flip_card_classes : '' ?>"
                       ontouchstart="this.classList.toggle('hover');">
                    <div class="flip-container">
                      <div class="front rounded z-depth-2"
                           style="<?= $item->image->url ? "background-image: url({$item->image->url})" : '' ?>"
                      >
                        <div class="inner">
                          <div class="flip-cards__title"><?= $item->title ?></div>
                          <div class="flip-cards__desc"><?= htmlspecialchars_decode($item->short_description) ?></div>
                        </div>
                      </div>
                      <div class="back rounded z-depth-2">
                        <div class="inner">
                          <?= htmlspecialchars_decode($item->body) ?>
                        </div>
                      </div>
                    </div>
                  </a>
                <?php endforeach; ?>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /flip-cards-->
