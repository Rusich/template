<!-- about-->
<section
  class="<?= $page->page_preview_section_parallax ? 'jarallax ' : '' ?><?= $page->page_preview_section_classes ? $page->page_preview_section_classes : 'page-preview grey lighten-3 pt-5 pb-5' ?>"
  style='<?= ($page->page_preview_section_image->url && !$page->page_preview_section_parallax) ? "background-image: url({$page->page_preview_section_image->url})" : '' ?>'
>
  <?php if ($page->page_preview_section_image->url && $page->page_preview_section_parallax): ?>
    <img class="jarallax-img" src="<?= $page->page_preview_section_image->url ?>" alt="">
  <?php endif; ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h2 class="<?= $page->page_preview_title_classes ? $page->page_preview_title_classes : 'text-black-50 text-uppercase' ?>">
          <?= $page->page_preview_page->string ? $page->page_preview_page->string : $page->page_preview_page->title ?>
        </h2>
        <?php if ($page->page_preview_page->short_description || $page->page_preview_page->body) : ?>
          <?= htmlspecialchars_decode(($page->page_preview_page->short_description) ? $page->page_preview_page->short_description : $page->page_preview_page->body) ?>
        <?php endif; ?>
        <?php if ($page->page_preview_page->script) echo $page->page_preview_page->script ?>
      </div>
      <?php if ($page->page_preview_page->short_description && $page->page_preview_page->body) : ?>
      <div class="col-sm-12 text-center">
        <a href="<?=$page->page_preview_page->url?>" class="btn danger-color-dark">Подробнее</a>
      </div>
      <?php endif; ?>
    </div>
  </div>
</section>
<!-- /about-->
