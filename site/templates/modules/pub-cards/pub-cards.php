<!-- pub cards-->
<section
  class="<?= $page->pub_section_paralax ? 'jarallax ' : '' ?><?= $page->pub_section_classes ? $page->pub_section_classes : 'pub-cards grey lighten-3 pt-5 pb-5' ?>"
  style='<?= ($page->pub_section_image->url && !$page->pub_section_paralax) ? "background-image: url({$page->pub_section_image->url})" : '' ?>'
>
  <?php if ($page->pub_section_image->url && $page->pub_section_paralax): ?>
    <img class="jarallax-img" src="<?= $page->pub_section_image->url ?>" alt="">
  <?php endif; ?>
  <div class="container news-cards">
    <div class="row">
      <?php if ($page->pub_title) : ?>
      <div class="col-sm-12 pt-5 pb-3 text-center">
        <h2 class="<?= $page->pub_title_classes ? $page->pub_title_classes : 'text-uppercase' ?>">
          <?= $page->pub_title ?>
        </h2>
      </div>
      <?php endif; ?>
      <?php if ($page->pub_cards_parent->children()->count): ?>
        <div class="col-sm-12 clearfix">
          <div class="row justify-content-center">
            <?php foreach ($page->pub_cards_parent->children('limit=' . $page->pub_cards_max) as $item) : ?>
              <div class="<?= $page->pub_card_classes ? $page->pub_card_classes : 'col-sm-6 col-lg-4 mb-4' ?>">
                <div class="card">
                  <?php if ($item->image->url) : ?>
                    <a href="<?= $item->url ?>">
                      <img class="card-img-top" src="<?= $item->image->size(600, 450)->url ?>"
                           alt="<?= $page->pub_title ?>">
                    </a>
                  <?php endif; ?>
                  <div class="card-body">
                    <h4 class="text-center pb-2"><a href="<?= $item->url ?>"><?= $item->title ?></a></h4>
                    <div class="text-black-50"><?= htmlspecialchars_decode($item->short_description) ?></div>
                    <div class="text-right"><a class="btn btn-elegant" href="<?= $item->url ?>">Подробнее...</a></div>
                  </div>
                  <div class="rounded-bottom elegant-color lighten-3 text-center pt-3">
                    <ul class="list-unstyled list-inline font-small">
                      <li class="list-inline-item pr-2 white-text">
                        <i class="fab fa-clock-o pr-1"> <?= $item->date?></i>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>
<!-- /pub cards-->
