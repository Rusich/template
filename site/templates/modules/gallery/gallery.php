<!-- gallery-->
<section
  class="<?= $page->gallery_parallax ? 'jarallax ' : '' ?><?= $page->gallery_classes ? $page->gallery_classes : 'gallery py-5' ?>"
  style='<?= ($page->gallery_image->url && !$page->gallery_parallax) ? "background-image: url({$page->gallery_image->url})" : '' ?>'
>
  <?php if ($page->gallery_image->url && $page->gallery_parallax): ?>
    <img class="jarallax-img" src="<?= $page->gallery_image->url ?>" alt="">
  <?php endif; ?>
  <div class="container">
    <div class="row pb-5">
      <?php if ($page->gallery_title): ?>
        <div class="col-sm-12">
          <h2 class="<?= $page->gallery_title_classes ? $page->gallery_title_classes : 'text-uppercase' ?>">
            <?= $page->gallery_title ?>
          </h2>
        </div>
      <?php endif; ?>
      <?php if ($page->gallery_images->count): ?>
        <?php
          $counter = 1;
          foreach ($page->gallery_images as $key=>$item):
          if ($counter > $page->gallery_count) break;
        ?>
          <figure class="<?= $page->gallery_images_classes ? $page->gallery_images_classes : 'col-sm-6 col-md-4' ?>">
            <?php if ($page->gallery_popup): ?>
            <a class="view overlay zoom" href="<?= $item->url ?>" data-size="1920x1080"
               data-lightbox="<?= $item->name ?>">
              <img class="img-fluid" src="<?= $item->size(800, 800)->url ?>" alt="gallery">
            </a>
            <?php else: ?>
              <img class="img-fluid" src="<?= $item->size(800, 800)->url ?>" alt="gallery">
            <?php endif; ?>
          </figure>
        <?php
          $counter++;
          endforeach; ?>
      <?php elseif($page->gallery_page->images->count) : ?>
        <?php
          $counter = 1;
          foreach ($page->gallery_page->images as $key=>$item):
          if ($counter > $page->gallery_count) break;
        ?>
          <figure class="<?= $page->gallery_images_classes ? $page->gallery_images_classes : 'col-sm-6 col-md-4' ?>">
            <?php if ($page->gallery_popup): ?>
              <a class="view overlay zoom" href="<?= $item->url ?>" data-size="1920x1080"
                 data-lightbox="<?= $item->name ?>">
                <img class="img-fluid" src="<?= $item->size(800, 800)->url ?>" alt="gallery">
              </a>
            <?php else: ?>
              <img class="img-fluid" src="<?= $item->size(800, 800)->url ?>" alt="gallery">
            <?php endif; ?>
          </figure>
          <?php
            $counter++;
            endforeach; ?>
      <?php endif; ?>
    </div>
    <?php if (!$page->gallery_images->count && $page->gallery_page->images->count) : ?>
      <div class="col-sm-12 text-center">
        <a href="<?=$page->gallery_page->url?>" class="btn danger-color-dark">Подробнее</a>
      </div>
    <?php endif; ?>
  </div>
</section>
<!-- /gallery-->