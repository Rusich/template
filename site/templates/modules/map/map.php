<!--<section class="map" id="map"></section>-->
<yandex-map
  :coords="mapCenter"
  :zoom="mapZoom"
  style="width: 100%; height: 600px;"
  :cluster-options="{1: {clusterDisableClickZoom: false}}"
  :behaviors="['ruler', mapDrag, mapScroll]"
  :controls="['zoomControl']"
  map-type="map"
>
  <ymap-marker
    marker-id="1",
    marker-type="placemark"
    :balloonTemplate = "balloonTemplate"
    cluster-name="1"
    :coords="mapPoint"
    circle-radius="1600"
    :hint-content="mapDesc"
    :marker-fill="{color: '#000000', opacity: 0}"
    :marker-stroke="{color: '#ff0000', width: 5}"
    :balloon="{header: 'header', body: 'body', footer: 'footer'}"
    :icon="mapIcon"
  ></ymap-marker>
</yandex-map>
