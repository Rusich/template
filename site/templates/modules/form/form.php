<!-- form-->
<section
  class="<?= $page->form_section_parallax ? 'jarallax ' : '' ?><?= $page->form_section_classes ? $page->form_section_classes : 'form-block view dark-section' ?>"
  style='<?= ($page->form_section_image->url && !$page->form_section_parallax) ? "background-image: url({$page->form_section_image->url})" : '' ?>'
>
  <?php if ($page->form_section_image->url && $page->form_section_parallax): ?>
    <img class="jarallax-img" src="<?= $page->form_section_image->url ?>" alt="">
  <?php endif; ?>
  <div class="container">
    <div class="row">
      <?php if ($page->form_title) : ?>
        <div class="col-sm-12">
          <h2 class="<?= $page->form_section_title ? $page->form_section_title : 'text-uppercase' ?>">
            <?= $page->form_title ?>
          </h2>
        </div>
      <?php endif; ?>
      <?php if ($page->form_page): ?>
        <div class="col-sm-12">
          <form
            v-if="form.id"
            @submit.prevent="sendForm()"
            id="<?= $page->form_page->id . '_form'?><?= (!$page->form_page->form_id) ?: ' '.$page->form_page->form_id?>"
            class="form <?= !$page->form_page->form_classes ?: $page->form_page->form_classes ?>"
          >
            <?php if ($page->form_page->children()->count): ?>
              <div class="row">
                <?php foreach ($page->form_page->children() as $item): ?>
                  <?php /* --------- input text ------------- */ ?>
                  <?php if ($item->template->name === 'field_text'): ?>
                    <?php if ($item->field_label): ?>
                      <label class="form__label <?= $item->field_label_classes ? $item->field_label_classes : '' ?>">
                        <?= $item->field_label ?>
                      <?php endif; ?>
                        <input
                          v-model="form.field_<?= $item->id ?>.fieldValue"
                          name="<?= $item->field_name ?>"
                          autocomplete="off"
                          class="form__field form__field--text <?= $item->field_classes ? $item->field_classes : 'form-control' ?>"
                          value="<?= $item->field_value ? $item->field_value : '' ?>"
                          type="<?= $item->field_type->name ? $item->field_type->name : '' ?>"
                          <?= ($item->field_type->name && $item->field_type->name === 'tel') ? "v-mask=\"'7(###)###-##-##'\"" : '' ?>
                          <?= $item->field_placeholder ? " placeholder='{$item->field_placeholder}'" : '' ?>
                          <?= $item->field_pattern ? " pattern='{$item->field_pattern}'" : '' ?>
                          <?= $item->field_required ? " required" : '' ?>
                        >
                    <?php if ($item->field_label): ?></label><?php endif; ?>
                  <?php endif; ?>
                  <?php /* --------- select ------------- */ ?>
                  <?php if ($item->template->name === 'field_select' && $item->field_options->count): ?>
                    <?php if ($item->field_label): ?>
                      <label class="form__label <?= $item->field_label_classes ? $item->field_label_classes : '' ?>">
                        <?= $item->field_label ?>
                      <?php endif; ?>
                    <select
                      v-model="form.field_<?= $item->id ?>.fieldValue"
                      name="<?= $item->field_name ?>"
                      class="form__field form__field--select <?= $item->field_classes ? $item->field_classes : 'custom-select' ?>">
                      <?php foreach ($item->field_options as $option): ?>
                        <option
                          value="<?= $option->field_value ?>"
                          <?= $option->on_off ? ' selected' : '' ?>
                        ><?= $option->field_value ?></option>
                      <?php endforeach; ?>
                    </select>
                    <?php if ($item->field_label): ?></label><?php endif; ?>
                  <?php endif; ?>
                  <?php /* --------- textarea ------------- */ ?>
                  <?php if ($item->template->name === 'field_textarea'): ?>
                    <?php if ($item->field_label): ?>
                      <label class="form__label <?= $item->field_label_classes ? $item->field_label_classes : '' ?>">
                        <?= $item->field_label ?>
                      <?php endif; ?>
                    <textarea
                      v-model="form.field_<?= $item->id ?>.fieldValue"
                      name="<?= $item->field_name ?>"
                      class="form__field form__field--textarea <?= $item->field_classes ? $item->field_classes : 'form-control' ?>"
                      <?= $item->field_placeholder ? " placeholder='{$item->field_placeholder}'" : '' ?>
                      <?= $item->field_required ? " required" : '' ?>></textarea>
                    <?php if ($item->field_label): ?></label><?php endif; ?>
                  <?php endif; ?>
                  <?php /* --------- checkbox ------------- */ ?>
                  <?php if ($item->template->name === 'field_checkbox'): ?>
                    <div class="col-sm-12 custom-control custom-checkbox mb-3 text-center">
                      <input
                        v-model="form.field_<?= $item->id ?>.fieldValue"
                        type="checkbox"
                        id="<?= $item->id . $item->field_name?>"
                        name="<?= $item->field_name ?>"
                        class="form__field form__field--checkbox <?= $item->field_classes ? $item->field_classes : 'custom-control-input' ?>"
                        <?= $item->field_required ? " required" : '' ?>>
                      <label
                        for="<?= $item->id . $item->field_name?>"
                        class="form__label <?= $item->field_label_classes ? $item->field_label_classes : '' ?>">
                        <?= htmlspecialchars_decode($item->field_desc) ?>
                      </label>
                    </div>
                  <?php endif; ?>
                <?php endforeach; ?>
              </div>
            <?php endif; ?>
            <button
              type="submit"
              :disabled="!formValidation()"
              class='<?= (!$page->form_page->form_submit_classes) ? "btn" : "{$page->form_page->form_submit_classes}" ?>'
            ><?= (!$page->form_page->form_submit_text) ? 'Отправить' : "{$page->form_page->form_submit_text}" ?></button>
          </form>
        </div>
      <?php endif; ?>
    </div>
  </div>

  <div class="modal fade right" id="formModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
       aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-side modal-bottom-right modal-notify" :class="form.modal.class" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <p class="heading">Оповещение</p>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="white-text">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          {{form.modal.message}}
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /form-->
