<?php
  function hasForms($list) {
    $form = false;
    foreach ($list as $k=>$item) {
      if ($item->name === 'form') {
        $form = true;
      }
    }
    return $form;
  }
  if (hasForms($page->sections_list) || hasForms($page->sections_list_after)) :
    $form = $page->form_page ?>
  <?= "var form = " ?>
      <?= '{'; ?>
      <?= "id: {$form->id}, "; ?>
      <?= "isValid: false, "; ?>
      <?= "email: '{$form->form_email}', "; ?>
      <?= "theme: '{$form->form_theme}', "; ?>
      <?= "error: '{$form->field_error}', "; ?>
      <?= "success: '{$form->field_success}', "; ?>
      <?= "modal: {class: 'modal-info', message: '', }, "; ?>
      <?php if ($form->children()->count): ?>
      <?php foreach ($form->children() as $field): ?>
      <?= "field_{$field->id}: " ?>
      <?= '{'; ?>
        <?= "fieldID: '{$field->id}', "; ?>
        <?= "fieldTitle: '{$field->title}', "; ?>
        <?= "fieldName: '{$field->field_name}', "; ?>
        <?= "fieldType: '{$field->field_type->name}', "; ?>
        <?= ($field->field_value) ? "fieldValue: '{$field->field_value}', " : "fieldValue: '', " ?>
        <?php if($field->field_pattern) echo "fieldPattern: '{$field->field_pattern}', "; ?>
        <?php if($field->field_error) echo "fieldError: '{$field->field_error}', "; ?>
        <?= "fieldRequired: '{$field->field_required}', "; ?>
        <?php if ($field->field_hidden) echo "fieldHidden: '{$field->field_hidden}', "; ?>
        <?php if ($field->field_options->count): ?>
        <?= "fieldOptions: ["; ?>
          <?php foreach ($field->field_options as $option): ?>
            <?= '{'; ?>
            <?= "optionName: '{$option->field_name}', "; ?>
            <?= "optionValue: '{$option->field_value}', "; ?>
            <?= "optionSelected: '{$option->on_off}', "; ?>
            <?= '},'; ?>
          <?php endforeach; ?>
        <?= "], "; ?>
        <?php endif; ?>
        <?= '},'; ?>
      <?php endforeach; ?>
      <?php endif; ?>
      <?= '};'; ?>
<?php endif; ?>
