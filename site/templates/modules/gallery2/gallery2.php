<!-- gallery-->
<?php
  $thumb_height = $page->gallery2_img_height ? $page->gallery2_img_height : 800;
  $thumb_width = $page->gallery2_img_width ? $page->gallery2_img_width : 800;
?>
<section
  class="<?= $page->gallery2_parallax ? 'jarallax ' : '' ?><?= $page->gallery2_classes ? $page->gallery2_classes : 'gallery py-5' ?>"
  style='<?= ($page->gallery2_image->url && !$page->gallery2_parallax) ? "background-image: url({$page->gallery2_image->url})" : '' ?>'
>
  <?php if ($page->gallery2_image->url && $page->gallery2_parallax): ?>
    <img class="jarallax-img" src="<?= $page->gallery2_image->url ?>" alt="">
  <?php endif; ?>
  <div class="container">
    <div class="row pb-5">
      <?php if ($page->gallery2_title): ?>
        <div class="col-sm-12">
          <h2 class="<?= $page->gallery2_title_classes ? $page->gallery2_title_classes : 'text-uppercase' ?>">
            <?= $page->gallery2_title ?>
          </h2>
        </div>
      <?php endif; ?>
      <?php if ($page->gallery2_images->count):
          $counter = 1;
          foreach ($page->gallery2_images as $key=>$item):
          if ($counter > $page->gallery2_count) break;
        ?>
          <figure class="<?= $page->gallery2_images_classes ? $page->gallery2_images_classes : 'col-sm-6 col-md-4' ?>">
            <?php if ($page->gallery2_popup): ?>
            <a class="view overlay zoom" href="<?= $item->url ?>" data-size="1920x1080"
               data-lightbox="<?= $item->name ?>">
              <img class="img-fluid" src="<?= $item->size($thumb_width, $thumb_height)->url ?>" alt="gallery">
            </a>
            <?php else: ?>
              <img class="img-fluid" src="<?= $item->size($thumb_width, $thumb_height)->url ?>" alt="gallery">
            <?php endif; ?>
          </figure>
        <?php
          $counter++;
          endforeach; ?>
      <?php elseif($page->gallery2_page->images->count) : ?>
        <?php
          $counter = 1;
          foreach ($page->gallery2_page->images as $key=>$item):
          if ($counter > $page->gallery2_count) break;
        ?>
          <figure class="<?= $page->gallery2_images_classes ? $page->gallery2_images_classes : 'col-sm-6 col-md-4' ?>">
            <?php if ($page->gallery2_popup): ?>
              <a class="view overlay zoom" href="<?= $item->url ?>" data-size="1920x1080"
                 data-lightbox="<?= $item->name ?>">
                <img class="img-fluid" src="<?= $item->size($thumb_width, $thumb_height)->url ?>" alt="gallery">
              </a>
            <?php else: ?>
              <img class="img-fluid" src="<?= $item->size($thumb_width, $thumb_height)->url ?>" alt="gallery">
            <?php endif; ?>
          </figure>
          <?php
            $counter++;
            endforeach; ?>
      <?php endif; ?>
    </div>
    <?php if (!$page->gallery2_images->count && $page->gallery2_page->images->count) : ?>
      <div class="col-sm-12 text-center">
        <a href="<?=$page->gallery2_page->url?>" class="btn danger-color-dark">Подробнее</a>
      </div>
    <?php endif; ?>
  </div>
</section>
<!-- /gallery-->