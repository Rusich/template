<!-- tabs -->
<section
  class="<?= $page->tabs_section_parallax ? 'jarallax ' : '' ?><?= $page->tabs_section_classes ? $page->tabs_section_classes : 'tabs grey lighten-3 pt-5 pb-5' ?>"
  style='<?= ($page->tabs_section_image->url && !$page->tabs_section_parallax) ? "background-image: url({$page->tabs_section_image->url})" : '' ?>'
>
  <?php if ($page->tabs_section_image->url && $page->tabs_section_parallax): ?>
    <img class="jarallax-img" src="<?= $page->tabs_section_image->url ?>" alt="">
  <?php endif; ?>
  <div class="container content-tabs">
    <div class="row">
      <div class="col-sm-12 text-center">
        <?php if ($page->tabs_title): ?>
          <h2 class="<?= $page->tabs_title_top_classes ? $page->tabs_title_top_classes : 'text-uppercase' ?>">
            <?= $page->tabs_title ?>
          </h2>
        <?php endif; ?>
        <div class="accordion md-accordion accordion-4" id="accordionEx2" role="tablist" aria-multiselectable="true">
          <?php $counter = 1;
          foreach ($page->tabs_repeater as $item): ?>
            <div class="card">
              <div id="heading<?= $counter ?>" role="tab"
                   class="<?= ($page->tabs_title_classes ? $page->tabs_title_classes : 'card-header z-depth-1 elegant-color') ?>">
                <a data-toggle="collapse" data-parent="#accordionEx2" href="#collapse<?= $counter ?>"
                   aria-expanded="true" aria-controls="collapse<?= $counter ?>">
                  <h4 class="mb-0 text-white text-center"><?= $item->title ?></h4>
                </a>
              </div>
              <div class="collapse<?= $counter === 1 ? ' show' : ''?>" id="collapse<?= $counter ?>" role="tabpanel"
                   aria-labelledby="heading<?= $counter ?>"
                   data-parent="#accordionEx2">
                <div class="<?= ($page->tabs_body_classes ? $page->tabs_body_classes : 'card-body grey lighten-3') ?>">
                  <?= htmlspecialchars_decode($item->body) ?>
                </div>
              </div>
            </div>
            <?php $counter++;
          endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /tabs -->