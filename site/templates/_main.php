<?php /** Шаблон подгружаемый после _init.php и основного шаблона страницы */ ?>
<!DOCTYPE html>
<html lang="ru" class=" h-100">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="<?= $seoDescription; ?>">
  <meta name="keywords" content="<?= $seoKeywords; ?>">
  <meta name="yandex-verification" content="71f96fa4880f5e4c" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
        integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="<?= $config->urls->css ?>main.css">
  <script>
    /** user agent */
    var mobiles = <?=$mobiles?>;
    /** map variables */
    var map_center = <?= (isset($page->map_center) && $page->map_center !== '') ? $page->map_center : '[]' ?>;
    var map_point = <?= (isset($page->map_point) && $page->map_point !== '') ? $page->map_point : '[]' ?>;
    var map_zoom = <?= (isset($page->map_zoom) && $page->map_zoom !== '') ? $page->map_zoom : 16 ?>;
    var map_scroll = <?= (isset($page->map_scroll) && $page->map_scroll) ? 1 : 0 ?>;
    var map_drag = <?= (isset($page->map_drag) && $page->map_drag) ? 1 : 0 ?>;
    var map_desc = "<?= (isset($page->map_desc) && $page->map_desc) ? $page->map_desc : '' ?>";
    /** forms variables */
    <?php include_once "./modules/form/js-generator.php"; ?>
  </script>
  <title><?= $seoTitle; ?></title>
</head>
<body class="nojs h-100 body <?= $page->template ?>">
  <div class="site d-flex flex-column h-100" id="site">
    <?php include_once "./parts/header/header.part.php"; ?>
    <main class="main flex-grow-1 pt-5">
      <?php
      if ($page->sections_list) {
        $customModules = $page->sections_list;
        include "./modules/modules-controller.php";
      }
      ?>
      <?php
      if (!$path) $path = $page->template;
      include_once "views/{$path}/{$page->template}.view.php";
      ?>
      <?php
      if ($page->sections_list_after) {
        $customModules = $page->sections_list_after;
        include "./modules/modules-controller.php";
      }
      ?>
    </main>
    <?php include_once "./parts/footer/footer.part.php"; ?>
  </div>
  <script src="<?= $config->urls->js ?>main.js"></script>

  <!-- Yandex.Metrika counter -->
  <script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
      m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(52566211, "init", {
      id:52566211,
      clickmap:true,
      trackLinks:true,
      accurateTrackBounce:true,
      webvisor:true
    });
  </script>
  <noscript><div><img src="https://mc.yandex.ru/watch/52566211" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  <!-- /Yandex.Metrika counter -->
</body>
</html>
